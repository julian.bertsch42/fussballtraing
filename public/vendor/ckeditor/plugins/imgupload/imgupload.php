<?php
// PHP Upload Script for CKEditor:  http://coursesweb.net/

// HERE SET THE PATH TO THE FOLDER WITH IMAGES ON YOUR SERVER (RELATIVE TO THE ROOT OF YOUR WEBSITE ON SERVER)


$upload_dir = realpath(__DIR__ . '/../../../../').'/upload/inline/';
$upload_url = 'upload/inline';


// HERE PERMISSIONS FOR IMAGE
$imgsets = array(
    'maxsize' => 5000,          // maximum file size, in KiloBytes (2 MB)
    'maxwidth' => 2000,          // maximum allowed width, in pixels
    'maxheight' => 2000,         // maximum allowed height, in pixels
    'minwidth' => 10,           // minimum allowed width, in pixels
    'minheight' => 10,          // minimum allowed height, in pixels
    'type' => array('bmp', 'gif', 'jpg', 'jpe', 'png', 'jpeg')        // allowed extensions
);

$maxDim = 2000;
if(isset($_FILES['upload'])){
    $size = $_FILES['upload']['size'];
}

if(isset($_POST['fileType']) && $_POST['fileType'] == 'avatar' ){
    $maxDim = 500;
}

list($width, $height, $type, $attr) = getimagesize( $_FILES['upload']['tmp_name'] );
if ( $width > $maxDim || $height > $maxDim ) {
    $target_filename = $_FILES['upload']['tmp_name'];
    $fn = $_FILES['upload']['tmp_name'];
    $size = getimagesize( $fn );
    $ratio = $size[0]/$size[1]; // width/height
    if( $ratio > 1) {
        $width = $maxDim;
        $height = $maxDim/$ratio;
    } else {
        $width = $maxDim*$ratio;
        $height = $maxDim;
    }
    $src = imagecreatefromstring( file_get_contents( $fn ) );
    $dst = imagecreatetruecolor( $width, $height );
    imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
    imagedestroy( $src );
    imagejpeg( $dst, $target_filename, 100 ); // adjust format as needed
    $size = getimagesize( $dst );
    imagedestroy( $dst );
}


$re = '';

if(isset($_FILES['upload']) && strlen($_FILES['upload']['name']) > 1) {
    $info = pathinfo(basename($_FILES['upload']['name']));
    $format = $info["extension"];
    $file = $info["filename"];
    $img_name = substr(md5(time()), -7);
    $img_name .= '_'.$file.'.'.$format;

    // get protocol and host name to send the absolute image path to CKEditor
    //$protocol = !empty($_SERVER['HTTPS']) ? 'https://' : 'http://';
    $protocol = 'http://';
    $site = $protocol. $_SERVER['SERVER_NAME'] .'/';

    $upload_dir = $upload_dir . $img_name;       // full file path
    $sepext = explode('.', strtolower($_FILES['upload']['name']));
    $type = end($sepext);       // gets extension
    list($width, $height) = getimagesize($_FILES['upload']['tmp_name']);     // gets image width and height
    $err = '';         // to store the errors

    // Checks if the file has allowed type, size, width and height (for images)
    if(!in_array($type, $imgsets['type'])) $err .= 'The file: '. $_FILES['upload']['name']. ' has not the allowed extension type.';
    if($size > $imgsets['maxsize']*1000) $err .= '\\n Maximum file size must be: '. $imgsets['maxsize']. ' KB.';
    if(isset($width) && isset($height)) {
        if($width > $imgsets['maxwidth'] || $height > $imgsets['maxheight']) $err .= '\\n Width x Height = '. $width .' x '. $height .' \\n The maximum Width x Height must be: '. $imgsets['maxwidth']. ' x '. $imgsets['maxheight'];
        if($width < $imgsets['minwidth'] || $height < $imgsets['minheight']) $err .= '\\n Width x Height = '. $width .' x '. $height .'\\n The minimum Width x Height must be: '. $imgsets['minwidth']. ' x '. $imgsets['minheight'];
    }

    // If no errors, upload the image, else, output the errors
    if(isset($_GET['CKEditorFuncNum'])){
        if($err == '') {
            if(move_uploaded_file($_FILES['upload']['tmp_name'], $upload_dir)) {
                $CKEditorFuncNum = $_GET['CKEditorFuncNum'];
                $url = $site. $upload_url . '/' . $img_name;
                $message = $img_name .' successfully uploaded: \\n- Size: '. number_format($_FILES['upload']['size']/1024, 3, '.', '') .' KB \\n- Image Width x Height: '. $width. ' x '. $height;
                $re = "window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$message')";
            }
            else $re = 'alert("Unable to upload the file")';
        }
        else $re = 'alert("'. $err .'")';
        echo "<script>$re;</script>";
    }
    else{
        if($err == '') {
            if(move_uploaded_file($_FILES['upload']['tmp_name'], $upload_dir)) {
                $message = 'pictureSuccessfullyUploaded';
                $re = $message;
                $url = $site. $upload_url . '/' . $img_name;
            }
            else $re = "unableToUploadFile";
        }
        else $re = $err;
    }
}
?>
