$(document).ready(function(){

    var id = 1;
    var objects = [];
    var objs = [];
    /* animation */
    //var scenes = []
    var scenesPos = []
    let originalScenePositions = []
    let playAll = false
    let stop = false

    let iPlayAll = 0
    sceneTime = 2000
    let stoppedSequence = null
    let reset

    /* endanimation */

    //Hide every Objectsite
    $('div.menu div.content ul').not('div.menu div.content ul.spieler').hide();

    //Menu Show-Hide Button
    $('#menu-control').click(function() {
        if($('div.menu').hasClass('expanded')){
            $('div.menu').css('display','none').toggleClass('expanded');
        }
        else {
            $('div.menu').css('display','inherit').toggleClass('expanded');
        }

    });

    //Select Change Event
    $('div.menu div.content select.form-control').change(function(){
        var drawingColorEl = $('#drawing-color');
        if($(this).val() === "freieszeichnen") {
          canvas.isDrawingMode = !canvas.isDrawingMode;
          canvas.freeDrawingBrush.color = "black";
          canvas.freeDrawingBrush.width = 4;
        } else {
          canvas.isDrawingMode = false;
        }
        $('div.menu div.content ul').hide();
        $('div.menu div.content ul.'+$(this).val()).show();

    });

    //Save Button
    $("#btnSave").click(function() {
        html2canvas($("div.gamefield"), {
            onrendered: function(canvas) {
                theCanvas = canvas;
                document.body.appendChild(canvas);

                // Convert and download as image
                Canvas2Image.saveAsPNG(canvas);
                $("#img-out").append(canvas).hide();
                // Clean up
                //document.body.removeChild(canvas);
            }
        });
    });

    //Gamefield Initiation
    var gamefield1 = new gamefield({});
    gamefield1.loadGamefield();


    //Objectsite's Object Click Event -- add a new Object
    $('div.menu div.content ul li img').click(function(){
        var type = $(this).parents('ul').attr('class');
        objects[id] = new object({
            objects: objects,
            id: id,
            image: $(this).attr('src'),
            type: type
        });
        var fObject = new fabric.Image({
            id: id,
            src: $(this).attr('src')
        });

        id++;
    });

    $("#deleteScene").on("click", function () {
        deleteScene(currentScene)
    })

    function deleteScene (scene) {
        scenes = scenes.filter(function (sceneI) {
            return sceneI != scenes[scene]
        })
        // clean other scene buttons
        $("button.scene").remove()
        scenes.forEach(function (scene, i) {
            let realI = i+1;
            $('.scenes-buttons button').removeClass('btn-primary')
            $('.scenes-buttons button').removeClass('btn-info')
            $('.scenes-buttons button').addClass('btn-info')
            $('.scenes-buttons').append('<button class="btn btn-primary btn-space scene" id="scene-'+i+'" value="'+i+'">Szene '+realI+'</button>')
            changeScene(i)
        })
    }

    function startSequence(o, key, positionArrays, objects, oldPositionArray, sequenceTime, currentSequence, last = false) {
        startAnimationQueue(o, positionArrays[key], function (pause) {
            if (!pause) {
                if (last) {
                    stoppedSequence = null
                    if (playAll) {
                        if (scenes.length-1 == iPlayAll) {setTimeout(function () { stopRecording(); }, sequenceTime); downloadButton.disabled = false; resetScene(true); playAll = false; changeScene(iPlayAll);  } else {resetScene(); iPlayAll++; changeScene(iPlayAll, true) }
                    } else {
                        resetScene(true)
                    }
                }
            } else {
                console.log("paused")
                stoppedSequence = currentSequence
                stop = true
                // save positions
                if (reset) resetScene(true);
            }
           //o.set({ top: oldPositionArray[key].top, left: oldPositionArray[key].left })
            /*let followKey = o.cacheKey
            while (followKey) {
                if (! (followKey in elementCircleArray)) {
                    followKey = null
                } else {
                    elementCircleArray[followKey].set({ opacity: 1 })
                    lineArray[followKey].set({ opacity: 1 })
                    if (followKey in elementCircleArray) {
                        followKey = elementCircleArray[followKey].cacheKey;
                    } else {
                        followKey = null
                    }
                }
            }*/
            o.setCoords()
            canvas.renderAll()
        });
    }

    function resetScene (last = false) {
        let current = canvas.getObjects().map(function(o) {
            return o.set('active', true);
        });
        current.forEach(function (o) {
            canvas.remove(o)
        })
        canvas.renderAll()
        scenes[currentScene].forEach(function (o, index) {
            let top = originalScenePositions[index].top
            let left = originalScenePositions[index].left
            o.set({opacity: 1, top: top, left: left})
            canvas.add(o)
            renderLines(o)
        })
        canvas.renderAll()
        // todo: last
        if (last) setButtonsReset()
        reset = false
    }

    function animate (sequence, maxSequence) {
        let sequenceTime = sceneTime;
        setTimeout(function() {
            let objects = []
            let positionArrays = []
            let oldPositionArray = []
            canvas.getObjects().forEach(function (o) {
                let second = false
                let oldTop  = o.top
                let oldLeft = o.left
                if ("cacheKey" in o) {
                    if (o.cacheKey in elementCircleArray) {
                        if (o.type !=  "circle") {
                            if (second) {
                                o.sequence = 2
                            }
                            /* let left = elementCircleArray[o.cacheKey].left
                            let top = elementCircleArray[o.cacheKey].top
                            startAnimationQueue(o, [{"left": left, "top": top, "duration": 1000}]); */
                            // todo: loop until the end

                            let followCircle = elementCircleArray[o.cacheKey]
                            let positionArray = []
                            let i = 0
                            while (followCircle) {
                                // todo: loop until the end
                                let left = followCircle.left
                                let top = followCircle.top

                                if (followCircle.sequence == sequence) positionArray.push({"left": left, "top": top, "duration": sequenceTime })
                                if ('cacheKey' in followCircle) {
                                    if (! (followCircle.cacheKey in elementCircleArray)) {
                                        followCircle = null
                                    }  else {
                                        followCircle = elementCircleArray[followCircle.cacheKey];
                                    }
                                }
                                i++
                            }
                            objects.push(o)
                            positionArrays.push(positionArray)
                            oldPositionArray.push({top: oldTop, left: oldLeft})
                        }
                    }
                }
            })
            if (maxSequence == 0) {
                if (playAll) {
                    if (scenes.length-1 == iPlayAll) {setTimeout(function () { stopRecording(); }, sequenceTime); downloadButton.disabled = false; resetScene(true); playAll = false; changeScene(iPlayAll);  } else {resetScene(); iPlayAll++; changeScene(iPlayAll, true) }
                } else {
                    resetScene(true)
                }

            } else {
               objects.forEach(function (o, key) {
                   let last = false
                   if (sequence == maxSequence) last = true
                   startSequence(o, key, positionArrays, objects, oldPositionArray, sequenceTime, sequence, last)
               })
            }
        }, sequenceTime * (stoppedSequence ? sequence-stoppedSequence : sequence-1));
    }

    function setPositions () {
        originalScenePositions = []
        scenes[currentScene].forEach(function (o) {
            originalScenePositions.push({top: o.top, left: o.left})
        })
    }

    function playScene () {
        if (!stop) { setPositions() }
        stop = false
        //set all originalk positions
        canvas.getObjects().forEach(function (o) {
            if (o.type == "circle" || o.type == "line") o.set({ opacity: 0 });
            canvas.renderAll()
        })
        let maxSequence = 0
        canvas.getObjects().forEach(function (o) {
            if ('sequence' in o)  if (o.sequence > maxSequence)  maxSequence = o.sequence
        })
        // animate
        for (stoppedSequence ? sequence = stoppedSequence : sequence  = 1; sequence <= maxSequence; sequence++) {
            animate(sequence, maxSequence)
        }
        if (maxSequence == 0) {
            animate(0, 0)

        }
    }
    function setButtonsPlaying () {
        $("#play").attr('disabled', true)
        $("#stop").attr('disabled', false)
        $("#reset").attr('disabled', false)
        $("#playAll").attr('disabled', true)
        $("#newScene").attr('disabled', true)
        $(".scenes-buttons button").attr('disabled', true)
    }

    function changeSceneButtons () {
        stoppedSequence = null
        setButtonsStop()
        $(".scenes-buttons button").attr('disabled', false)
        $("#playAll").attr('disabled', false)
        $("#reset").attr('disabled', true)
        $("#newScene").attr('disabled', true)
    }

    function setButtonsReset () {
        stoppedSequence = null
        $("#play").attr('disabled', false)
        $("#stop").attr('disabled', true)
        $(".scenes-buttons button").attr('disabled', false)
        $("#playAll").attr('disabled', false)
        $("#reset").attr('disabled', true)
        $("#newScene").attr('disabled', false)
    }

    function setButtonsStop () {
        $("#play").attr('disabled', false)
        $("#stop").attr('disabled', true)
        $(".scenes-buttons button").attr('disabled', true)
        $("#reset").attr('disabled', false)
        $("#playAll").attr('disabled', true)
    }

    $("#play").on('click', function () {
        setButtonsPlaying()
        playScene()
    })

    $("#reset").on('click', function () {
        if (stop)  {
            resetScene(true)
        } else {
            stop = !stop
            reset = true
        }
    })

    $("#stop").on('click', function () {
        stop = !stop
        setButtonsStop()
    })

    $("#playAll").on('click', function () {
        playAll = true
        setButtonsPlaying()
        changeScene(currentScene, true)
        startRecording()
    })

    $("#sceneTime").on('change', function () {
        console.log($(this).val())
       sceneTime = $(this).val()*1000
    })

    $("#newScene").on('click', function() {
        canvas.discardActiveObject();
        let current = canvas.getObjects().map(function(o) {
            return o.set('active', true);
        });
        current.forEach(function(o) {
            canvas.remove(o)
        });
        scenes.push([])
        sceneLog[currentScene] = {
            circleArray,
            elementCircleArray,
            lineArray,
        }
        circleArray = []
        elementCircleArray = []
        lineArray = []
        currentScene = scenes.length-1
        $('.scenes-buttons button').removeClass('btn-primary')
        $('.scenes-buttons button').removeClass('btn-info')
        $('.scenes-buttons button').addClass('btn-info')
        $('.scenes-buttons').append('<button class="btn btn-primary btn-space scene" id="scene-'+currentScene+'" value="'+currentScene+'">Szene '+scenes.length+'</button>')
    })

    function changeScene (scene, play = false) {
        console.log('scene changed')
        // set active button
        $('.scenes-buttons button').removeClass('btn-primary')
        $('.scenes-buttons button').addClass('btn-info')
        $('.scenes-buttons button[value='+scene+']').removeClass('btn-info')
        $('.scenes-buttons button[value='+scene+']').addClass('btn-primary')
        canvas.discardActiveObject();
        sceneLog[currentScene] = {
            circleArray,
            elementCircleArray,
            lineArray,
        }
        currentScene = scene
        iPlayAll = scene
        circleArray = sceneLog[currentScene].circleArray
        elementCircleArray = sceneLog[currentScene].elementCircleArray
        lineArray = sceneLog[currentScene].lineArray
        let current = canvas.getObjects().map(function(o) {
            return o.set('active', true);
        });
        current.forEach(function(o) {
            canvas.remove(o)
        })
        canvas.renderAll()
        if (scenes[scene]) {
            scenes[scene].forEach(function(o) {

                canvas.add(o);
                canvas.renderAll()
                canvas.getObjects().map(function(o) {
                    renderLines(o)
                })
            })
        }
        if (play) playScene();
    }

    $(".scenes-buttons").on('click', 'button.scene', function(e) {

        changeScene($(this).val())
    })


    /*
     * animation function
     */
    function startAnimationQueue(element, animationQueue, _callback = function () {}){
        var runningDuration = 0; // variable that adds up the animationQueue durations
        for (var i=0; i<animationQueue.length; i++){
            var animationDefinition = animationQueue[i];
            let elementLeft = i ? animationQueue[i-1].left : element.left
            let elementTop = i ? animationQueue[i-1].top : element.top
            // check if no animation required
            let top = elementTop > animationDefinition.top ? elementTop - animationDefinition.top : animationDefinition.top - elementTop
            let left = elementLeft > animationDefinition.left ? elementLeft - animationDefinition.left : animationDefinition.left - elementLeft
            let addTop = animationDefinition.top > elementTop ? '+=':'-='
            let addLeft = animationDefinition.left > elementLeft ? '+=':'-='
            if ((elementLeft-animationDefinition.left) + (elementTop-animationDefinition.top)) {
                // Create a closure around the animationDefiniton so that each setTimeout gets sequential animationDefinition inputs
                var fn = function(animationDefinition, _callback = null){

                        setTimeout((function() {
                            return new Promise(function(resolve, reject) {
                                if (!stop) {
                                    element.animate('left', addLeft + left, {
                                        duration: animationDefinition.duration,
                                        onChange: canvas.renderAll.bind(canvas),
                                        abort: function () {
                                            return stop
                                        }
                                    })
                                    element.animate('top', addTop + top, {
                                        duration: animationDefinition.duration,
                                        onChange: canvas.renderAll.bind(canvas),
                                        onComplete: function () {
                                            _callback(stop)
                                        },
                                        abort: () => stop
                                    })
                                    return resolve()
                                }
                            })
                        }), runningDuration);
                }

                if (i == animationQueue.length-1) { fn(animationDefinition, _callback) } else { fn(animationDefinition) }

                runningDuration += animationDefinition.duration
            }
            element.setCoords()
            canvas.renderAll()
        }
    }
});
