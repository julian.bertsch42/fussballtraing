var object = function(options){

    var vars = {
        type : 'Spieler',
        image : 'keeper_1_left.png',
        id : ''
    };

    this.construct = function(options){
        $.extend(vars , options);
        newObject();
    };
    var self = this;

    //Public

    //Private
    var newObject = function(){
        /*
        $('div.objects').append('' +
            '<div class="object object-'+vars.id+'" style="left: 17%; top: 8%;">' +
            '<button class="delete"><span class="glyphicon glyphicon-trash"></span></button>' +
            '<button class="copy" style="left: 16px"><span class="glyphicon glyphicon-copy"></span></button>' +
            '<img id="img-'+vars.id+'" src="'+vars.image+'" width="64" height="64" />' +
            '</div>' +
            ''); */
            var imgElement = document.getElementById("img-"+vars.id);
            fabric.Image.fromURL(vars.image, function(oImg) {
              var widthHeight = 0.2;
              if (oImg.width <= 100) widthHeight = 1;
              if(vars.type == "material") {
                widthHeight = 0.3;
                if (oImg.width >= 500) widthHeight = 0.2;
                if (oImg.width <= 350) widthHeight = (oImg.width/1000)/2;
                if (oImg.width <= 110) widthHeight = 0.3;
                if (oImg.width <= 102) widthHeight = 0.8;
                if (oImg.width <= 50) widthHeight = 1.2;
                if (oImg.width <= 25) widthHeight = 1.5;
              } else if (vars.type == "baelle") {
                widthHeight = 0.5;
                if (oImg.width >= 500) widthHeight = 0.2;
              } else if (vars.type == "pfeile") {
                widthHeight = 0.5;
              }
              oImg.scale(widthHeight);
              oImg.objectCaching = true
              oImg.top = 200
              oImg.left = 200
              oImg.originX = oImg.originY = 'center';
              canvas.add(oImg).renderAll();
            });
        InitObjectEvents();
    };

    var InitObjectEvents = function(){

        //Object Click Event
        $('.object-'+vars.id+' img').click(function(e){
            e.stopPropagation();

            $('.object img.ui-resizable').resizable('destroy').parent().draggable('destroy').removeClass('active');
            $(this).parent().addClass('active');
            $(this).resizable();

            //Object Drag Event
            $(this).parent().parent().draggable({
                stop: function( event, ui ) {
                    $(this).css("left",parseInt($(this).css("left")) / ($(".objects").width() / 100)+"%");
                    $(this).css("top",parseInt($(this).css("top")) / ($(".objects").height() / 100)+"%");
                }
            });
        });

        $('.object-'+vars.id+' button.delete').click(function(e){
            e.stopPropagation();

            removeObject();
        });
        $('.object-'+vars.id+' button.copy').click(function(e){
            e.stopPropagation();

            addObject();
        });

    };

    var addObject = function () {
      console.log(self);
      var newId = vars.objects.length;
      vars.objects[newId] = new object(vars);
    };

    var removeObject = function(){
      console.log(vars.objects);
        $('.object-'+vars.id).remove();
    };

    //Constructor
    this.construct(options);
};
