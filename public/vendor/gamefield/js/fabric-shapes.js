var canvas_shape = new fabric.Canvas('shape-canvas');
canvas_shape.renderAll();

init();

function init () {
  var recR = new fabric.Rect({
    top: 30,
    left: 15,
    width: 75,
    height: 100,
    hasControls: false,
    fill: "#47b475",
    opacity: $("#shape-opacity").val()/100,
    rx: 10,
    ry: 10,
    lockMovementX: true,
    lockMovementY: true
  });
  var rec = new fabric.Rect({
      top: 30,
      left: 115,
      width: 75,
      height: 100,
      hasControls: false,
      lockMovementX: true,
      lockMovementY: true,
      fill: "#47b475",
      opacity: $("#shape-opacity").val()/100
  });

  var cir = new fabric.Circle({
      top: 30,
      left: 15,
      radius: 50,
      hasControls: false,
      lockMovementX: true,
      lockMovementY: true,
      fill: "#47b475",
      opacity: $("#shape-opacity").val()/100
  });

  var p1 = {left:200,top:200}; var p2 = {left:160,top:240}; var p3 = {left:240,top:240};
  var tri = new fabric.Polygon([{x:p1.left, y:p1.top},{x:p2.left, y:p2.top},{x:p3.left, y:p3.top}], {
    top: 30,
    left: 15,
    angle: 0,
    fill: "#47b475",
    strokeWidth: 8,
    stroke: "#47b475",
    hasControls: false,
    lockMovementX: true,
    lockMovementY: true
  });

  var triR = new fabric.Polygon([{x:p1.left, y:p1.top},{x:p2.left, y:p2.top},{x:p3.left, y:p3.top}], {
    top: 30,
    left: 115,
    angle: 0,
    fill: "#47b475",
    rx:20,
    ry:20,
    strokeWidth: 8,
    strokeLineJoin: 'round',
    stroke: "#47b475",
    hasControls: false,
    lockMovementX: true,
    lockMovementY: true
  });
  let page = 0;
  showDifferentShapes(page)
  $(".prev-shape").click(function() {
    if (page > 0) {
      page--
      showDifferentShapes(page)
    }
  });
  $(".next-shape").click(function() {
    if (page < 2) {
      page++
      showDifferentShapes(page)
    }
  });
  function showDifferentShapes (page) {
    //hide
    canvas_shape.remove(...canvas_shape.getObjects());
    if (page == 0) {
      //show rectangles
      canvas_shape.add(recR);
      canvas_shape.renderAll();
      canvas_shape.add(rec);
      canvas_shape.renderAll();
    }
    if (page == 1) {
      //show circle
      canvas_shape.add(cir);
      canvas_shape.renderAll();
    }
    if (page == 2) {
      //show rectangles
      canvas_shape.add(tri);
      canvas_shape.renderAll();
      canvas_shape.add(triR);
      canvas_shape.renderAll();
    }
  }
}

function AddShape() {
  canvas_shape.getActiveObject().clone(function(cloned) {
    // clone again, so you can do multiple copies.
    	cloned.clone(function(clonedObj) {
    		canvas.discardActiveObject();
    		clonedObj.set({
    			left: 250,
    			top: 250,
    			evented: true,
    		});
    		if (clonedObj.type === 'activeSelection') {
    			// active selection needs a reference to the canvas.
    			clonedObj.canvas = canvas;
    			clonedObj.forEachObject(function(obj) {
    				canvas.add(obj);
    			});
    			// this should solve the unselectability
    			clonedObj.setCoords();
    		} else {
    			canvas.add(clonedObj);
    		}
    		canvas.setActiveObject(clonedObj);
    		canvas.requestRenderAll();
    	});
  });
}

$("#shape-opacity").on('input', function () {
  $("#shape-opacity-value").html($("#shape-opacity").val());
  canvas_shape.forEachObject(function(o) {
    o.set("opacity", $("#shape-opacity").val()/100);
  });
  canvas_shape.renderAll();
  if (canvas.getActiveObject()) {
    canvas.getActiveObject().set("opacity", $("#shape-opacity").val()/100);
    canvas.renderAll();
  }
});
