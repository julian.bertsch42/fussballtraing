

function Addtext() {
  canvas.add(new fabric.IText('Schreibe hier', {
    left: 50,
    top: 100,
    fontFamily: $("#font-family").val(),
    fill: textColor,
    fontSize: $("#text-font-size").val()
  }));
}

document.getElementById('font-family').onchange = function() {
  canvas.getActiveObject().fontFamily = this.value;
  canvas.renderAll();
};

    $("#text-font-size").change(function() {
      canvas.getActiveObject().set("fontSize", this.value);
      canvas.renderAll();
    });


radios5 = document.getElementsByName("fonttype");  // wijzig naar button
for(var i = 0, max = radios5.length; i < max; i++) {
    radios5[i].onclick = function() {

        if(document.getElementById(this.id).checked == true) {
            if(this.id == "text-cmd-bold") {
                canvas.getActiveObject().set("fontWeight", "bold");
            }
            if(this.id == "text-cmd-italic") {
                canvas.getActiveObject().set("fontStyle", "italic");
            }
            if(this.id == "text-cmd-underline") {
                canvas.getActiveObject().set("textDecoration", "underline");
            }
    if(this.id == "text-cmd-linethrough") {
                canvas.getActiveObject().set("textDecoration", "line-through");
            }
    if(this.id == "text-cmd-overline") {
                canvas.getActiveObject().set("textDecoration", "overline");
            }



        } else {
            if(this.id == "text-cmd-bold") {
                canvas.getActiveObject().set("fontWeight", "");
            }
            if(this.id == "text-cmd-italic") {
                canvas.getActiveObject().set("fontStyle", "");
            }
            if(this.id == "text-cmd-underline") {
                canvas.getActiveObject().set("textDecoration", "");
            }
    if(this.id == "text-cmd-linethrough") {
                canvas.getActiveObject().set("textDecoration", "");
            }
            if(this.id == "text-cmd-overline") {
                canvas.getActiveObject().set("textDecoration", "");
            }
        }


        canvas.renderAll();
    }
}
