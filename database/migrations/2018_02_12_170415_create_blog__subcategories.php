<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogSubcategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog__subcategories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug',191)->unique();
            $table->integer('ordering')->nullable();
            $table->integer('color')->default(1);
            $table->string('headerimage')->nullable();
            $table->string('seotitle')->nullable();
            $table->text('seodescription')->nullable();
            $table->integer('category_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog__subcategories');
    }
}
