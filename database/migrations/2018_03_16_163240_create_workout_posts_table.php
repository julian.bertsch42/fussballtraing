<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkoutPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workout_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',77);
            $table->string('slug',191)->unique();
            $table->string('seo-title',77)->nullable();
            $table->text('body');
            $table->string('image')->nullable();
            $table->integer('subcategory_id');
            $table->integer('user_id');
            $table->integer('active')->default(0);
            $table->string('tags')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workout_posts');
    }
}
