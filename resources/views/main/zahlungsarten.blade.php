@extends('layouts.app')

@section('seo-title', 'Zahlungsarten')

@section('bc-title', 'Zahlungsarten')
@section('bc', 'Zahlungsarten')
@section('bc-1', 'Support')

@section('content')


    <div class="main-container">
       <section>
            <div class="container">
                <div class="row v-align-children">
                    <div class="col-md-7 col-sm-6 text-center mb-xs-24">
                        <img class="cast-shadow" alt="Screenshot" src="img/screenshot.jpg">
                    </div>
                    <div class="col-md-4 col-md-offset-1 col-sm-5 col-sm-offset-1">
                        <h3>PayPal</h3>
                        <p>
                            Foundry is your complete design toolkit, built from the ground up to be flexible, extensible and stylish. Building slick, contemporary sites has never been this easy!
                        </p>

                    </div>
                </div>

            </div>

        </section><section>
            <div class="container">
                <div class="row v-align-children">
                    <div class="col-md-7 col-sm-6 text-center mb-xs-24">
                        <img class="cast-shadow" alt="Screenshot" src="img/screenshot.jpg">
                    </div>
                    <div class="col-md-4 col-md-offset-1 col-sm-5 col-sm-offset-1">
                        <h3>Überweisung</h3>
                        <p>
                            Foundry is your complete design toolkit, built from the ground up to be flexible, extensible and stylish. Building slick, contemporary sites has never been this easy!
                        </p>

                    </div>
                </div>

            </div>

        </section><section>
            <div class="container">
                <div class="row v-align-children">
                    <div class="col-md-7 col-sm-6 text-center mb-xs-24">
                        <img class="cast-shadow" alt="Screenshot" src="img/screenshot.jpg">
                    </div>
                    <div class="col-md-4 col-md-offset-1 col-sm-5 col-sm-offset-1">
                        <h3>Auf Rechnung<br></h3>
                        <p>
                            Foundry is your complete design toolkit, built from the ground up to be flexible, extensible and stylish. Building slick, contemporary sites has never been this easy!
                        </p>

                    </div>
                </div>

            </div>

        </section>
    </div>


@endsection