@extends('layouts.app')

@section('seo-title', '10% Gutschrift und ein Platz als Beta-Tester sichern!')
@section('seo-description','Jetzt am Newsletter anmelden und mit uns durchstarten! Egal ob Updates, Neuigkeiten oder aktuelle Trainingsübungen. Mit uns auf der richtigen Seite!')

@section('content')


    <div class="main-container">
        <section class="cover fullscreen overlay parallax">
            <div class="background-image-holder">
                <img alt="image" class="background-image" src="img/home13.jpg">
            </div>
            <div class="container v-align-transform">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 col-sm-12 text-center">

                        {{--<script id="n2g_script">
                            !function(e,t,n,c,r,a,i){e.Newsletter2GoTrackingObject=r,e[r]=e[r]||function(){(e[r].q=e[r].q||[]).push(arguments)},e[r].l=1*new Date,a=t.createElement(n),i=t.getElementsByTagName(n)[0],a.async=1,a.src=c,i.parentNode.insertBefore(a,i)}(window,document,"script","https://static.newsletter2go.com/utils.js","n2g");
                            n2g('create', 'bnergzjb-qh67grmr-zxs');
                            n2g('subscribe:createForm');
                        </script>--}}


                    </div>
                </div>

            </div>

        </section>
    </div>


@endsection

@section('scripts')

    <script id="n2g_script">
        !function(e,t,n,c,r,a,i){e.Newsletter2GoTrackingObject=r,e[r]=e[r]||function(){(e[r].q=e[r].q||[]).push(arguments)},e[r].l=1*new Date,a=t.createElement(n),i=t.getElementsByTagName(n)[0],a.async=1,a.src=c,i.parentNode.insertBefore(a,i)}(window,document,"script","https://static.newsletter2go.com/utils.js","n2g");
        n2g('create', 'bnergzjb-qh67grmr-zxs');
        n2g('subscribe:createPopup', {}, 1);
    </script>

@endsection