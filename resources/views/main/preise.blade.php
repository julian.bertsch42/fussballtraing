@extends('layouts.app')

@section('seo-title', 'Preise und Tools Deiner individuellen Fußball-Software')
@section('seo-description','Hier erhältst Du eine Übersicht der Preise für Deine individuelle Fußballtrainer-Software. Buche das Software-Komplettpaket oder ausschließlich die Tools, die Du für das Management Deines Vereins oder Deiner Mannschaft benötigst.')

@section('bc-title', 'Preise')
@section('bc', 'Preise')
@section('bc-1', 'Support')

@section('content')


    <div class="main-container">
        <section>
            <div class="container">
                <div class="row v-align-children">
                    <div class="col-sm-5">
                        <h3>Fussballtraining FREE<br></h3>
                        <p class="lead mb40">Mit unserer kostenlose Trainingsdatenbank, haben Sie hunderte Übungen, ohne zusätzlich was zu bezahlen.<br></p>
                        <div class="overflow-hidden mb32 mb-xs-24">
                            <i class="ti-package icon icon-sm pull-left"></i>
                            <h6 class="uppercase mb0 inline-block p32">Mehr als hunderte fussballübungen<br></h6>
                        </div>
                        <div class="overflow-hidden mb32 mb-xs-24">
                            <i class="ti-medall-alt icon icon-sm pull-left"></i>
                            <h6 class="uppercase mb0 inline-block p32">kostenlose funktion<br></h6>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="pricing-table pt-1 text-center emphasis">
                            <h5 class="uppercase">FreE<br></h5>
                            <span class="price">0€</span>
                            <p class="lead">Jährlich oder monatlich<br></p>
                            <a class="btn btn-white btn-lg" href="#">JETZT TESTEN</a>
                            <p>
                                <a href="#">Login</a> <br>3 Tage kostenlos testen<br></p>
                        </div>

                    </div>
                </div>

            </div>

        </section>

        <section>
            <div class="container">
                <div class="row v-align-children">
                    <div class="col-sm-5">
                        <h3>Fussballtraining Premium<br></h3>
                        <p class="lead mb40">Sie wollen noch mehr Fussballübungen? Dann ist die Premium Version genau das was du suchst. Mit mehr als tausenden Trainingsübungen ist für jeden was dabei. Viele Übungen sind inklusive Videos.<br></p>
                        <div class="overflow-hidden mb32 mb-xs-24">
                            <i class="ti-package icon icon-sm pull-left"></i>
                            <h6 class="uppercase mb0 inline-block p32">Mehr als tausende fussballübungen<br></h6>
                        </div>
                        <div class="overflow-hidden mb32 mb-xs-24">
                            <i class="ti-medall-alt icon icon-sm pull-left"></i>
                            <h6 class="uppercase mb0 inline-block p32">viele übungen mit videos<br></h6>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="pricing-table pt-1 text-center emphasis">
                            <h5 class="uppercase">Premium</h5>
                            <span class="price">Später</span>
                            <p class="lead">Jährlich oder monatlich<br></p>
                            <a class="btn btn-white btn-lg" href="#">JETZT testen</a>
                            <p>Login<br>3 Tage kostenlos testen</p>
                        </div>

                    </div>
                </div>

            </div>

        </section>

        <section>
            <div class="container">
                <div class="row v-align-children">
                    <div class="col-sm-5">
                        <h3>Fussballtraining Kurse<br></h3>
                        <p class="lead mb40">Mit unseren Fussballtraining Kurse erhalten Sie nur Zugang zu Trainingsinhalten, wenn Sie vorab von unseren lizenzierten und ausgebildeten DFB Trainern die Prüfung bestehen. Die Kurse sind kostenlos und nach bestandener Prüfung erhalten Sie von uns ein Zertifikat.<br></p>
                        <div class="overflow-hidden mb32 mb-xs-24">
                            <i class="ti-package icon icon-sm pull-left"></i>
                            <h6 class="uppercase mb0 inline-block p32">kostenlose ausbildung<br></h6>
                        </div>
                        <div class="overflow-hidden mb32 mb-xs-24">
                            <i class="ti-medall-alt icon icon-sm pull-left"></i>
                            <h6 class="uppercase mb0 inline-block p32">zertifikat bei bestandener prüfung<br></h6>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="pricing-table pt-1 text-center emphasis">
                            <h5 class="uppercase">Kurse</h5>
                            <span class="price">Später</span>
                            <p class="lead">Jährlich oder monatlich<br></p>
                            <a class="btn btn-white btn-lg" href="#">Jetzt testen</a>
                            <p>
                                <a href="#">Login<br>3 Tage kostenlos testen</a></p>
                        </div>

                    </div>
                </div>

            </div>

        </section>

        <section>
            <div class="container">
                <div class="row v-align-children">
                    <div class="col-sm-5">
                        <h3>Autogrammkarten</h3>
                        <p class="lead mb40">Neue Sponsoren gewinnen! Neues Merchandise für Vereine und Mannschaften. So werden auch die kleinen zu großen Stars!<br></p>
                        <div class="overflow-hidden mb32 mb-xs-24">
                            <i class="ti-package icon icon-sm pull-left"></i>
                            <h6 class="uppercase mb0 inline-block p32">viele auswahl an designs<br></h6>
                        </div>
                        <div class="overflow-hidden mb32 mb-xs-24">
                            <i class="ti-medall-alt icon icon-sm pull-left"></i>
                            <h6 class="uppercase mb0 inline-block p32">neue sponsoren gewinnen<br></h6>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="pricing-table pt-1 text-center emphasis">
                            <h5 class="uppercase">premium</h5>
                            <span class="price">Später</span>
                            <p class="lead">Jährlich oder monatlich<br></p>
                            <a class="btn btn-white btn-lg" href="#">jetzt testen </a>
                            <p>
                                <a href="#">Login<br>3 Tage kostenlos testen</a></p>
                        </div>

                    </div>
                </div>

            </div>

        </section>

        <section>
            <div class="container">
                <div class="row v-align-children">
                    <div class="col-sm-5">
                        <h3>Ernährungsportal Später<br></h3>
                        <p class="lead mb40">Füge deinen Spielern Ernährungsplänen hinzu und setze Ziele. Erkundige dich im Blog über Neuigkeiten und mache deine Mannschaft fit!<br></p>
                        <div class="overflow-hidden mb32 mb-xs-24">
                            <i class="ti-package icon icon-sm pull-left"></i>
                            <h6 class="uppercase mb0 inline-block p32">Viele ernährungspläne<br></h6>
                        </div>
                        <div class="overflow-hidden mb32 mb-xs-24">
                            <i class="ti-medall-alt icon icon-sm pull-left"></i>
                            <h6 class="uppercase mb0 inline-block p32">kostenlose funktion<br></h6>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="pricing-table pt-1 text-center emphasis">
                            <h5 class="uppercase">Später</h5>
                            <span class="price">Später</span>
                            <p class="lead">Jährlich oder monatlich<br></p>
                            <a class="btn btn-white btn-lg" href="#">jetzt testen</a>
                            <p>
                                <a href="#">Login<br>3 Tage kostenlos testen</a></p>
                        </div>

                    </div>
                </div>

            </div>

        </section>

        <section>
            <div class="container">
                <div class="row v-align-children">
                    <div class="col-sm-5">
                        <h3>Verletzungsportal Später<br></h3>
                        <p class="lead mb40">Welche Verletzungsarten gibt es? Wie kann man schnell handeln bei einer aktuellen Verletzung? Ihre Spieler haben eine Verletzungshistorie, somit sehen Sie welcher Spieler wie oft verletzt war.<br></p>
                        <div class="overflow-hidden mb32 mb-xs-24">
                            <i class="ti-package icon icon-sm pull-left"></i>
                            <h6 class="uppercase mb0 inline-block p32">schnelle informationshilfe<br></h6>
                        </div>
                        <div class="overflow-hidden mb32 mb-xs-24">
                            <i class="ti-medall-alt icon icon-sm pull-left"></i>
                            <h6 class="uppercase mb0 inline-block p32">kostenlose funktion<br></h6>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="pricing-table pt-1 text-center emphasis">
                            <h5 class="uppercase">Später</h5>
                            <span class="price">Später</span>
                            <p class="lead">Jährlich oder monatlich<br></p>
                            <a class="btn btn-white btn-lg" href="#">jetzt testen</a>
                            <p>
                                <a href="#">Login<br>3 Tage kostenlos testen</a></p>
                        </div>

                    </div>
                </div>

            </div>

        </section>


    </div>


@endsection