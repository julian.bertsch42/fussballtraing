@extends('layouts.app')

@section('seo-title', 'FAQ')

@section('bc-title', 'Häufig gestellte Fragen')
@section('bc', 'FAQ')
@section('bc-1', 'Support')

@section('content')


    <div class="main-container">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h4 class="mb16">Funktionen freischalten</h4>
                        <p class="lead mb64">
                            Demnächst
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <ul class="accordion accordion-2 one-open">
                            <li>
                                <div class="title">
                                    <h4 class="inline-block mb0">Demnächst</h4>
                                </div>
                                <div class="content">
                                    <p>
                                        Demnächst
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="title">
                                    <h4 class="inline-block mb0">Demnächst</h4>
                                </div>
                                <div class="content">
                                    <p>
                                        Demnächst
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="title">
                                    <h4 class="inline-block mb0">Demnächst</h4>
                                </div>
                                <div class="content">
                                    <p>
                                        Demnächst
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="title">
                                    <h4 class="inline-block mb0">Demnächst</h4>
                                </div>
                                <div class="content">
                                    <p>
                                        Demnächst
                                    </p>
                                </div>
                            </li>
                        </ul>

                    </div>
                </div>

            </div>

        </section><section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h4 class="mb16">Zahlungen</h4>
                        <p class="lead mb64">
                            Demnächst
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <ul class="accordion accordion-2 one-open">
                            <li>
                                <div class="title">
                                    <h4 class="inline-block mb0">Demnächst</h4>
                                </div>
                                <div class="content">
                                    <p>
                                        Demnächst
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="title">
                                    <h4 class="inline-block mb0">Demnächst</h4>
                                </div>
                                <div class="content">
                                    <p>
                                        Demnächst
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="title">
                                    <h4 class="inline-block mb0">Demnächst</h4>
                                </div>
                                <div class="content">
                                    <p>
                                        Demnächst
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="title">
                                    <h4 class="inline-block mb0">Demnächst</h4>
                                </div>
                                <div class="content">
                                    <p>
                                        Demnächst
                                    </p>
                                </div>
                            </li>
                        </ul>

                    </div>
                </div>

            </div>

        </section>
    </div>


@endsection