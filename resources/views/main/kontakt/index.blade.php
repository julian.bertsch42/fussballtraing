@extends('layouts.app')

@section('seo-title', 'Kontaktformular')

@section('bc-title', 'Kontakt')
@section('bc', 'Kontakt')
@section('bc-1', 'Support')

@section('content')


    <div class="main-container">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-5">
                        <h4 class="uppercase">Kontaktformular<br></h4>
                        <p>Schreib uns, egal welches Anliegen du hast. Wir melden uns aufjedenfall bei dir. Kritik ist wünschenswert um für dich dir optimale Plattform zu schaffen.<br></p>
                        <hr>
                        <p>
                            <strong>E-Mail:</strong> support@fussballtrainer-software.de<br>
                        </p>
                    </div>
                    <div class="col-sm-6 col-md-5 col-md-offset-1">
                        <form method="POST" action="{{route('main.kontakt.send')}}">
                            {{csrf_field()}}
                            <input required name="name" placeholder="Dein Name" type="text">
                            <input required name="email" placeholder="Email Addresse" type="email">
                            <textarea required name="message" rows="4" placeholder="Nachricht"></textarea>
                            <button type="submit">Sende Nachricht</button>
                        </form>

                        @if(isset($status))

                            <div class="alert alert-{{$status[0] == 1 ? 'danger' : 'success'}}">

                                <p>{{$status[1]}}</p>

                            </div>

                        @endif

                        @if ($errors->has('email'))
                            <div class="alert alert-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                        @endif

                    </div>
                </div>

            </div>

        </section>
    </div>


@endsection

