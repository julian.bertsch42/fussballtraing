@extends('layouts.app')

@section('seo-title', 'Impressum')

@section('bc-title', 'Impressum')
@section('bc', 'Impressum')
@section('bc-1', 'Rechtliches')

@section('content')


    <div class="main-container">
        <section>
            <div class="container">
                <div class="feed-item mb96 mb-xs-48">
                    <div class="row mb16 mb-xs-0">
                        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">

                            <h3>Impressum</h3>
                        </div>
                    </div>

                    <div class="row mb32 mb-xs-16">
                        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">


                            <p class="lead">
                                Fussballtrainer-Software<br><br>
                                Vertreten durch:<br>
                                Michele Finkenstein<br>
                                Im Vogelsang 102<br>
                                45527 Hattingen<br><br>

                                E-Mail: <a href="mailto://support@fussballtrainer-software.de">support@fussballtrainer-software.de</a><br><br>

                                Der Inhalt dieser Website ist urheberrechtlich geschützt. Nachdruck, Aufnahme in Online-Dienste, Internet und Vervielfältigung auf Datenträger wie CD-ROM, DVD-ROM usw. dürfen, auch auszugsweise, nur nach vorheriger schriftlicher Zustimmung nur durch Fussballtrainer Software erfolgen. Eine kommerzielle Weitervermarktung des Inhalts ist untersagt. Fussballtrainer Software haftet nicht für unverlangt eingesandte Inhalte, Manuskripte und Fotos. Für Inhalte externer Links und fremde Inhalte übernimmt Fussballtrainer Software keine Verantwortung.<br><br>Die Europäische Kommission stellt eine Plattform zur Online-Streitbeilegung (OS) bereit, die Sie hier finden http://ec.europa.eu/consumers/odr/. Wir sind bereit, an einem außergerichtlichen Schlichtungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.<br><br>Zuständig ist die Allgemeine Verbraucherschlichtungsstelle des Zentrums für Schlichtung e.V., Straßburger Straße 8, 77694 Kehl am Rhein, www.verbraucher-schlichter.de.<br><br>Nachweis der Bilder: <br>www.pixabay.com <br><br>Designed by Freepik   </p>
                        </div>
                    </div>


                </div>

            </div>
        </section>
    </div>


@endsection
