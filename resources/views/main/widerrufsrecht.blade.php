@extends('layouts.app')

@section('seo-title', 'Widerrufsrecht')

@section('bc-title', 'Widerrufsrecht')
@section('bc', 'Widerrufsrecht')
@section('bc-1', 'Rechtliches')

@section('content')


    <div class="main-container">
        <section>
            <div class="container">
                <div class="feed-item mb96 mb-xs-48">
                    <div class="row mb16 mb-xs-0">
                        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">

                            <h1>Widerrufsrecht für die Fussballtrainer-Software<br></h1>
                        </div>
                    </div>

                    <div class="row mb32 mb-xs-16">
                        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">

                            <p class="lead"><h2>1.Widerrufsrecht</h2>Für den Nutzer gelten für Ihn als Verbraucher im Sinne des Gesetzes (§13 BGB) folgende Bestimmungen: Die 14 tägige Frist beginnt direkt nach Rechnungserhalt(Datum) auf der Fussballtrainer Software. Das Datum der Registrierung ist unter Einstellungen Daten verwalten einzusehen. Nach Ablauf der 14 tägigen Frist erlischt der Widerruf.
                            Der Nutzer kann innerhalb von 2 Wochen(14 Tage) nach Rechnungsdatum gegenüber von der Fussballtrainer Software den Vertrag schriftlich widerrufen. Die Kündigung bedarf der Schriftform oder per E-Mail! Per Post bitte an folgende Adresse schicken
                            <br><br>Fussballtrainer Software
                            <br>support@fussballtrainer-software.de <br><br>
                            Die Kosten für den Versand sind vom Nutzer zu tragen und können nicht erstattet werden. Der Widerruf kann ohne Angaben von Gründen widerrufen werden.<br><br><h2>2.Widerrufsfolgen</h2>Im Falle eines wirksamen Widerrufs, sind beide empfangene Leistungen zurückzugewähren. Es entstehen keine Kosten für den Nutzer. Die Fussballtrainer Software ist ab Erhalt des wirksamen Widerrufs verpflichtet innerhalb von 30 tagen das Geld dem Nutzer zu überweisen. Die Frist für den Nutzer beginnt mit dem Absenden des Widerrufes. Der Widerruf erlischt vorzeitig nach 14 Tagen ab Rechnungsdatum und von beiden Seiten ausdrücklich und vollständig erfüllt ist, bevor Sie Ihren Widerrufrechts in Anspruch genommen haben.<br><br><h2>Widerrufsrecht für Kundenspezifische Aufträge</h2> <br><br><h2>1.Ausschluss des Widerrufs</h2> Eine Vielzahl der von Fussballtrainer Software angebotenen Produkte werden nach Spezifikation des Kunden angefertigt. Diese Kundenspezifische Produkte sind vom Recht des Verbrauchers zum Widerruf eines Fernabsatzvertrages gemäß § 312 d Abs. 4 Nr. 1 BGB ausgeschlossen. Da es sich bei unseren Produkten um nicht körperlichen Datenträgern handelt, haben Sie kein Anspruch auf ein 14 tägiges Widerrufsrecht. <br> <br>
                            Sie werden bei jeder Bestellung die Sie aufgeben, extra darauf hingewiesen, dass Sie auf Ihr Widerrufsrecht verzichten.<br><br>
                            Der Verbraucher erhält von uns auf einen dauerhaften Datenträger(E-Mail oder digitaler Papierform) eine Bestätigung auf den Verzicht des Widerrufs. Die Bestätigung muss festhalten , dass der Verbraucher vor Ausführung des Vertrages a) ausdrücklich zugestimmt hat, dass der Unternehmer mit der Ausführung des Vertrags vor Ablauf der Widerrufsfrist beginnt, und b) seine Kenntnis davon bestätigt hat, dass er durch seine Zustimmung mit Beginn der Ausführung des Vertrages sein Widerrufsrecht verliert.<br><br>
                            Für alle anderen Produkte besteht das nachfolgende Widerrufsrecht.</p>

                        </div>
                    </div>


                </div>

            </div>
        </section>
    </div>


@endsection