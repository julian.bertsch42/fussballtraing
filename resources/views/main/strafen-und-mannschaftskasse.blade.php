@extends('layouts.app')

@section('seo-title', 'Strafen und Mannschaftskasse')

@section('bc-title', 'Strafen und Mannschaftskasse')
@section('bc', 'Strasen und Mannschaftskasse')
@section('bc-1', 'Funktionen')

@section('content')


    <div class="main-container">
        <section class="image-bg bg-light parallax overlay pt160 pb160 pt-xs-80 pb-xs-80">
            <div class="background-image-holder">
                <img alt="image" class="background-image" src="img/intro1.jpg">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-sm-8">
                        <i class="{{--ti-quote-left --}}icon icon-sm mb16"></i>
                        {{--<h3>
                            Telefonliste
                        </h3>--}}
                        <h3 class="mb32">Erstelle unkompliziert eine Telefonliste für jede Vereinsmannschaft und ergänze individuelle Angaben wie Wohn- oder E-Mail-Adressen und exportiere die Liste als PDF.</h3>
                    </div>
                </div>

            </div>

        </section>

        <section class="bg-secondary">
            <div class="container">
                <div class="row v-align-children">
                    <div class="col-md-12 col-sm-6 text-center mb-xs-24">
                        <div class="local-video-container">
                            <div class="background-image-holder">
                                <img alt="Background Image" class="background-image" src="{{asset('bilder/coming-soon.png')}}">
                            </div>
                            <video controls="">
                                <source src="video/video.webm" type="video/webm">
                                <source src="video/video.mp4" type="video/mp4">
                                <source src="video/video.ogv" type="video/ogg">
                            </video>
                            {{--<iframe width="560" height="315" src="https://www.youtube.com/embed/2OjL8u_4pFM" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                            <div class="play-button"></div>--}}
                        </div>

                    </div>
                    <div class="col-md-4 col-md-offset-1 col-sm-5 col-sm-offset-1">
                        <h3>Neugierig geworden?</h3>
                        <p>
                            ...dann schau dir unser Video an!
                        </p>
                    </div>
                </div>

            </div>

        </section>

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h4 class="uppercase mb16">Überzeuge dich selbst!</h4>
                        <p class="lead mb64">
                            Klicke ein Bild an, um es zu vergrößern
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="lightbox-grid square-thumbs" data-gallery-title="Gallery">
                            <ul>
                                <li>
                                    <a href="{{asset('bilder/coming-soon.png')}}" data-lightbox="true">
                                        <div class="background-image-holder">
                                            <img alt="image" class="background-image" src="{{asset('bilder/coming-soon.png')}}">
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{asset('bilder/coming-soon.png')}}" data-lightbox="true">
                                        <div class="background-image-holder">
                                            <img alt="image" class="background-image" src="{{asset('bilder/coming-soon.png')}}">
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{asset('bilder/coming-soon.png')}}" data-lightbox="true">
                                        <div class="background-image-holder">
                                            <img alt="image" class="background-image" src="{{asset('bilder/coming-soon.png')}}">
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{asset('bilder/coming-soon.png')}}" data-lightbox="true">
                                        <div class="background-image-holder">
                                            <img alt="image" class="background-image" src="{{asset('bilder/coming-soon.png')}}">
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{asset('bilder/coming-soon.png')}}" data-lightbox="true">
                                        <div class="background-image-holder">
                                            <img alt="image" class="background-image" src="{{asset('bilder/coming-soon.png')}}">
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{asset('bilder/coming-soon.png')}}" data-lightbox="true">
                                        <div class="background-image-holder">
                                            <img alt="image" class="background-image" src="{{asset('bilder/coming-soon.png')}}">
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{asset('bilder/coming-soon.png')}}" data-lightbox="true">
                                        <div class="background-image-holder">
                                            <img alt="image" class="background-image" src="{{asset('bilder/coming-soon.png')}}">
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{asset('bilder/coming-soon.png')}}" data-lightbox="true">
                                        <div class="background-image-holder">
                                            <img alt="image" class="background-image" src="{{asset('bilder/coming-soon.png')}}">
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>

            </div>

        </section>


    </div>


@endsection