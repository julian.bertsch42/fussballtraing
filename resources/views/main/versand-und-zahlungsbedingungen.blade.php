@extends('layouts.app')

@section('seo-title', 'Versand- und Zahlungsbedingungen')

@section('bc-title', 'Versand und Zahlungsbedingungen')
@section('bc', 'Versand und Zahlungsbedingungen')
@section('bc-1', 'Support')

@section('content')


    <div class="main-container">
        <section>
            <div class="container">
                <div class="feed-item mb96 mb-xs-48">
                    <div class="row mb16 mb-xs-0">
                        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">

                            <h1>Versand und Zahlungsbedingungen für die Fussballtrainer Software<br></h1>
                        </div>
                    </div>

                    <div class="row mb32 mb-xs-16">
                        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">

                            <p class="lead"><h2>1.Zahlungsmöglichkeiten</h2>Für die Freischaltung des Accounts innerhalb Deutschlands, bieten wir folgende Zahlungsmöglichkeiten an, sofern in der jeweiligen Produktinfo nichts anderes angegeben ist:

                            <br><br>– Vorauskasse per Überweisung
                            <br>– Auf Rechnung<br><br><h2>2.Lieferung</h2>2.1. Die Freischaltung erfolgt manuell nach Zahlungseingang durch die Fussballtrainer Software.

                            <br><br>- Die Freischaltung erfolgt weltweit <br><br><h2>3.Versandkosten</h2>Es fallen keine Kosten für die Freischaltung an.</p>

                        </div>
                    </div>


                </div>

            </div>
        </section>
    </div>


@endsection