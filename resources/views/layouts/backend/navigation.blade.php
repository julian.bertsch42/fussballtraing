<!-- #HEADER -->
<header id="header">
    <div id="logo-group">

        <!-- PLACE YOUR LOGO HERE -->
        <span id="logo"> <img src="{{asset('vendor/backend/img/logo.png')}}" alt="SmartAdmin"> </span>
        <!-- END LOGO PLACEHOLDER -->

    </div>

    <!-- #TOGGLE LAYOUT BUTTONS -->
    <!-- pulled right: nav area -->
    <div class="pull-right">

        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
            <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->

        <!-- #MOBILE -->
        <!-- Top menu profile link : this shows only when top menu is active -->
        <ul id="mobile-profile-img" class="header-dropdown-list hidden-xs padding-5">
            <li class="">
                <a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown">
                    <img src="{{asset('vendor/backend/img/avatars/sunny.png')}}" alt="{{Auth::User()->name}}" class="online" />
                </a>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0"><i class="fa fa-cog"></i> Setting</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="profile.html" class="padding-10 padding-top-0 padding-bottom-0"> <i class="fa fa-user"></i> <u>P</u>rofile</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="toggleShortcut"><i class="fa fa-arrow-down"></i> <u>S</u>hortcut</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="launchFullscreen"><i class="fa fa-arrows-alt"></i> Full <u>S</u>creen</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="login.html" class="padding-10 padding-top-5 padding-bottom-5" data-action="userLogout"><i class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong></a>
                    </li>
                </ul>
            </li>
        </ul>

        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
            <span>
                <a href="{{ route('logout') }}"onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i></a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </span>
        </div>
        <!-- end logout button -->

        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
        </div>
        <!-- end fullscreen button -->

    </div>
    <!-- end pulled right: nav area -->

</header>
<!-- END HEADER -->

<!-- #NAVIGATION -->
<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it -->

					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
						<img src="{{asset('vendor/backend/img/avatars/sunny.png')}}" alt="me" class="online" />
						<span>
							{{Auth::User()->username}}
						</span>
						<i class="fa fa-angle-down"></i>
					</a>

				</span>
    </div>
    <!-- end user info -->

    <nav>

        <ul>

            @if(auth::user()->isAdmin())
                <li>
                    <a href="#" title="Administration"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Administration</span></a>
                    <ul>

                        <li>
                            <a href="#" title="Einstellungen"><span class="menu-item-parent">Einstellungen</span></a>
                            <ul>
                                <li>
                                    <a href="#" title="System"><span class="menu-item-parent">System</span></a>
                                    <ul>
                                        <li class="{{Navigation::isActiveRoute(['backend.admin.settings.system.newsticker'],true)}}">
                                            <a href="{{route('backend.admin.settings.system.newsticker.edit')}}" title="Blog"><span class="menu-item-parent">Newsticker</span></a>
                                        </li>
                                        <li class="{{Navigation::isActiveRoute(['backend.admin.settings.system.blog-background'],true)}}">
                                            <a href="{{route('backend.admin.settings.system.blog-background.edit',1)}}" title="Blog-Hintergund"><span class="menu-item-parent">Blog-Hintergund</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" title="Funktionen"><span class="menu-item-parent">Funktionen</span></a>
                                    <ul>

                                        <li>
                                            <a href="#" title="Blog"><span class="menu-item-parent">Blog</span></a>
                                            <ul>
                                                <li class="{{Navigation::isActiveRoute(['backend.admin.settings.functions.blog.categories'],true)}}">
                                                    <a href="{{route('backend.admin.settings.functions.blog.categories.index')}}" title="Blog"><span class="menu-item-parent">Kategorien</span></a>
                                                </li>
                                                <li class="{{Navigation::isActiveRoute(['backend.admin.settings.functions.blog.subcategories'],true)}}">
                                                    <a href="{{route('backend.admin.settings.functions.blog.subcategories.index')}}" title="Blog"><span class="menu-item-parent">Unterkategorien</span></a>
                                                </li>

                                            </ul>
                                        </li>

                                        <li>
                                            <a href="#" title="Trainingsübungen"><span class="menu-item-parent">Trainingsübungen</span></a>
                                            <ul>
                                                <li class="{{Navigation::isActiveRoute(['backend.admin.settings.functions.workout.categories'],true)}}">
                                                    <a href="{{route('backend.admin.settings.functions.workout.categories.index')}}" title="Blog"><span class="menu-item-parent">Kategorien</span></a>
                                                </li>
                                                <li class="{{Navigation::isActiveRoute(['backend.admin.settings.functions.workout.subcategories'],true)}}">
                                                    <a href="{{route('backend.admin.settings.functions.workout.subcategories.index')}}" title="Blog"><span class="menu-item-parent">Unterkategorien</span></a>
                                                </li>

                                            </ul>
                                        </li>

                                    </ul>
                                </li>
                            </ul>
                        </li>





                        <li class="{{Navigation::isActiveRoute(['backend.admin.users'],true)}}">
                            <a href="{{route('backend.admin.users.index')}}" title="Benutzer"><span class="menu-item-parent">Benutzer</span></a>
                        </li>

                        <li>
                            <a href="#" title="Vereinsverzeichnis"><span class="menu-item-parent">Vereinsverzeichnis</span></a>
                            <ul>
                                <li class="{{Navigation::isActiveRoute(['backend.admin.submissions'],true)}}">
                                    <a href="{{route('backend.admin.submissions.index')}}">Vorschläge</a>
                                </li>
                                <li class="{{Navigation::isActiveRoute(['backend.admin.clubs'],true)}}">
                                    <a href="{{route('backend.admin.clubs.index')}}">Vereine</a>
                                </li>
                                <li class="{{Navigation::isActiveRoute(['backend.admin.associations'],true)}}">
                                    <a href="{{route('backend.admin.associations.index')}}">Verbände</a>
                                </li>
                            </ul>
                        </li>


                    </ul>
                </li>

            @endif
            <li>
                <a href="#" title="Blog"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Blog</span></a>
                <ul>

                    <li>
                        <a href="#" title="Beiträge"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Beiträge</span></a>
                        <ul>
                            <li class="{{Navigation::isActiveRoute(['backend.blog.posts.index']).Navigation::isActiveRoute(['backend.blog.posts.create', 'backend.blog.posts.edit'],true)}}">
                                <a href="{{route('backend.blog.posts.index')}}" title="Alles"><span class="menu-item-parent">Alles</span></a>
                            </li>

                            @foreach($blogCategories as $cat)

                                <li>
                                    <a href="#" title="{{$cat->name}}"><span class="menu-item-parent">{{$cat->name}}@if($cat->unactive_posts_count > 0) <span class="badge">{{$cat->unactive_posts_count}}</span>@endif</span></a>
                                    <ul>
                                        @foreach($cat->childs->sortBy('ordering') as $subCat)
                                            <li class="{{Navigation::isActiveRoute(['backend.blog.posts.index' => [$subCat->id]])}}">
                                                <a href="{{route('backend.blog.posts.index',$subCat->id)}}" title="{{$subCat->name}}"><span class="menu-item-parent">{{$subCat->name}}@if($subCat->unactive_posts_count > 0) <span class="badge">{{$subCat->unactive_posts_count}}</span>@endif</span></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>

                            @endforeach

                        </ul>
                    </li>

                </ul>
            </li>

                <li>
                    <a href="#" title="Blog"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Trainingsübungen</span></a>
                    <ul>

                        <li>
                            <a href="#" title="Beiträge"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Beiträge</span></a>
                            <ul>
                                <li class="{{Navigation::isActiveRoute(['backend.workout.posts.index']).Navigation::isActiveRoute(['backend.workout.posts.create', 'backend.workout.posts.edit'],true)}}">
                                    <a href="{{route('backend.workout.posts.index')}}" title="Alles"><span class="menu-item-parent">Alles</span></a>
                                </li>

                                @foreach($workoutCategories as $cat)

                                    <li>
                                        <a href="#" title="{{$cat->name}}"><span class="menu-item-parent">{{$cat->name}}@if($cat->unactive_posts_count > 0) <span class="badge">{{$cat->unactive_posts_count}}</span>@endif</span></a>
                                        <ul>
                                            @foreach($cat->childs->sortBy('ordering') as $subCat)
                                                <li class="{{Navigation::isActiveRoute(['backend.workout.posts.index' => [$subCat->id]])}}">
                                                    <a href="{{route('backend.workout.posts.index',$subCat->id)}}" title="{{$subCat->name}}"><span class="menu-item-parent">{{$subCat->name}}@if($subCat->unactive_posts_count > 0) <span class="badge">{{$subCat->unactive_posts_count}}</span>@endif</span></a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>

                                @endforeach

                            </ul>
                        </li>

                    </ul>
                </li>


        </ul>
    </nav>


    <span class="minifyme" data-action="minifyMenu">
				<i class="fa fa-arrow-circle-left hit"></i>
			</span>

</aside>
<!-- END NAVIGATION -->