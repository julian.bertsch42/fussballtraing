@include('layouts.shared.footer')
<!-- footer -->
<!-- Vendors (All Essential JavaScript plugins) -->

<!-- bootstrap -->
<script type="text/javascript" src="{{asset('vendor/blog/js/vendor/bootstrap.min.js')}}"></script>
<!-- magnific -->
<script type="text/javascript" src="{{asset('vendor/blog/js/vendor/magnific.popup.min.js')}}"></script>
<!-- slick -->
<script type="text/javascript" src="{{asset('vendor/blog/js/vendor/slick.min.js')}}"></script>
<!-- slicknav -->
<script type="text/javascript" src="{{asset('vendor/blog/js/vendor/slicknav.min.js')}}"></script>
<!-- masonry -->
<script type="text/javascript" src="{{asset('vendor/blog/js/vendor/masonry.pkgd.min.js')}}"></script>
<!-- web ticker -->
<script type="text/javascript" src="{{asset('vendor/blog/js/vendor/web.ticker.min.js')}}"></script>
<!-- active -->
<script type="text/javascript" src="{{asset('vendor/blog/js/active.js')}}"></script>

<script type="text/javascript" src="{{asset('vendor/lightbox/js/lightbox.js')}}"></script>

