<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    <!-- SEO -->
    <title>@yield('seo-title','Fußballvereine suchen in Deutschlands größtem Vereinsverzeichnis')</title>
    <meta name="description" content="@yield('seo-description','Deutschlands größtes Verzeichnis für Fußballvereine. ✔ Umfassende Vereinsinformationen ✔ Kreisklasse bis Bundesliga ✔ Routenplaner und Adressen')">
    <meta property="og:image" content="{{asset('vendor/vereinsverzeichnis/images/seoheader.jpg')}}">
    <meta property="og:title" content="@yield('seo-title','Fußballvereine suchen in Deutschlands größtem Vereinsverzeichnis')">
    <meta property="og:url" content="{{url()->current()}}">
    <meta property="og:description" content="@yield('seo-description','Deutschlands größtes Verzeichnis für Fußballvereine. ✔ Umfassende Vereinsinformationen ✔ Kreisklasse bis Bundesliga ✔ Routenplaner und Adressen')">
    <meta property="og:type" content="website"/>
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@yield('twitter','@Platzhalter')">
    <meta name="twitter:creator" content="@Platzhalter">
    <meta name="twitter:title" content="@yield('seo-title','Fußballvereine suchen in Deutschlands größtem Vereinsverzeichnis')">
    <meta name="twitter:description" content="@yield('seo-description','Deutschlands größtes Verzeichnis für Fußballvereine. ✔ Umfassende Vereinsinformationen ✔ Kreisklasse bis Bundesliga ✔ Routenplaner und Adressen')">
    <meta name="twitter:url" content="{{env('APP_URL')}}">
    <meta name="twitter:image:src" content="{{asset('vendor/vereinsverzeichnis/images/seoheader.jpg')}}">
    <meta name="twitter:image:width " content="400">
    <meta name="twitter:image:height" content="200">
    <meta name="twitter:image" content="{{asset('vendor/vereinsverzeichnis/images/seoheader.jpg')}}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link href="{{ asset('vendor/vereinsverzeichnis/css/app.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    {!! NoCaptcha::renderJs() !!}

</head>
<body>
<div id="@yield('id')" class="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img class="pull-left" src="{{asset('vendor/vereinsverzeichnis/images/logo.png')}}" height="30px" style="margin: -4px 3px 0 0" />
                    <span>{{ config('app.name', 'Laravel') }}</span>
                </a>

            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="">Was ist das Vereinsverzeichnis?</a></li>
                    <li><a href="{{route('submissions.create')}}">Verein hinzufügen</a></li>
                    @yield('navigation_extra_right')
                    @auth
                        @if(Auth::User()->isAdmin())
                            <li><a href="{{route('backend.home')}}">Backend</a></li>
                        @endif
                    @endauth
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

</div>

@include('layouts.vereinsverzeichnis.footer')




</body>
</html>
