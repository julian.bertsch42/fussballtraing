<footer>

    <div class="container">
        <div class="row">

            <div class="col-md-6">
                <ul class="links">

                    <li>
                        <a href="">Impressum</a>
                    </li>
                    |
                    <li>
                        <a href="">Datenschutz</a>
                    </li>
                    |
                    <li>
                        <a href="">Nutzungsbedingungen</a>
                    </li>

                </ul>
            </div>


            <div class="col-md-6">
                <ul class="socialmedia">

                    <li>
                        <a href="https://www.facebook.com/fussballvereine/" target="_blank">
                            <i class="fa fa-facebook-square"></i>
                        </a>
                    </li>
                    <li>
                        <a href="" target="_blank">
                            <i class="fa fa-youtube-square"></i>
                        </a>
                    </li>

                </ul>
            </div>

        </div>
    </div>

</footer>

<!-- Scripts -->
<script src="{{ asset('vendor/vereinsverzeichnis/js/app.js') }}"></script>

@yield('scripts')