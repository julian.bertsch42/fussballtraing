<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('seo-title','Fussballtrainer-software.de - einfach gut organisiert!')</title>
    <meta name="description" content="@yield('seo-description','Die innovative Fußballtrainer-Software für Profi- und Amateurvereine. Professionelle Trainingsübungen | Verletzungs- und Ernährungsportal | Mannschaftsorganisation- und Management.')">
    <meta property="og:image" content="{{asset('vendor/main/bilder/fussballtrainer-software.jpg')}}">
    <meta property="og:title" content="@yield('seo-title','Fussballtrainer-software.de - einfach gut organisiert!')">
    <meta property="og:url" content="{{Request::url()}}">
    <meta property="og:description" content="@yield('seo-description','Die innovative Fußballtrainer-Software für Profi- und Amateurvereine. Professionelle Trainingsübungen | Verletzungs- und Ernährungsportal | Mannschaftsorganisation- und Management.')">
    <meta property="og:type" content="websiste"/>
    <link rel="publisher" href="https://plus.google.com/112379064220231236403" property="schema:url">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@#">
    <meta name="twitter:creator" content="@#">
    <meta name="twitter:title" content="@yield('seo-title','Fussballtrainer-software.de - einfach gut organisiert!')">
    <meta name="twitter:description" content="@yield('seo-description','Die innovative Fußballtrainer-Software für Profi- und Amateurvereine. Professionelle Trainingsübungen | Verletzungs- und Ernährungsportal | Mannschaftsorganisation- und Management.')">
    <meta name="twitter:url" content="{{Request::url()}}">
    <meta name="twitter:image:src" content="{{asset('vendor/main/bilder/fussballtrainer-software.jpg')}}">
    <meta name="twitter:image:width " content="400">
    <meta name="twitter:image:height" content="200">
    <meta name="twitter:image" content="@yield('seo-title','Fussballtrainer-software.de - einfach gut organisiert!')">
    <meta name="geo.region" content="DE-NW" />
    <meta name="geo.placename" content="Hattingen" />
    <meta name="geo.position" content="51.400301;7.214898" />
    <meta name="ICBM" content="51.400301, 7.214898" />
    <meta name="robots" content="noindex,nofollow"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- FAVICONS ANFANG -->
    <link rel="shortcut icon" href="{{asset('vendor/main/bilder/favicon.png')}}">
    <link rel="apple-touch-icon" href="{{asset('vendor/main/bilder/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('vendor/main/bilder/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('vendor/main/bilder/favicon.png')}}">
    <!-- FAVICONS ENDE -->

    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,400,700%7CNoto+Sans:400,700" rel="stylesheet">

    <!-- CSS ANFANG -->
    <link href="{{asset('vendor/main/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" media="all">
    <link href="{{asset('vendor/blog/css/material-design-iconic-font.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/main/css/themify-icons.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('vendor/main/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('vendor/main/css/theme.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('vendor/main/css/custom.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400%7CRaleway:100,400,300,500,600,700%7COpen+Sans:400,500,600' rel='stylesheet' type='text/css'>
    <link href="{{asset('vendor/footer/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
    <!-- SOCIAL MEDIA ANFANG -->
    <link href="{{asset('vendor/social-share/css/slideout-style.css')}}" rel="stylesheet" type="text/css">
    <!-- SOCIAL MEDIA ENDE -->
    <link rel="stylesheet" href="{{asset('vendor/lightbox/css/lightbox.min.css')}}">
    <!-- CSS Ende -->
    <!-- excel -->
    <script> var __basePath = ''; </script>
    <script src="https://unpkg.com/xlsx-style@0.8.13/dist/xlsx.full.min.js"></script>
    <script src="https://unpkg.com/ag-grid-community/dist/ag-grid-community.min.noStyle.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/ag-grid-community/dist/styles/ag-grid.css">
    <link rel="stylesheet" href="https://unpkg.com/ag-grid-community/dist/styles/ag-theme-balham.css">
    <style>
        html, body {
            width: 100%;
            height: 100%;
        }
    </style>
</head>