<!doctype html>
<html lang="de">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="@yield('seo-description','8 Alleinstellungsmerkmale')">
    <meta property="og:image" content="SEO Bild">
    <meta property="og:title" content="@yield('seo-title','Fussballtrainer Software')">
    <meta property="og:url" content="SEO Link">
    <meta property="og:description" content="@yield('seo-description','8 Alleinstellungsmerkmale')">
    <meta property="og:type" content="website"/>
    <link rel="publisher" href="Google Link" property="schema:url">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@#">
    <meta name="twitter:creator" content="@#">
    <meta name="twitter:title" content="@yield('seo-title','Fussballtrainer Software')">
    <meta name="twitter:description" content="@yield('seo-description','8 Alleinstellungsmerkmale')">
    <meta name="twitter:url" content="SEO Link Twitter">
    <meta name="twitter:image:src" content="SEO Bild Twitter">
    <meta name="twitter:image:width " content="400">
    <meta name="twitter:image:height" content="200">
    <meta name="twitter:image" content="SEO Bild Twitter">
    <meta name="geo.region" content="DE-NW" />
    <meta name="geo.placename" content="Hattingen" />
    <meta name="geo.position" content="51.400301;7.214898" />
    <meta name="ICBM" content="51.400301, 7.214898" />
    <meta name="google-site-verification" content="s-7YoeLdPFmzbctymsI7DWKSk7Xq4GqeI2S1ywwblio" />
    <meta name="robots" content="noindex,nofollow"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- FAVICONS ANFANG -->
    <link rel="shortcut icon" href="{{asset('img/favicon.png')}}">
    <link rel="apple-touch-icon" href="{{asset('img/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('img/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('img/favicon.png')}}">
    <!-- FAVICONS ENDE -->

    <title>404 - Page Not Found</title>

    <link rel="stylesheet" type="text/css" href="{{asset('vendor/error/css/style.css')}}" />
    <link rel="stylesheet" href="{{asset('vendor/error/font-awesome/css/font-awesome.min.css')}}">
    <script type="text/javascript" src="{{asset('vendor/error/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/error/js/wordsearch-resize.js')}}"></script>

    <script type="text/javascript">
        $(function() {
            /* 4 */ $(this).delay(1500).queue(function(){ $( ".one" ).addClass("selected"); $(this).dequeue(); })
            /* 0 */ .delay(500).queue(function(){ $( ".two" ).addClass("selected"); $(this).dequeue(); })
            /* 4 */ .delay(500).queue(function(){ $( ".three" ).addClass("selected"); $(this).dequeue(); })

            /* P */ .delay(500).queue(function(){ $( ".four" ).addClass("selected"); $(this).dequeue(); })
            /* A */ .delay(500).queue(function(){ $( ".five" ).addClass("selected"); $(this).dequeue(); })
            /* G */ .delay(500).queue(function(){ $( ".six" ).addClass("selected"); $(this).dequeue(); })
            /* E */ .delay(500).queue(function(){ $( ".seven" ).addClass("selected"); $(this).dequeue(); })

            /* N */ .delay(500).queue(function(){ $( ".eight" ).addClass("selected"); $(this).dequeue(); })
            /* O */ .delay(500).queue(function(){ $( ".nine" ).addClass("selected"); $(this).dequeue(); })
            /* T */ .delay(500).queue(function(){ $( ".ten" ).addClass("selected"); $(this).dequeue(); })

            /* F */ .delay(500).queue(function(){ $( ".eleven" ).addClass("selected"); $(this).dequeue(); })
            /* O */ .delay(500).queue(function(){ $( ".twelve" ).addClass("selected"); $(this).dequeue(); })
            /* U */ .delay(500).queue(function(){ $( ".thirteen" ).addClass("selected"); $(this).dequeue(); })
            /* N */ .delay(500).queue(function(){ $( ".fourteen" ).addClass("selected"); $(this).dequeue(); })
            /* D */ .delay(500).queue(function(){ $( ".fifteen" ).addClass("selected"); $(this).dequeue(); });


        });
    </script>
</head>

<body>

@yield('content')

</body>

</html>
