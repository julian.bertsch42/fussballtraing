<!doctype html>
<html lang="{{ app()->getLocale() }}">

@include('layouts.auth.header')

<body>

    @yield('content')

</body>

</html>