<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('seo-title','Fussballtrainer Software')</title>
    <meta name="description" content="@yield('seo-description','8 Alleinstellungsmerkmale')">
    <meta property="og:image" content="SEO Bild">
    <meta property="og:title" content="@yield('seo-title','Fussballtrainer Software')">
    <meta property="og:url" content="SEO Link">
    <meta property="og:description" content="@yield('seo-description','8 Alleinstellungsmerkmale')">
    <meta property="og:type" content="website"/>
    <link rel="publisher" href="Google Link" property="schema:url">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@#">
    <meta name="twitter:creator" content="@#">
    <meta name="twitter:title" content="@yield('seo-title','Fussballtrainer Software')">
    <meta name="twitter:description" content="@yield('seo-description','8 Alleinstellungsmerkmale')">
    <meta name="twitter:url" content="SEO Link Twitter">
    <meta name="twitter:image:src" content="SEO Bild Twitter">
    <meta name="twitter:image:width " content="400">
    <meta name="twitter:image:height" content="200">
    <meta name="twitter:image" content="SEO Bild Twitter">
    <meta name="geo.region" content="DE-NW" />
    <meta name="geo.placename" content="Hattingen" />
    <meta name="geo.position" content="51.400301;7.214898" />
    <meta name="ICBM" content="51.400301, 7.214898" />
    <meta name="google-site-verification" content="s-7YoeLdPFmzbctymsI7DWKSk7Xq4GqeI2S1ywwblio" />

    <!-- FAVICONS ANFANG -->
    <link rel="shortcut icon" href="{{asset('img/favicon.png')}}">
    <link rel="apple-touch-icon" href="{{asset('img/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('img/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('img/favicon.png')}}">
    <!-- FAVICONS ENDE -->

    <!-- CSS ANFANG -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css" media="all">
    <link href="{{asset('vendor/auth/css/alternative/app.css')}}" rel="stylesheet" type="text/css" media="all" />
    <!-- CSS Ende -->

</head>