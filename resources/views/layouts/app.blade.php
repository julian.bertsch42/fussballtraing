<!doctype html>
<html lang="{{ app()->getLocale() }}">

@include('layouts.header')

<body>

    @include('layouts.navigation')


    @yield('content')

    @include('layouts.shared.footer')

    <script src="{{asset('vendor/main/js/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/main/js/flexslider.min.js')}}"></script>
    <script src="{{asset('vendor/main/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('vendor/main/js/smooth-scroll.min.js')}}"></script>
    <script src="{{asset('vendor/main/js/parallax.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/main/js/lightbox.min.js')}}"></script>
    <script src="{{asset('vendor/main/js/scripts.js')}}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
    <script>
        window.addEventListener("load", function(){
            window.cookieconsent.initialise({
                "palette": {
                    "popup": {
                        "background": "#000"
                    },
                    "button": {
                        "background": "transparent",
                        "text": "#f1d600",
                        "border": "#f1d600"
                    }
                },
                "showLink": false,
                "content": {
                    "message": "Wir verwenden Cookies, um die Webseite bestmöglich an die Bedürfnisse unserer Besucher anpassen zu können. Wenn Sie auf der Seite weitersurfen stimmen Sie der Cookie-Nutzung zu",
                    "dismiss": "ICH STIMME ZU!"
                }
            })});
    </script>

    @yield('scripts')

</body>

</html>