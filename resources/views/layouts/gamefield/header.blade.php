<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('seo-title','Fussballtrainer-software.de - einfach gut organisiert!')</title>
    <meta name="description" content="@yield('seo-description','Die innovative Fußballtrainer-Software für Profi- und Amateurvereine. Professionelle Trainingsübungen | Verletzungs- und Ernährungsportal | Mannschaftsorganisation- und Management.')">
    <meta property="og:image" content="{{asset('vendor/main/bilder/fussballtrainer-software.jpg')}}">
    <meta property="og:title" content="@yield('seo-title','Fussballtrainer-software.de - einfach gut organisiert!')">
    <meta property="og:url" content="{{Request::url()}}">
    <meta property="og:description" content="@yield('seo-description','Die innovative Fußballtrainer-Software für Profi- und Amateurvereine. Professionelle Trainingsübungen | Verletzungs- und Ernährungsportal | Mannschaftsorganisation- und Management.')">
    <meta property="og:type" content="website"/>
    <link rel="publisher" href="https://plus.google.com/112379064220231236403" property="schema:url">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@#">
    <meta name="twitter:creator" content="@#">
    <meta name="twitter:title" content="@yield('seo-title','Fussballtrainer-software.de - einfach gut organisiert!')">
    <meta name="twitter:description" content="@yield('seo-description','Die innovative Fußballtrainer-Software für Profi- und Amateurvereine. Professionelle Trainingsübungen | Verletzungs- und Ernährungsportal | Mannschaftsorganisation- und Management.')">
    <meta name="twitter:url" content="{{Request::url()}}">
    <meta name="twitter:image:src" content="{{asset('vendor/main/bilder/fussballtrainer-software.jpg')}}">
    <meta name="twitter:image:width " content="400">
    <meta name="twitter:image:height" content="200">
    <meta name="twitter:image" content="@yield('seo-title','Fussballtrainer-software.de - einfach gut organisiert!')">
    <meta name="geo.region" content="DE-NW" />
    <meta name="geo.placename" content="Hattingen" />
    <meta name="geo.position" content="51.400301;7.214898" />
    <meta name="ICBM" content="51.400301, 7.214898" />
    <meta name="robots" content="noindex,nofollow"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- FAVICONS ANFANG -->
    <link rel="shortcut icon" href="{{asset('vendor/main/bilder/favicon.png')}}">
    <link rel="apple-touch-icon" href="{{asset('img/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('vendor/main/bilder/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('vendor/main/bilder/favicon.png')}}">
    <!-- FAVICONS ENDE -->

    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,400,700%7CNoto+Sans:400,700" rel="stylesheet">

    <!-- CSS ANFANG -->
    <link href="{{asset('vendor/main/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" media="all">
    <link href="{{asset('vendor/blog/css/material-design-iconic-font.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/main/css/themify-icons.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('vendor/main/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
   {{-- <link href="{{asset('css/theme.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css" media="all" />--}}
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400%7CRaleway:100,400,300,500,600,700%7COpen+Sans:400,500,600' rel='stylesheet' type='text/css'>
    <link href="{{asset('vendor/footer/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
    <!-- SOCIAL MEDIA ANFANG -->
    <link href="{{asset('vendor/social-share/css/slideout-style.css')}}" rel="stylesheet" type="text/css">
    <!-- SOCIAL MEDIA ENDE -->
    <link rel="stylesheet" href="{{asset('vendor/lightbox/css/lightbox.min.css')}}">

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- CSS Ende -->

    <!-- end canvas recording -->

    {{-- TODO: Export as CSS File in Future --}}
    <style>

        img{
            max-width: 100%;
        }

        div.gamefield{
            margin-top: 50px;
            position: relative;
        }

        body{
            background-image: url('vendor/gamefield/texture/background.jpg');
            display: -webkit-flex !important;
            display: -webkit-box !important;
            display: -ms-flexbox !important;
            display: flex !important;
            -webkit-align-items: center !important;
            -webkit-box-align: center !important;
            -ms-flex-align: center !important;
            align-items: center !important;
            height: 100vh;
        }

        div.objects{
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }

        div.object{
            display:inline-block;
            position: absolute;
            z-index: 1000;
            box-sizing: content-box;
            padding: 1px;
        }

        div.object button{
            position: absolute;
            left: -16px;
            display: none;
            border-radius: 5px;
            background-color: red;
            color: white;
            border: 1px solid darkred;
            padding: 2px;
            width: 20px;
            height: 20px;
            font-size: 12px;
            top: -14px;
        }

        div.object:hover, div.object.active{
            padding: 0px;
            border-radius: 25%;
        }

        div.object:hover{
            border: 1px dashed #373737;
        }

        div.object.active{
            border: 1px solid black;
        }

        div.object.active button{
            display: block !important;
        }

        div.menu{
            display: none;
            position: absolute;
            right: 0;
            top: 0;
            bottom: 0;
            width: 300px;
            background-color: rgba(255, 255, 255, 0.5);
            height: 100%;
            z-index: 1500;
        }

        /* animation */
        div#video-container.opened {
            background-color: rgba(255, 255, 255, 0.5);
        }
        /* endanimation */

        div.menu div.content{
            width: 100%;
        }

        div.menu div.content ul{
            /*overflow-y: auto;
            height: 100%;*/
        }

        #menu-control{
            position: absolute;
            right: 0;
            top: 0;
            z-index: 2000;
            background: none;
            border: none;
            box-shadow: none;
            color: #0000006b;
        }

        #menu-control:focus{
            outline: 0;
        }


    </style>

</head>
