<!doctype html>
<html lang="{{ app()->getLocale() }}" style="overflow-x: hidden;">

@include('layouts.gamefield.header')

<body>
    @yield('content')
</body>

<!-- canvas recording -->
<script src="{{ asset('vendor/canvasrecorder/src/js/third_party/webgl_teapot/webgl-utils.js') }}"></script>
<script src="{{ asset('vendor/canvasrecorder/src/js/third_party/webgl_teapot/webgl-debug.js') }}"></script>
<script src="{{ asset('vendor/canvasrecorder/src/js/third_party/webgl_teapot/matrix4x4.js') }}"></script>
<script src="{{ asset('vendor/canvasrecorder/src/js/third_party/webgl_teapot/cameracontroller.js') }}"></script>
<script src="{{ asset('vendor/canvasrecorder/src/js/third_party/webgl_teapot/teapot-streams.js') }}"></script>


<!-- include adapter for srcObject shim -->
<script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
<script src="{{ asset('vendor/canvasrecorder/src/content/capture/canvas-record/js/main.js') }}"></script>
<script src="{{ asset('vendor/canvasrecorder/src/js/lib/ga.js') }}"></script>

</body>

</html>
