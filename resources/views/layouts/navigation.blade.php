<div class="nav-container">
    <nav>
        <div class="nav-utility">
            <div class="module left">
                <i class="ti-location-arrow">&nbsp;</i>
                <span class="sub">www.fussballtrainer-software.de</span>
            </div>
            <div class="module left">
                <i class="ti-email">&nbsp;</i>
                <span class="sub">support@fussballtrainer-software.de</span>
            </div>

        </div>
        <div class="nav-bar">
            <div class="module left">
                <a href="{{route('welcome')}}">
                    <img class="logo logo-light" alt="Fussballsoftware" src="{{asset('vendor/main/bilder/fussballsoftware.png')}}">
                    <img class="logo logo-dark" alt="Fussballsoftware" src="{{asset('vendor/main/bilder/fussballsoftware.png')}}">
                </a>
            </div>
            <div class="module widget-handle mobile-toggle right visible-sm visible-xs">
                <i class="ti-menu"></i>
            </div>


            <div class="module right">
                @guest
                    <a class="btn btn-sm" href="{{route('register')}}">Registrieren</a>
                    <a class="btn btn-sm" href="{{route('login')}}">Einloggen</a>
                @endguest
                @auth
                    <a class="btn btn-sm" href="{{ route('logout') }}"onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Ausloggen</a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                @endauth
            </div>

            <div class="module-group right">
                <div class="module left">
                    <ul class="menu">
                        <li><a href="{{route('welcome')}}">Startseite</a></li>
                        <li class="has-dropdown">
                            <a href="#">Funktionen</a>
                            <ul class="mega-menu">
                                <li>
                                    <ul>
                                        <li><a href="{{route('main.fussballtraining')}}">Fussballtraining</a></li>
                                        <li><a href="{{route('main.autogrammkarten')}}">Autogrammkarten</a></li>
                                        <li><a href="{{route('main.telefonliste')}}">Telefonliste</a></li>
                                        <li><a href="{{route('main.strafen-und-mannschaftskasse')}}">Strafen und Mannschaftskasse</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <ul>
                                        <li><a href="{{route('main.verletzungsportal')}}">Verletzungsportal</a></li>
                                        <li><a href="{{route('main.ernaehrungsportal')}}">Ernährungsportal</a></li>
                                        <li><a href="{{route('main.rueckennummern')}}">Rückennummern</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="{{route('main.preise')}}">Preise</a></li>
                        <li><a href="{{route('blog.index')}}">Fu&szlig;ball-News</a></li>
                        <li><a href="{{route('workout.index')}}">Trainingsübungen</a></li>
                        <li><a href="{{route('search.index')}}">Vereinsverzeichnis</a></li>
                        {{--<li><a href="ueberuns.html">�ber uns</a></li>--}}
                        {{--<li><a href="kooperationen.html">Kooperationen</a></li>--}}

                        {{--<div class="module widget-handle language left">
                            <ul class="menu">
                                <li class="has-dropdown">
                                    <a href="#">GER</a>
                                    <ul>
                                        <li><a href="#">English</a></li>
                                        <li><a href="#">Deutsch</a> </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>--}}
                    </ul>
                </div>

            </div>

        </div>
    </nav>
</div>

<ul id="wpSlideOutTabs">
    <li>
        <a class="wpso-fb" href="https://facebook.com/sharer/sharer.php?u={{Request::url()}}" target="_blank">
            <!-- Facebook Tab URL -->
            <span style="background: #111;">Facebook</span>
            <!-- Facebook Tab URL -->
        </a>
    </li>

    <li>
        <a class="wpso-google" href="https://plus.google.com/share?url={{Request::url()}}" target="_blank">
            <!-- Blog Tab URL -->
            <span style="background: #111;">Google+</span>
            <!-- Blog Tab Color -->
        </a>
    </li>

    <li>
        <a class="wpso-twit" href="https://twitter.com/intent/tweet/?text=Fussball%20Software&amp;url={{Request::url()}}" target="_blank">
            <!-- Twitter Tab URL -->
            <span style="background: #111;">Twitter</span>
            <!-- Twitter Tab URL -->
        </a>
    </li>

    <li>
        <a class="wpso-contact" href="mailto:?subject=Fussball%20Software&amp;body={{Request::url()}}">
            <!-- YouTube Tab URL -->
            <span style="background: #111;">Email</span>
            <!-- YouTube Tab URL -->
        </a>
    </li>

    {{--<li>
        <a class="wpso-instagram" href="https://www.instagram.com/fussballteamverwaltung/">
            <!-- Blog Tab URL -->
            <span style="background: #111;">Instagram</span>
            <!-- Blog Tab Color -->
        </a>
    </li>--}}
</ul>


@if (array_key_exists('bc', View::getSections()))
    <div class="main-container">
        <section class="page-title page-title-4 image-bg overlay parallax">
            <div class="background-image-holder">
                <img alt="Background Image" class="background-image" src="{{asset('vendor/main/bilder/cover14.jpg')}}">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="uppercase mb0">@yield('bc-title')</h3>
                    </div>
                    <div class="col-md-6 text-right">
                        <ol class="breadcrumb breadcrumb-2">
                            <li>
                                <a href="{{route('welcome')}}">Startseite</a>
                            </li>
                            <li>
                                <a href="#">@yield('bc-1')</a>
                            </li>
                            <li class="active">@yield('bc')</li>
                        </ol>
                    </div>
                </div>

            </div>

        </section>
    </div>
@endif


