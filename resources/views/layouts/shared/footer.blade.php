<footer class="footer st--footer--1">
    <div class="st--footer--top st--sp--65 st--dark--bg--2">
        <div class="container">
            <div class="row">
                {{--<div class="col-md-4 col-sm-6">
                    <div class="widget st--footer--widget">
                        <a href="#" class="footer--logo">
                            <img src="{{asset('vendor/blog/img/logo--white.png')}}" alt="">
                        </a>
                        <p>Stylish is a blog and magazine theme for all compnay who are looking for blog magazine template.we try to create a with more stylish also great UI .</p>

                    </div>
                </div>--}}
                <div class="col-md-4 col-sm-6">
                    <div class="widget st--footer--widget">
                        <div class="st--widget--title--2">
                            <h4>Letzten 5 Trainingsübungen</h4>
                        </div>
                        <div class="st--recent--postbox--1">


                            @foreach($last5Workouts as $post)
                            <div class="st--single">
                                <div class="pull-left">
                                    <div class="st--recent--img" style="background-image: url({{asset($post->image)}})"></div>
                                </div>
                                <div class="st--recent--content">
                                    <h5><a href="{{$post->link()}}">{{$post->title}}</a></h5>
                                    <a href="#" class="st--date">{{$post->created_at->format('d.m.Y')}}</a>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="widget st--footer--widget">
                        <div class="st--widget--title--2">
                            <h4>Letzten 5 Anleitungen</h4>
                        </div>
                        <div class="st--recent--postbox--1">


                            @foreach($last5Manuals as $post)
                                <div class="st--single">
                                    <div class="pull-left">
                                        <div class="st--recent--img" style="background-image: url({{asset($post->image)}})"></div>
                                    </div>
                                    <div class="st--recent--content">
                                        <h5><a href="{{$post->link()}}">{{$post->title}}</a></h5>
                                        <a href="#" class="st--date">{{$post->created_at->format('d.m.Y')}}</a>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="widget st--footer--widget">
                        <div class="st--widget--title--2">
                            <h4>Letzten 5 Ernährungen u. Verletzungen</h4>
                        </div>
                        <div class="st--recent--postbox--1">


                            @foreach($last5FoodAndInjuries as $post)
                                <div class="st--single">
                                    <div class="pull-left">
                                        <div class="st--recent--img" style="background-image: url({{asset($post->image)}})"></div>
                                    </div>
                                    <div class="st--recent--content">
                                        <h5><a href="{{$post->link()}}">{{$post->title}}</a></h5>
                                        <a href="#" class="st--date">{{$post->created_at->format('d.m.Y')}}</a>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-md-4 col-sm-12">
                    <div class="widget st--footer--widget">
                        <div class="st--widget--title--2">
                            <h4>Rechtliches</h4>
                        </div>
                        <div class="st--footer--inner">

                            <ul style="list-style: none">

                                <li><a href="{{route('main.agb')}}">AGB</a></li>
                                <li><a href="{{route('main.datenschutz')}}">Datenschutz</a></li>
                                <li><a href="{{route('main.impressum')}}">Impressum</a></li>
                                <li><a href="{{route('main.widerrufsrecht')}}">Widerrufsrecht</a></li>

                            </ul>

                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12">
                    <div class="widget st--footer--widget">
                        <div class="st--widget--title--2">
                            <h4>Support</h4>
                        </div>
                        <div class="st--footer--inner">

                            <ul style="list-style: none">

                                <li><a href="{{route('main.faq')}}">FAQ</a></li>
                                <li><a href="{{route('main.versand-und-zahlungsbedingungen')}}">Versand und Zahlungsbedingungen</a></li>
                                <li><a href="{{route('main.zahlungsarten')}}">Zahlungsarten</a></li>
                                <li><a href="{{route('main.preise')}}">Preise</a></li>
                                <li><a href="{{route('main.kontakt.index')}}">Kontakt</a></li>
                                <li><a href="{{route('main.newsletter.anmelden')}}">Newsletter anmelden</a></li>
                                <li><a href="http://unsubscribe.newsletter2go.com/form.html?n2g=bnergzjb-ws2ucpjr-wwk&_ga=2.44447716.155479812.1519717957-106827841.1519717957">Newsletter abmelden</a></li>

                            </ul>

                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12">
                    <div class="widget st--footer--widget">
                        <div class="st--widget--title--2">
                            <h4>Funktionen</h4>
                        </div>
                        <div class="st--footer--inner">

                            <ul style="list-style: none">

                                <li><a href="{{route('main.autogrammkarten')}}">Autogrammkarten</a></li>
                                <li><a href="{{route('main.ernaehrungsportal')}}">Ernährungsportal</a></li>
                                <li><a href="{{route('main.fussballtraining')}}">Fussballtraining</a></li>
                                <li><a href="{{route('main.verletzungsportal')}}">Verletzungsportal</a></li>
                                <li><a href="{{route('main.telefonliste')}}">Telefonliste</a></li>
                                <li><a href="{{route('main.rueckennummern')}}">Rückennummern</a></li>
                                <li><a href="{{route('main.strafen-und-mannschaftskasse')}}">Strafen und Mannschaftskasse</a></li>

                            </ul>

                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-md-4 col-sm-12">
                    <a href="https://www.instantssl.com/ssl-certificate.html" style="text-decoration:none; ">
                        <img alt="SSL Certificate" src="https://www.instantssl.com/ssl-certificate-images/support/comodo_secure_113x59_transp.png" style="border: 0px;" /><br />
                        <span style="font-weight:bold;font-size:12px; padding-left:20px; color:#77BAE4;"></a>
                </div>

            </div>


        </div>
    </div>
    <div class="st--footer--btm st--dark--bg--3">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 text-center">
                    <span>© Copyright 2018 Fussballtrainer Software</span>
                </div>
            </div>
        </div>
    </div>
</footer>