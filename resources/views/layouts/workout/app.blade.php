<!doctype html>
<html lang="{{ app()->getLocale() }}">

@include('layouts.workout.header')

<body class="home" style="background-image: url('{{asset($settings->where('id',1)->first()->value)}}'); background-repeat: no-repeat; background-size: cover;">

    @include('layouts.workout.navigation')


    @yield('content')

    @include('layouts.shared.footer')

    <!-- footer -->
    <!-- Vendors (All Essential JavaScript plugins) -->

    <!-- bootstrap -->
    <script type="text/javascript" src="{{asset('vendor/blog/js/vendor/bootstrap.min.js')}}"></script>
    <!-- magnific -->
    <script type="text/javascript" src="{{asset('vendor/blog/js/vendor/magnific.popup.min.js')}}"></script>
    <!-- slick -->
    <script type="text/javascript" src="{{asset('vendor/blog/js/vendor/slick.min.js')}}"></script>
    <!-- slicknav -->
    <script type="text/javascript" src="{{asset('vendor/blog/js/vendor/slicknav.min.js')}}"></script>
    <!-- masonry -->
    <script type="text/javascript" src="{{asset('vendor/blog/js/vendor/masonry.pkgd.min.js')}}"></script>
    <!-- web ticker -->
    <script type="text/javascript" src="{{asset('vendor/blog/js/vendor/web.ticker.min.js')}}"></script>
    <!-- active -->
    <script type="text/javascript" src="{{asset('vendor/blog/js/active.js')}}"></script>

    <script type="text/javascript" src="{{asset('vendor/lightbox/js/lightbox.js')}}"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
    <script>
        window.addEventListener("load", function(){
            window.cookieconsent.initialise({
                "palette": {
                    "popup": {
                        "background": "#000"
                    },
                    "button": {
                        "background": "transparent",
                        "text": "#f1d600",
                        "border": "#f1d600"
                    }
                },
                "showLink": false,
                "content": {
                    "message": "Wir verwenden Cookies, um die Webseite bestmöglich an die Bedürfnisse unserer Besucher anpassen zu können. Wenn Sie auf der Seite weitersurfen stimmen Sie der Cookie-Nutzung zu",
                    "dismiss": "ICH STIMME ZU!"
                }
            })});
    </script>

</body>

</html>