<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('seo-title','Fußball-News: von Bundesliga bis Kreisklasse')</title>
    <meta name="description" content="@yield('seo-description','Auf unserem News-Portal findest Du die neuesten Nachrichten aus dem Fußball. Wir informieren Dich über brandaktuelle Entwicklungen in den deutschen und internationalen Profi- und Amateurligen - und natürlich zu Deinem Lieblingsverein. Verpasse nie weder Regeländerungen, Transfers oder Trainerentlassungen!')">
    <meta property="og:url" content="{{Request::url()}}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="@yield('seo-title','Fußball-News: von Bundesliga bis Kreisklasse')"/>
    <meta property="og:image" content="@yield('seo-image',asset('bilder/fussballtrainer-software.jpg'))"/>
    <meta property="og:description" content="@yield('seo-description','Auf unserem News-Portal findest Du die neuesten Nachrichten aus dem Fußball. Wir informieren Dich über brandaktuelle Entwicklungen in den deutschen und internationalen Profi- und Amateurligen - und natürlich zu Deinem Lieblingsverein. Verpasse nie weder Regeländerungen, Transfers oder Trainerentlassungen!')"/>
    <link rel="publisher" href="https://plus.google.com/112379064220231236403" property="schema:url">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@#">
    <meta name="twitter:creator" content="@#">
    <meta name="twitter:title" content="@yield('seo-title','Fußball-News: von Bundesliga bis Kreisklasse')">
    <meta name="twitter:description" content="@yield('seo-description','Auf unserem News-Portal findest Du die neuesten Nachrichten aus dem Fußball. Wir informieren Dich über brandaktuelle Entwicklungen in den deutschen und internationalen Profi- und Amateurligen - und natürlich zu Deinem Lieblingsverein. Verpasse nie weder Regeländerungen, Transfers oder Trainerentlassungen!')">
    <meta name="twitter:url" content="{{Request::url()}}">
    <meta name="twitter:image:src" content="@yield('seo-image',asset('bilder/fussballtrainer-software.jpg'))">
    <meta name="twitter:image:width " content="400">
    <meta name="twitter:image:height" content="200">
    <meta name="twitter:image" content="@yield('seo-title','Auf unserem News-Portal findest Du die neuesten Nachrichten aus dem Fußball. Wir informieren Dich über brandaktuelle Entwicklungen in den deutschen und internationalen Profi- und Amateurligen - und natürlich zu Deinem Lieblingsverein. Verpasse nie weder Regeländerungen, Transfers oder Trainerentlassungen!')">
    <meta name="geo.region" content="DE-NW" />
    <meta name="geo.placename" content="Hattingen" />
    <meta name="geo.position" content="51.400301;7.214898" />
    <meta name="ICBM" content="51.400301, 7.214898" />
    <meta name="robots" content="noindex,nofollow"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- FAVICONS ANFANG -->
    <link rel="shortcut icon" href="{{asset('vendor/blog/img/favicon.png')}}">
    <link rel="apple-touch-icon" href="{{asset('vendor/blog/img/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('vendor/blog/img/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('vendor/blog/img/favicon.png')}}">
    <!-- FAVICONS ENDE -->

    <!--bootstrap-->
    <link href="{{asset('vendor/blog/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- fontawesome -->
    <link href="{{asset('vendor/blog/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- material-icon -->
    <link href="{{asset('vendor/blog/css/material-design-iconic-font.min.css')}}" rel="stylesheet">
    <!-- owl -->
    <link href="{{asset('vendor/blog/css/owl.carousel.css')}}" rel="stylesheet">
    <!-- animate css -->
    <link href="{{asset('vendor/blog/css/animate.css')}}" rel="stylesheet">
    <!-- owl.carousel -->
    <link rel="stylesheet" href="{{asset('vendor/blog/css/owl.carousel.css')}}">
    <!-- sweet.alert.css -->
    <link rel="stylesheet" href="{{asset('vendor/blog/css/sweet.alert.css')}}">
    <!-- slicknav.css -->
    <link rel="stylesheet" href="{{asset('vendor/blog/css/slicknav.min.css')}}">
    <!-- mfg.css -->
    <link rel="stylesheet" href="{{asset('vendor/blog/css/magnific.pupup.css')}}">

    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <!-- <link rel="stylesheet" href="assets/rs-plugin/css/settings.css')}}'"> -->

    <!-- custom -->
    <link rel="stylesheet" href="{{asset('vendor/blog/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/lightbox/css/lightbox.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/social-share/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
    <link href="{{asset('vendor/footer/css/style_blog.css')}}" rel="stylesheet" type="text/css" media="all" />

    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,400,700%7CNoto+Sans:400,700" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{asset('vendor/blog/js/jquery.min.js')}}"></script>
    <!-- jQuery -->

</head>