<!-- preloader -->
<div class="preloader" aria-busy="true" aria-label="Loading, please wait." role="progressbar"></div>
<div class="preloader--main">
    <div class="st--inner">
        <span class="preloader-spin"></span>
    </div>
</div>

<header>

    <div class="logo--area">
        <div class="container">
            <div class="row">
                <div class="col-md-4 logo--column">
                    <a href="#" class="Fussball Blog"><img src="{{asset('vendor/blog/img/'.($category->headerimage ?? 'fussball_blog.png'))}}" alt=""></a>
                </div>
                <div class="col-md-8 ads--column--1">
                    <a href="#" class="header--ads">
                        <img src="{{asset('vendor/blog/img/ads--1.jpg')}}" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="menu--area header--bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12 menu--column">
                    <nav class="main_menu">
                        <ul>
                            <li><a href="{{route('welcome')}}">Startseite</a></li>
                            <li><a href="{{route('blog.index')}}">Alles</a></li>
                            @foreach($workoutCategories as $cat)
                                <li><a href="{{$cat->link()}}">{{$cat->name}}</a>
                                    <ul class="sub-menu">
                                        <li><a href="{{$cat->link()}}">Alles</a></li>
                                        @foreach($cat->childs as $child)
                                            <li><a href="{{$child->link()}}">{{$child->name}}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                        </ul>
                    </nav>
                </div>

            </div>
        </div>
    </div>

    <div class="news--ticker dark--ticker">
        <div class="container">
            <div class="st--inner">
                <span class="st--tags"><i class="zmdi zmdi-trending-up"></i> &nbsp; Breaking News</span>
                <div class="st--header--ticker--wrap">
                    <ul class="st--news--ticker--1">
                        @foreach($newsTicker as $text)
                            <li>{!! $text->body !!}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>



</header>

<!-- /header -->