@extends('layouts.vereinsverzeichnis.app')

@section('content')
    <div class="container" id="club-show">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-transparent">
                    <div class="panel-heading">Verein hinzufügen</div>

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-12">

                                <form method="POST" class="form-horizontal" action="{{route('submissions.store')}}">

                                    {{csrf_field()}}

                                    <div class="form-group">
                                        <label for="author" class="control-label col-md-2">Eigener Name für Erwähnungen (Optional):</label>
                                        <div class="col-md-10 col-lg-4">
                                            <input class="form-control" name="author" value="{{old('author')}}" />
                                        </div>
                                    </div>

                                    <hr />

                                    {{-- Name, Kontaktdaten --}}
                                    <div class="form-group">
                                        <label for="title" class="control-label col-md-2">Name:</label>
                                        <div class="col-md-10 col-lg-4">
                                            <input class="form-control" name="title" value="{{old('title')}}" />
                                        </div>

                                        <label for="street" class="control-label col-md-2">Adresse:</label>
                                        <div class="col-md-10 col-lg-4">
                                            <input class="form-control" name="street" value="{{old('street')}}" />
                                        </div>

                                        <label for="zip" class="control-label col-md-2">PLZ:</label>
                                        <div class="col-md-10 col-lg-4">
                                            <input class="form-control" name="zip" value="{{old('zip')}}" />
                                        </div>

                                        <label for="city" class="control-label col-md-2">Ort:</label>
                                        <div class="col-md-10 col-lg-4">
                                            <input class="form-control" name="city" value="{{old('city')}}" />
                                        </div>

                                        <label for="phone" class="control-label col-md-2">Telefon:</label>
                                        <div class="col-md-10 col-lg-4">
                                            <input class="form-control" name="phone" value="{{old('phone')}}" />
                                        </div>

                                        <label for="fax" class="control-label col-md-2">Fax:</label>
                                        <div class="col-md-10 col-lg-4">
                                            <input class="form-control" name="fax" value="{{old('fax')}}" />
                                        </div>

                                        <label for="email" class="control-label col-md-2">Email:</label>
                                        <div class="col-md-10 col-lg-4">
                                            <input class="form-control" name="email" value="{{old('email')}}" />
                                        </div>

                                        <label for="uri" class="control-label col-md-2">Webseite:</label>
                                        <div class="col-md-10 col-lg-4">
                                            <input class="form-control" name="uri" value="{{old('uri')}}" />
                                        </div>
                                    </div>

                                    <hr />

                                    {{-- Stadion und Verband --}}
                                    <div class="form-group">
                                        <label for="stadium" class="control-label col-md-2">Stadion:</label>
                                        <div class="col-md-10 col-lg-4">
                                            <input class="form-control" name="stadium" value="{{old('stadium')}}" />
                                        </div>

                                        <label for="association" class="control-label col-md-2">Verband:</label>
                                        <div class="col-md-10 col-lg-4">
                                            <input class="form-control" name="association" value="{{old('association')}}" />
                                        </div>
                                    </div>

                                    <hr />

                                    {{-- Social --}}
                                    <div class="form-group">
                                        <label for="facebook" class="control-label col-md-2">Facebook:</label>
                                        <div class="col-md-10 col-lg-4">
                                            <input class="form-control" name="facebook" value="{{old('facebook')}}" />
                                        </div>

                                        <label for="twitter" class="control-label col-md-2">Twitter:</label>
                                        <div class="col-md-10 col-lg-4">
                                            <input class="form-control" name="twitter" value="{{old('twitter')}}" />
                                        </div>

                                        <label for="youtube" class="control-label col-md-2">Youtube:</label>
                                        <div class="col-md-10 col-lg-4">
                                            <input class="form-control" name="youtube" value="{{old('youtube')}}" />
                                        </div>

                                        <label for="instagram" class="control-label col-md-2">Instagram:</label>
                                        <div class="col-md-10 col-lg-4">
                                            <input class="form-control" name="instagram" value="{{old('instagram')}}" />
                                        </div>
                                    </div>

                                    <hr />

                                    {{-- Beschreibung --}}
                                    <div class="form-group">
                                        <label for="description" class="control-label col-md-2">Beschreibung:</label>
                                        <div class="col-md-10">
                                            <textarea id="editor" class="form-control" name="description" rows="20">
                                                {{old('description')}}
                                            </textarea>
                                        </div>
                                    </div>

                                    <hr />

                                    <div class="pull-right text-right">
                                        {!! NoCaptcha::display() !!}
                                        <button class="btn btn-primary">Absenden</button>
                                    </div>




                                </form>

                            </div>

                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="//cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'editor' );
    </script>
@endsection
