@extends('layouts.vereinsverzeichnis.app')

@section('content')
    <div class="container" id="association-show">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Ergebnisse:</div>

                    <div class="panel-body">

                        <h1>Verband: {{$association->title}}</h1>

                        <table class="table table-bordered table-hover">

                        @foreach($association->toArray() as $key => $item)

                            <tr>
                                <th>{{$key}}</th>
                                <td>{{$item}}</td>
                            </tr>

                        @endforeach

                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
