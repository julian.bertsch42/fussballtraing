@extends('layouts.backend.app')

@section('title',"Vereine - ")

@section('page','Vereine Übersicht')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{route('backend.admin.clubs.index')}}">Vereine</a>
    </li>
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Vereine</h1>

            <form class="form-inline" method="GET" action="{{route('backend.admin.clubs.index')}}">
                <a class="btn btn-default" href="{{route('backend.admin.clubs.create')}}">Neuer Verein</a>

                <div class="input-group custom-search-form">
                    <input class="form-control" type="text" name="search" value="{{old('search')}}" placeholder="Suchen..." />
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>

            <hr />

            <table class="table table-striped table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($clubs as $club)
                        <tr>
                            <td>{{$club->title}}</td>
                            <td>Aktiv</td>
                            <td><a href="{{route('backend.admin.clubs.edit',$club->id)}}" class="btn btn-warning"><i class="fa fa-edit"></i></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{$clubs->links()}}

        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

@endsection