@extends('layouts.backend.app')

@section('page-title', 'Benutzer - Übersicht')

@section('content')
    <!--
        The ID "widget-grid" will start to initialize all widgets below
        You do not need to use widgets if you dont want to. Simply remove
        the <section></section> and you can use wells or panels instead
        -->

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                    <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                    -->
                    <header>
                        <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                        <h2>Benutzer - Übersicht </h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                            <input class="form-control" type="text">
                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">

                            <!-- this is what the user will see -->

                            <a href="{{route('backend.admin.users.create')}}" class="btn btn-default">Neuer Benutzer</a>

                            <hr />

                            <table class="table">

                                <thead>

                                    <tr>

                                        <th>ID #</th>
                                        <th>Name</th>
                                        <th>Benutzername</th>
                                        <th>Email</th>
                                        <th>Admin</th>
                                        <th>Notiz</th>
                                        <th>Optionen</th>

                                    </tr>

                                </thead>

                                <tbody>

                                    @foreach($users as $user)

                                        <tr>

                                            <td>{{$user->id}}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->username}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->isAdmin() ? "Ja" : "Nein"}}</td>
                                            <td>{{$user->adminnote}}</td>
                                            <td><a href="{{route('backend.admin.users.edit',$user->id)}}">Bearbeiten</a></td>

                                        </tr>

                                    @endforeach

                                </tbody>

                            </table>

                            {{$users->links()}}


                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- WIDGET END -->

        </div>

        <!-- end row -->



    </section>
    <!-- end widget grid -->
@endsection