@extends('layouts.backend.app')

@section('page-title', 'Blog - Newsticker Bearbeiten')

@section('content')
    <!--
        The ID "widget-grid" will start to initialize all widgets below
        You do not need to use widgets if you dont want to. Simply remove
        the <section></section> and you can use wells or panels instead
        -->

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                    <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                    -->
                    <header>
                        <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                        <h2>Blog - Newsticker Bearbeiten </h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                            <input class="form-control" type="text">
                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">

                            <!-- this is what the user will see -->

                            <form class="form-horizontal" method="POST" action="{{route('backend.admin.settings.system.newsticker.update')}}">

                                {{csrf_field()}}

                                <div class="form-group">

                                    <label class="col-md-2 col-sm-4 control-label" for="body">Text 1:</label>
                                    <div class="col-md-4 col-sm-8">
                                        <input id="body" class="form-control" name="body1" type="text" value="{{old('body1',$newsticker->where('id',1)->first()->body ?? null)}}" />
                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-md-2 col-sm-4 control-label" for="body">Text 2:</label>
                                    <div class="col-md-4 col-sm-8">
                                        <input id="body" class="form-control" name="body2" type="text" value="{{old('body2',$newsticker->where('id',2)->first()->body ?? null)}}" />
                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-md-2 col-sm-4 control-label" for="body">Text 3:</label>
                                    <div class="col-md-4 col-sm-8">
                                        <input id="body" class="form-control" name="body3" type="text" value="{{old('body3',$newsticker->where('id',3)->first()->body ?? null)}}" />
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="col-md-4 col-sm-8 col-md-offset-2 col-sm-offset-4">
                                        <button type="submit" class="btn btn-default">Ändern</button>
                                    </div>
                                </div>



                            </form>

                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- WIDGET END -->

        </div>

        <!-- end row -->



    </section>
    <!-- end widget grid -->
@endsection