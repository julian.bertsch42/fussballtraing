@extends('layouts.backend.app')

@section('page-title', 'Einstellung Bearbeiten')

@section('content')
    <!--
        The ID "widget-grid" will start to initialize all widgets below
        You do not need to use widgets if you dont want to. Simply remove
        the <section></section> and you can use wells or panels instead
        -->

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                    <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                    -->
                    <header>
                        <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                        <h2>Einstellung Bearbeiten </h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                            <input class="form-control" type="text">
                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">

                            <!-- this is what the user will see -->

                            <form class="form-horizontal" method="POST" action="{{route('backend.admin.settings.system.blog-background.update',$setting->id)}}" enctype="multipart/form-data">

                                {{csrf_field()}}
                                {{method_field('PUT')}}

                                <div class="form-group">

                                    <label class="col-md-2 col-sm-4 control-label" for="name">Name:</label>
                                    <div class="col-md-4 col-sm-8">
                                        <input id="name" class="form-control" name="name" type="text" value="{{old('name',$setting->name)}}" required />
                                    </div>

                                    @if($setting->id == 1)
                                    <label class="col-md-2 col-sm-4 control-label" for="value">Vorschaubild:</label>
                                    <div class="col-md-4 col-sm-8">
                                        <input id="value" class="form-control" name="value" type="file" value="{{old('value',$setting->value)}}" />
                                        @if ($errors->has('value'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('value') }}</strong>
                                            </span>
                                        @endif
                                        <br />
                                        <img style="border:1px solid #ccc;" src="{{asset($setting->value)}}" height="200px" />
                                    </div>
                                    @endif


                                </div>


                                <div class="form-group">
                                    <div class="col-md-4 col-sm-8 col-md-offset-2 col-sm-offset-4">
                                        <button type="submit" class="btn btn-default">Ändern</button>
                                    </div>
                                </div>



                            </form>


                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- WIDGET END -->

        </div>

        <!-- end row -->



    </section>
    <!-- end widget grid -->
@endsection
