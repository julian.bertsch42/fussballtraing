@extends('layouts.backend.app')

@section('page-title', 'Blog - Kategorie Erstellen')

@section('content')

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">

                    <header>
                        <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                        <h2>Blog - Kategorie Erstellen </h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                            <input class="form-control" type="text">
                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">

                            <!-- this is what the user will see -->

                            <form class="form-horizontal" method="POST" action="{{route('backend.admin.settings.functions.blog.categories.store')}}">

                                {{csrf_field()}}

                                <div class="form-group">

                                    <label class="col-md-2 col-sm-4 control-label" for="name">Name:</label>
                                    <div class="col-md-4 col-sm-8">
                                        <input id="name" class="form-control" name="name" type="text" value="{{old('name')}}" required />
                                    </div>

                                    <label class="col-md-2 col-sm-4 control-label" for="ordering">Reihenfolge:</label>
                                    <div class="col-md-4 col-sm-8">
                                        <input id="ordering" class="form-control" name="ordering" type="text" value="{{old('ordering')}}" />
                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-md-2 col-sm-4 control-label" for="seotitle">SEO-Title:</label>
                                    <div class="col-md-4 col-sm-8">
                                        <input id="seotitle" class="form-control" name="seotitle" type="text" value="{{old('seotitle')}}" />
                                    </div>

                                    <label class="col-md-2 col-sm-4 control-label" for="seodescription">SEO-Description:</label>
                                    <div class="col-md-4 col-sm-8">
                                        <input id="seodescription" class="form-control" name="seodescription" type="text" value="{{old('seodescription')}}" />
                                    </div>

                                </div>

                                <div class="form-group">

                                    <label class="col-md-2 col-sm-4 control-label" for="color">Farbe:</label>
                                    <div class="col-md-4 col-sm-8">
                                        <select id="color" name="color" class="form-control" required>
                                            @foreach([1 => 'Rot', 2 => 'Blau', 3 => 'Gelb', 4 => 'Schwarz', 5 => 'Grau'] as $id => $color)
                                                <option value="{{$id}}" @if(old('color') == $id) selected @endif>{{$color}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>


                                <div class="form-group">

                                    <label class="col-md-2 col-sm-4 control-label" for="headerimage">Header-Bild:</label>
                                    <div class="col-md-4 col-sm-8">
                                        <select id="headerimage" name="headerimage" class="form-control">
                                            <option></option>
                                            @foreach($headerimages as $image)
                                                <option value="{{$image}}" @if(old('headerimage') == $image) selected @endif>{{$image}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                    <div class="form-group">
                                    <div class="col-md-4 col-sm-8 col-md-offset-2 col-sm-offset-4">
                                        <button type="submit" class="btn btn-default">Speichern</button>
                                    </div>
                                </div>



                            </form>


                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- WIDGET END -->

        </div>

        <!-- end row -->



    </section>
    <!-- end widget grid -->
@endsection