@extends('layouts.backend.app')

@section('page-title', 'Blog - Unterkategorien')

@section('content')

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">

                    <header>
                        <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                        <h2>Blog - Übersicht </h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                            <input class="form-control" type="text">
                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">

                            <!-- this is what the user will see -->

                            <a href="{{route('backend.admin.settings.functions.blog.subcategories.create')}}" class="btn btn-default">Neue Unterkategorie</a>

                            <hr />

                            <table class="table">

                                <thead>

                                    <tr>

                                        <th>ID #</th>
                                        <th>Header-Bild</th>
                                        <th>Name</th>
                                        <th>Übergeordnet</th>
                                        <th>Reihenfolge</th>
                                        <th>Optionen</th>

                                    </tr>

                                </thead>

                                <tbody>

                                    @foreach($blogSubCategories as $cat)

                                        <tr>

                                            <td>{{$cat->id}}</td>
                                            <td><img src="{{asset('vendor/blog/img/'.($cat->headerimage ?? 'fussball_blog.png'))}}" height="50px" /></td>
                                            <td><span class="st--tags bg-{{$cat->color}}">{{$cat->name}}</span></td>
                                            <td><span class="st--tags bg-{{$cat->parent->color ?? 0}}">{{$cat->parent->name ?? 0}}</span></td>
                                            <td>{{$cat->ordering ?? 0}}</td>
                                            <td><a href="{{route('backend.admin.settings.functions.blog.subcategories.edit',$cat->id)}}">Bearbeiten</a></td>

                                        </tr>

                                    @endforeach

                                </tbody>

                            </table>


                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- WIDGET END -->

        </div>

        <!-- end row -->



    </section>
    <!-- end widget grid -->
@endsection