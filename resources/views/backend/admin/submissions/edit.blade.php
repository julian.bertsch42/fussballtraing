@extends('layouts.backend.app')

@section('title',"Vorschlag Bearbeiten - ")

@section('page','Vorschlag Bearbeiten')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{route('backend.admin.submissions.index')}}">Vorschläge</a>
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Vorschlag Bearbeiten</h1>

            <div class="row">

                <div class="col-md-6">

                    <h3 class="page-header">Vorschlag:</h3>

                    <form id="submission" method="POST" class="form-horizontal" action="{{route('backend.admin.submissions.update',$submission->id)}}">

                        {{csrf_field()}}
                        {{method_field('PUT')}}

                        {{-- Name, Kontaktdaten --}}
                        <div class="form-group">
                            <label for="title" class="control-label col-md-2">Name:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="title" value="{{old('title',$submission->title)}}" />
                            </div>

                            <label for="street" class="control-label col-md-2">Adresse:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="street" value="{{old('street',$submission->street)}}" />
                            </div>

                            <label for="zip" class="control-label col-md-2">PLZ:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="zip" value="{{old('zip',$submission->zip)}}" />
                            </div>

                            <label for="city" class="control-label col-md-2">Ort:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="city" value="{{old('city',$submission->city)}}" />
                            </div>

                            <label for="phone" class="control-label col-md-2">Telefon:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="phone" value="{{old('phone',$submission->phone)}}" />
                            </div>

                            <label for="fax" class="control-label col-md-2">Fax:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="fax" value="{{old('fax',$submission->fax)}}" />
                            </div>

                            <label for="email" class="control-label col-md-2">Email:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="email" value="{{old('email',$submission->email)}}" />
                            </div>

                            <label for="uri" class="control-label col-md-2">Webseite:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="uri" value="{{old('uri',$submission->uri)}}" />
                            </div>
                        </div>

                        <hr />

                        {{-- Stadion und Verband --}}
                        <div class="form-group">
                            <label for="stadium" class="control-label col-md-2">Stadion:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="stadium" value="{{old('stadium',$submission->stadium)}}" />
                            </div>

                            <label for="association" class="control-label col-md-2">Verband:</label>
                            <div class="col-md-10 col-lg-4">
                                <select class="form-control" name="association_id">
                                    @foreach($associations as $association)
                                        <option value="{{$association->id}}" @if(old('association_id',$associationSelected->id ?? 0) == $association->id) selected @endif>{{$association->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <hr />

                        {{-- Social --}}
                        <div class="form-group">
                            <label for="facebook" class="control-label col-md-2">Facebook:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="facebook" value="{{old('facebook',$submission->facebook)}}" />
                            </div>

                            <label for="twitter" class="control-label col-md-2">Twitter:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="twitter" value="{{old('twitter',$submission->twitter)}}" />
                            </div>

                            <label for="youtube" class="control-label col-md-2">Youtube:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="youtube" value="{{old('youtube',$submission->youtube)}}" />
                            </div>

                            <label for="instagram" class="control-label col-md-2">Instagram:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="instagram" value="{{old('instagram',$submission->instagram)}}" />
                            </div>
                        </div>

                        <hr />

                        {{-- Beschreibung --}}
                        <div class="form-group">
                            <label for="description" class="control-label col-md-2">Beschreibung:</label>
                            <div class="col-md-10">
                                <textarea id="editor" class="form-control" name="description" rows="20">
                                    {!! old('description',$submission->description) !!}
                                </textarea>
                            </div>
                        </div>

                    </form>

                </div>





                <div class="col-md-6">

                    <h3 class="page-header">Aktuell in der Datenbank:</h3>

                    <div class="form-horizontal">

                        {{csrf_field()}}

                        {{-- Name, Kontaktdaten --}}
                        <div class="form-group">
                            <label for="title" class="control-label col-md-2">Name:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="title" value="{{$club->title ?? ""}}" readonly />
                            </div>

                            <label for="street" class="control-label col-md-2">Adresse:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="street" value="{{$club->street ?? ""}}" readonly />
                            </div>

                            <label for="zip" class="control-label col-md-2">PLZ:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="zip" value="{{$club->zip ?? ""}}" readonly />
                            </div>

                            <label for="city" class="control-label col-md-2">Ort:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="city" value="{{$club->city ?? ""}}" readonly />
                            </div>

                            <label for="phone" class="control-label col-md-2">Telefon:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="phone" value="{{$club->phone ?? ""}}" readonly />
                            </div>

                            <label for="fax" class="control-label col-md-2">Fax:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="fax" value="{{$club->fax ?? ""}}" readonly />
                            </div>

                            <label for="email" class="control-label col-md-2">Email:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="email" value="{{$club->email ?? ""}}" readonly />
                            </div>

                            <label for="uri" class="control-label col-md-2">Webseite:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="uri" value="{{$club->uri ?? ""}}" readonly />
                            </div>
                        </div>

                        <hr />

                        {{-- Stadion und Verband --}}
                        <div class="form-group">
                            <label for="stadium" class="control-label col-md-2">Stadion:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="stadium" value="{{$club->stadium ?? ""}}" readonly />
                            </div>

                            <label for="association" class="control-label col-md-2">Verband:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="association" value="{{$club->association->title ?? ""}}" readonly />
                            </div>
                        </div>

                        <hr />

                        {{-- Social --}}
                        <div class="form-group">
                            <label for="facebook" class="control-label col-md-2">Facebook:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="facebook" value="{{$club->facebook ?? ""}}" readonly />
                            </div>

                            <label for="twitter" class="control-label col-md-2">Twitter:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="twitter" value="{{$club->twitter ?? ""}}" readonly />
                            </div>

                            <label for="youtube" class="control-label col-md-2">Youtube:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="youtube" value="{{$club->youtube ?? ""}}" readonly />
                            </div>

                            <label for="instagram" class="control-label col-md-2">Instagram:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="instagram" value="{{$club->instagram ?? ""}}" readonly />
                            </div>
                        </div>

                        <hr />

                        {{-- Beschreibung --}}
                        <div class="form-group">
                            <label for="description" class="control-label col-md-2">Beschreibung:</label>
                            <div class="col-md-10">
                                <div class="well">
                                    {!! $club->description ?? "" !!}
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

            </div>

            <hr />

            <div class="row">

                <div class="col-md-12">
                    @if($submission->status !== 1)
                        <button form="submission" name="action" value="approve" class="btn btn-primary pull-right">Vorschlag übernehmen</button>
                        @if(empty($submission->club_id))
                            <button form="submission" name="action" value="new" class="btn btn-success pull-right" style="margin-right: 10px;">Vorschlag als neuen Verein anlegen</button>
                        @endif

                        @if($submission->status !== 2)
                            <button form="submission" name="action" value="deny" class="btn btn-danger pull-right" style="margin-right: 10px;">Ablehnen</button>
                        @endif
                    @endif
                </div>

            </div>

            <br />


        </div>
    </div>
@endsection

@section('scripts')
    <script src="//cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'editor' );
    </script>
@endsection