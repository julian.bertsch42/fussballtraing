@extends('layouts.backend.app')

@section('title',"Vorschläge - ")

@section('page','Vorschläge Übersicht')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{route('backend.admin.submissions.index')}}">Vorschläge</a>
    </li>
@endsection

@section('content')


    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Vorschläge</h1>

            <table class="table table-striped table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Verein</th>
                        <th>Typ</th>
                        <th>Datum</th>
                        <th>Autor</th>
                        <th>Status
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($submissions as $submission)
                        <tr>
                            <td>{{$submission->title}}</td>
                            <td>@if(empty($submission->club_id)) Neuer Verein @else Änderungsvorschlag @endif</td>
                            <td data-order="{{$submission->updated_at->format('Y-m-d H:i:s')}}">{{$submission->updated_at->format('d.m.Y, H:i \U\h\r')}}</td>
                            <td>{{$submission->author ?? "Anonym"}}</td>
                            <td>@if($submission->status == 0) Offen @elseif($submission->status == 1) Akzeptiert @else Abgelehnt @endif</td>
                            <td><a href="{{route('backend.admin.submissions.edit',$submission->id)}}" class="btn btn-warning"><i class="fa fa-edit"></i></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

@endsection

@section('scripts')
    <script src="{{asset('vendor/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('vendor/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('.table').DataTable({
                "language": {
                    "url": "{{asset('js')}}/dataTables.german.lang"
                },
                "order": [[2,'desc']]
            });
        } );
    </script>
@endsection