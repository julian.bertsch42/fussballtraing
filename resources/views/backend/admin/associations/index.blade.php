@extends('layouts.backend.app')

@section('title',"Verbände - ")

@section('page','Verbände Übersicht')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{route('backend.admin.associations.index')}}">Verbände</a>
    </li>
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Verbände</h1>

            <form class="form-inline" method="GET" action="{{route('backend.admin.associations.index')}}">
                <a class="btn btn-default" href="{{route('backend.admin.associations.create')}}">Neuer Verband</a>
                
                <div class="input-group custom-search-form">
                    <input class="form-control" type="text" name="search" value="{{old('search')}}" placeholder="Suchen..." />
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>

            <hr />

            <table class="table table-striped table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Übergeordneter Verband</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($associations as $association)
                        <tr>
                            <td>{{$association->title}}</td>
                            <td>Aktiv</td>
                            <td>{{$association->parent() ? $association->parent()->title : null}}</td>
                            <td><a href="{{route('backend.admin.associations.edit',$association->id)}}" class="btn btn-warning"><i class="fa fa-edit"></i></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{$associations->links()}}

        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

@endsection