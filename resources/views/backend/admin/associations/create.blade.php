@extends('layouts.backend.app')

@section('title',"Verband Erstellen - ")

@section('page','Verband Erstellen')

@section('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{route('backend.admin.associations.index')}}">Verbände</a>
    </li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Verband Erstellen</h1>

            <div class="row">

                <div class="col-md-12">

                    <form id="submission" method="POST" class="form-horizontal" action="{{route('backend.admin.associations.store')}}">

                        {{csrf_field()}}

                        <div class="form-group">
                            <label for="title" class="control-label col-md-2">Name:</label>
                            <div class="col-md-10 col-lg-4">
                                <input class="form-control" name="title" value="{{old('title')}}" />
                            </div>


                            <label for="association" class="control-label col-md-2">Übergeordneter Verband:</label>
                            <div class="col-md-10 col-lg-4">
                                <select class="form-control" name="parent_id">
                                    @foreach($associations as $parent)
                                        <option value="{{$parent->id}}" @if(old('parent_id') == $parent->id) selected @endif>{{$parent->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <hr />

                        <button class="btn btn-primary pull-right">Übernehmen</button>

                    </form>

                </div>


            </div>


            <br />


        </div>
    </div>
@endsection

@section('scripts')
    <script src="//cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'editor' );
    </script>
@endsection