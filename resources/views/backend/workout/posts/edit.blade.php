@extends('layouts.backend.app')

@section('page-title', $post->subCategory->name . ' - Eintrag Bearbeiten')

@section('content')

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">

                    <header>
                        <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                        <h2>{{$post->subCategory->name}} - Eintrag Bearbeiten </h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                            <input class="form-control" type="text">
                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">

                            <!-- this is what the user will see -->

                            <form class="form-horizontal" method="POST" action="{{route('backend.workout.posts.update',$post->id)}}" enctype="multipart/form-data">

                                {{csrf_field()}}
                                {{method_field('PUT')}}

                                <div class="form-group">

                                    <label class="col-md-2 col-sm-4 control-label" for="title">Titel:</label>
                                    <div class="col-md-4 col-sm-8">
                                        <input id="title" class="form-control" name="title" type="text" value="{{old('title',$post->title)}}" required />
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                        @if ($errors->has('slug'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('slug') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <label class="col-md-2 col-sm-4 control-label" for="image">Vorschaubild:</label>
                                    <div class="col-md-4 col-sm-8">
                                        <input id="image" class="form-control" name="image" type="file" value="{{old('image',$post->image)}}" />
                                        <input id="imageObj" class="form-control" name="imageObj" type="hidden" value="{{old('imageObj')}}" />
                                        @if ($errors->has('image'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                        <br />
                                        <button type="button" id="gamefield_btn" class="btn btn-default">
                                            Übung erstellen
                                        </button>
                                        <br />
                                        <img id="imageImg" style="border:1px solid #ccc;" src="{{asset($post->image)}}" height="200px" />
                                    </div>


                                </div>

                                <div class="form-group">
                                    @if(auth::user()->isAdmin())
                                        <label class="col-md-2 col-sm-4 control-label" for="seo-title">Seo Titel:</label>
                                        <div class="col-md-4 col-sm-8">
                                            <input id="seo-title" class="form-control" name="seo-title" type="text" value="{{old('seo-title',$post->{"seo-title"})}}" />
                                            @if ($errors->has('seo-title'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('seo-title') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group">


                                    <label class="col-md-2 col-sm-4 control-label" for="category">Kategorie:</label>
                                    <div class="col-md-4 col-sm-8">
                                        <select id="category" name="category" class="form-control" required>
                                            @foreach($workoutSubCategories as $subCat)
                                                <option value="{{$subCat->id}}" @if(old('category',$post->workout_category_id) == $subCat->id) selected @endif>{{$subCat->parent->name." - ".$subCat->name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('category'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('category') }}</strong>
                                            </span>
                                        @endif
                                    </div>



                                    <label class="col-md-2 col-sm-4 control-label" for="tags">Tags:</label>
                                    <div class="col-md-4 col-sm-8">
                                        <input id="tags" class="form-control" placeholder="#Tag1, #Tag2, ..." name="tags" type="text" value="{{old('tags',$post->tagsAsString())}}" />
                                        @if ($errors->has('tags'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('tags') }}</strong>
                                        </span>
                                        @endif
                                    </div>


                                </div>


                                <div class="form-group">

                                    <label class="col-md-2 col-sm-4 control-label" for="body">Inhalt:</label>
                                    <div class="col-md-10 col-sm-8">
                                        <textarea id="body" class="form-control" name="body" required>{{old('body',$post->body)}}</textarea>
                                        @if ($errors->has('body'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('body') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                </div>


                                <div class="form-group">
                                    <div class="col-md-4 col-sm-8 col-md-offset-2 col-sm-offset-4">
                                        <button type="submit" class="btn btn-default">Ändern</button>
                                        @if(auth::user()->isAdmin())
                                            @if(empty($post->active))
                                                <button type="submit" name="active" value="1" class="btn btn-success">Freischalten</button>
                                            @else
                                                <button type="submit" name="active" value="0" class="btn btn-danger">Sperren</button>
                                            @endif
                                        @endif
                                        <button form="form_delete" type="submit" class="btn btn-danger">Löschen</button>
                                    </div>
                                </div>



                            </form>

                            <form id="form_delete" method="POST" action="{{route('backend.workout.posts.destroy',$post->id)}}">
                                {{csrf_field()}}
                                {{method_field('DELETE')}}
                            </form>


                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- WIDGET END -->

        </div>

        <!-- end row -->



    </section>
    <!-- end widget grid -->
@endsection


@section('scripts')
    <script src="{{asset('vendor/ckeditor/ckeditor.js')}}"></script>

    <script>

        function saveGamefield(image) {
            var ImageURL = $(image).attr('src');
            $('#image').attr('type','text').attr('disabled','true').val('Spielfeld eingefügt');
            $('#imageObj').val(ImageURL);
            $('#imageImg').attr('src', ImageURL);
        }

        $(document).ready(function() {
            CKEDITOR.replace( 'body', {
                "filebrowserImageUploadUrl": "/vendor/ckeditor/plugins/imgupload/imgupload.php"
            });

            $('#gamefield_btn').click(function(){
                var childWin = window.open("{{route('gamefield')}}", "_blank", "height=700, width=1200, status=yes, toolbar=no, menubar=no, location=no,addressbar=no");
            });
        });

    </script>
@endsection