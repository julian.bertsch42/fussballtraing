@extends('layouts.backend.app')

@section('page-title', (!empty($cat) ? $cat->name : 'Blog'). ' - Übersicht')

@section('content')

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">

                    <header>
                        <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                        <h2>@if(!empty($cat)){{$cat->name}} @else Blog @endif - Übersicht </h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                            <input class="form-control" type="text">
                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">

                            <!-- this is what the user will see -->

                            @if(!empty($cat))
                                <a href="{{route('backend.workout.posts.create',$cat->id)}}" class="btn btn-default">Neuer Eintrag</a>
                            @endif

                            <hr />

                            <table class="table">

                                <thead>

                                    <tr>

                                        <th>ID #</th>
                                        <th>Bild</th>
                                        <th>Titel</th>
                                        @if(empty($cat))
                                            <th>Kategorie</th>
                                        @endif
                                        <th>Autor</th>
                                        <th>Status</th>
                                        <th>Datum</th>
                                        <th>Optionen</th>

                                    </tr>

                                </thead>

                                <tbody>

                                    @foreach($posts as $post)

                                        <tr>

                                            <td>{{$post->id}}</td>
                                            <td><img src="{{asset($post->image)}}" height="25px" /></td>
                                            <td>{{$post->title}}</td>
                                            @if(empty($cat))
                                                <td><span class="st--tags bg-{{$post->subCategory->color}}">{{$post->category->name." - ".$post->subCategory->name}}</span></td>
                                            @endif
                                            <td>{{$post->user->name}}</td>
                                            <td>{!! $post->active ? "<span class='label label-success'>Freigeschaltet</span>" : "<span class='label label-danger'>Nicht Freigeschaltet</span>" !!}</td>
                                            <td>{{$post->updated_at->format('d.m.Y')}}</td>
                                            <td><a href="{{route('backend.workout.posts.edit',$post->id)}}">Bearbeiten</a></td>

                                        </tr>

                                    @endforeach

                                </tbody>

                            </table>

                            {{$posts->links()}}


                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- WIDGET END -->

        </div>

        <!-- end row -->



    </section>
    <!-- end widget grid -->
@endsection