@extends('layouts.gamefield.app')
<style>
.jscolor {
  width: auto !important;
}
.evo-pop {
  width: auto !important;
  padding: 12px !important;
}
ul > li.col-xs-4 {
  max-width: 112px;
}
    .btn-space {
        margin-right: 5px;
    }
div#video-container {
    width: 100%;
    position: fixed;
    top: 0;
    padding-top: 20px;
    padding-bottom: 20px;
    text-align: center
}
div#video-container button#menu-control-video-close {
    margin-right: 300px;
}
div#video-container.closed div#control-menu, div#video-container.closed button#menu-control-video-close {
    display: none;
}
div#video-container.opened button#menu-control-video-open {
    display: none;
}
div#content.vmenu-open {
    margin-top: 50px
}

</style>
@section('content')
    <div id="video-container" class="closed">
        <button class="btn btn-info" id="menu-control-video-open"><span class="glyphicon glyphicon-facetime-video"></span> Videokontrolle öffnen</button>
        <div>
            <button class="btn btn-info pull-right" id="menu-control-video-close" class="opened"><span class="glyphicon glyphicon-remove"></span> Videokontrolle schließen</button>
            <div id="control-menu">
                <div id="sceneTime-container" style="width: 312px; margin-top: 12px; margin-left: auto; margin-right: auto" class="m-12">
                    <input id="sceneTime" type="range" min="1" max="5" value="1">
                </div>
                <button class="btn btn-info" id="download" disabled><span class="glyphicon glyphicon-download-alt"></span> download</button>
                <button class="btn btn-info" id="reset" disabled><span class="glyphicon glyphicon-refresh"></span> reset</button>
                <button class="btn btn-info" id="play"><span class="glyphicon glyphicon-play"></span> abspielen</button>
                <button class="btn btn-info" id="stop" disabled><span class="glyphicon glyphicon-pause"></span> pause</button>
                <button class="btn btn-info" id="playAll"><span class="glyphicon glyphicon-play"></span> alle szenen</button>
                <span class="scenes-buttons">
                    <button class="btn btn-primary btn-space scene" id="scene-0" value="0">Szene 1</button>
            </span>
                <button class="btn btn-info" id="newScene"><span class="glyphicon glyphicon-plus"></span> neue Szene</button>
                <button class="btn btn-info" id="deleteScene" style="background-color: #FF3333;"><span class="glyphicon glyphicon-trash"></span> Szene löschen</button>
            </div>
        </div>
    </div>

    <button class="btn btn-info pull-right" id="menu-control"><span class="glyphicon glyphicon-menu-hamburger"></span></button>

    <div class="menu">
        <div class="content" style="padding-top: 50px;">

            <div>
                <button id="btnSave" class="btn btn-info" style="margin: 0 auto; display: block; width: 100px;">Exportieren</button>
            </div>

            <hr style="border-color: rgba(0,0,0,0.23);" />

            <select class="form-control" style="margin: 0 auto; display: block; width: 100px;">
                <option value="spieler" selected>Spieler</option>
                <option value="baelle">Bälle</option>
                <option value="markierungen">Markierungen</option>
                <option value="pfeile">Pfeile</option>
                <option value="material">Material</option>
                <option value="freieszeichnen">Freies Zeichnen</option>
                <option value="formenflächen">Formen und Flächen</option>
                <option value="texthinzufuegen">Text hinzufügen</option>
            </select>

            <hr style="border-color: rgba(0,0,0,0.23);" />

            <ul class="spieler" style="list-style: none;">
                @foreach($spieler as $object)
                    <li class="col-xs-4">
                        <img src="{{asset('vendor/gamefield/texture/Spieler/'.$object)}}" width="" height="" />
                    </li>
                @endforeach
                <div style="min-width: 100%; margin-top: 12px; display: inline-block; text-align: center;">
                  <button type="button" class="left btn btn-info" name="button"><</button>
                  <button type="button" class="right btn btn-info" name="button">></button>
                </div>
            </ul>

            <ul class="baelle" style="list-style: none;">
                @foreach($baelle as $object)
                    <li class="col-xs-4">
                        <img src="{{asset('vendor/gamefield/texture/Baelle/'.$object)}}" width="" height="" />
                    </li>
                @endforeach
                <div style="min-width: 100%; margin-top: 12px; display: inline-block; text-align: center;">
                  <button type="button" class="left btn btn-info" name="button"><</button>
                  <button type="button" class="right btn btn-info" name="button">></button>
                </div>
            </ul>

            <ul class="markierungen" style="list-style: none;">
                @foreach($markierungen as $object)
                    <li class="col-xs-4">
                        <img src="{{asset('vendor/gamefield/texture/Markierungen/'.$object)}}" width="" height="" />
                    </li>
                @endforeach
                <div style="min-width: 100%; margin-top: 12px; display: inline-block; text-align: center;">
                  <button type="button" class="left btn btn-info" name="button"><</button>
                  <button type="button" class="right btn btn-info" name="button">></button>
                </div>
            </ul>

            <ul class="pfeile" style="list-style: none;">
                @foreach($pfeile as $object)
                    <li class="col-xs-4">
                        <img src="{{asset('vendor/gamefield/texture/Pfeile/'.$object)}}" width="" height="" />
                    </li>
                @endforeach
                <div style="min-width: 100%; margin-top: 12px; display: inline-block; text-align: center;">
                  <button type="button" class="left btn btn-info" name="button"><</button>
                  <button type="button" class="right btn btn-info" name="button">></button>
                </div>
            </ul>

            <ul class="material" style="list-style: none;">
                @foreach($material as $object)
                    <li class="col-xs-4">
                        <img src="{{asset('vendor/gamefield/texture/Material/'.$object)}}" width="" height="" />
                    </li>
                @endforeach
                <div style="min-width: 100%; margin-top: 12px; display: inline-block; text-align: center;">
                  <button type="button" class="left btn btn-info" name="button"><</button>
                  <button type="button" class="right btn btn-info" name="button">></button>
                </div>
            </ul>

            <ul class="texthinzufuegen" style="padding: 12px; text-align: center; list-style: none;">
              <button onclick="Addtext()" class="btn btn-info">Text hinzufügen</button>
              <div id="text-wrapper" style="margin-top: 32px;" ng-show="getText()">
                <div id="text-controls">
              	  <span class="jscolor"></span>
                  <br>
                    <label for="font-family" style="display:inline-block; margin-top: 36px;">Schriftart:</label>
                    <select id="font-family" class="btn btn-dropdown">
                      <option value="times new roman" selected>Times New Roman</option>
                      <option value="arial">Arial</option>
                      <option value="helvetica">Helvetica</option>
                      <option value="myriad pro">Myriad Pro</option>
                      <option value="delicious">Delicious</option>
                      <option value="verdana">Verdana</option>
                      <option value="georgia">Georgia</option>
                      <option value="courier">Courier</option>
                      <option value="comic sans ms">Comic Sans MS</option>
                      <option value="impact">Impact</option>
                      <option value="monaco">Monaco</option>
                      <option value="optima">Optima</option>
                      <option value="hoefler text">Hoefler Text</option>
                      <option value="plaster">Plaster</option>
                      <option value="engagement">Engagement</option>
                    </select>
                    <br>
                    <div>
                    <label for="text-font-size" style="margin-top: 32px; margin-bottom: 12px">Schriftgröße:</label>
                    <span id="text-font-size-value" class="info">300</span>
                    <input
                      id="text-font-size"
                      type="range"
                      min="0"
                      max="48"
                      step="2"
                      value="300"
                      data-orientation="horizontal"
                     >
                    </div>
                  </div>
                  </ul>

                  <ul class="freieszeichnen" style="padding: 12px; text-align: center; list-style: none;">
                    <button id="drawing-mode" class="btn btn-info" style="margin: auto; text-align: center; min-width: 164px">Zeichenmodus deaktivieren</button>
                    <div id="drawing-mode-options" style="text-align: center; margin-top: 32px;">
                      <label for="drawing-mode-selector">Modus:</label>
                      <select class="btn btn-dropdown" id="drawing-mode-selector">
                        <option>Stift</option>
                        <option>Kreis</option>
                        <option>Spray</option>
                        <option>Muster</option>

                        <option>hline</option>
                        <option>vline</option>
                        <option>square</option>
                        <option>diamond</option>
                        <option>texture</option>
                      </select><br>
                      <div style="margin-top: 32px;">
                        <label for="drawing-line-width">Linienstärke:</label>
                        <select class="btn btn-dropdown" name="" id="drawing-line-width">
                          <option value="2">1</option>
                          <option value="4">2</option>
                          <option value="6">3</option>
                          <option value="8">4</option>
                          <option value="10">5</option>
                        </select>
                      </div>
                      <br>
                      <div style="margin-top: 32px;">
                        <span class="jscolor" value="#47b475" id="drawing-color"></span>
                      </div>



                      <!-- label for="drawing-shadow-width">Schattenbreite:</label>
                      <span class="info">0</span><input type="range" value="0" min="0" max="50" id="drawing-shadow-width"><br -->
                    </div>
                  </ul>
                  <ul class="formenflächen" style="padding: 32px; padding-top: 0px; text-align: center;">
                    <button class="btn btn-info" type="button" name="button"  onclick="AddShape()">Form hinzufügen</button>
                    <br>
                    <canvas id="shape-canvas" height="200"></canvas>
                    <div style="min-width: 100%; margin-top: 12px; display: inline-block; text-align: center;">
                      <button type="button" class="prev-shape btn btn-info" name="button"><</button>
                      <button type="button" class="next-shape btn btn-info" name="button">></button>
                    </div>
                    <br>
                    <label for="shape-opacity">Transparenz:</label>
                    <span id="shape-opacity-value" class="info">100</span><input type="range" value="100" min="0" max="100" id="shape-opacity">
                    <br>
                    <label for="shape-color">Formenfarbe:</label>
                    <span class="jscolor" value="#47b475" id="shape-color"></span>
                    <!-- dreieck !-->
                    <!-- quadrat !-->
                    <!-- kreis !-->
                    <!-- change color !-->
                    <!-- change transparenz !-->
                  </ul>
      </div>
    </div>


    <div id="content" class="container">

       <div class="row">


           <div class="gamefield" style="width: 1100px; height: 446px">
               <div class="objects" >

                   <canvas id="sheet">

                   </canvas>

               </div>



           </div>

       </div>

   </div>

    <div id="img-out"></div>

    <script src="{{asset('vendor/main/js/app.js')}}"></script>
    <script src="{{asset('vendor/main/js/jquery.min.js')}}"></script>
    <script
            src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous">
    </script>
    <script src="{{asset('vendor/gamefield/js/jquery.ui.touch-punch.min.js')}}"></script>

    <script src="{{asset('vendor/gamefield/js/canvas2image.js')}}"></script>
    <script src="{{asset('vendor/gamefield/js/html2canvas.js')}}"></script>

    <script src="{{asset('vendor/gamefield/js/fabric.min.js')}}"></script>

    <script src="{{asset('vendor/gamefield/js/gamefield.js')}}"></script>
    <script src="{{asset('vendor/gamefield/js/object.js')}}"></script>
    <script src="{{asset('vendor/colorpicker/js/colorpicker.js')}}"></script>
    <link rel="stylesheet" href="{{asset('vendor/colorpicker/css/colorpicker.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/gamefield/js/colorpicker/css/evol-colorpicker.min.css')}}">
    <script src="{{asset('vendor/gamefield/js/colorpicker/js/evol-colorpicker.min.js')}}"></script>
    <script src="{{asset('vendor/rangesliderjs/rangeslider.js')}}"></script>
    <link rel="stylesheet" href="{{asset('vendor/rangesliderjs/rangeslider.css')}}">
    <style media="sn">
      .colorpicker {
        width: 283px;
        margin: auto;
      }
      .rangeslider--horizontal {
        width: 167.826px !important;
      }
    </style>
    <script>
      /* pagination */
      let page = 0;
      let elements = 15;
      let total = $('ul.spieler > li').length;
      let element = 'spieler'
      let lineArray = []
      let moving = false
      let currentLine = false
      let circleArray = []
      let elementCircleArray = []
      let key = null
      let ponter = null
      let scenes = [[]]
      let currentScene = 0
      let sceneLog = []
      /* animation */
      function renderLines (object, point = null) {
          let line = {}
          let top = point ? point.y : object.top
          let left = point ? point.x : object.left
          if(object.cacheKey in lineArray) {
              line = lineArray[object.cacheKey]
              line.set({
                  x1: left,
                  y1: top
              });
          }
          if(object.cacheKey in circleArray) {
              line = circleArray[object.cacheKey]
              line.set({
                  x2: left,
                  y2: top
              });
          }
      }
      $("#menu-control-video-close").click(function () {
          $("#video-container").addClass("closed");
          $("#video-container").removeClass("opened");
          $("#content").removeClass("vmenu-open");
      })
      $("#menu-control-video-open").click(function () {
          $("#video-container").addClass("opened");
          $("#video-container").removeClass("closed");
          $("#content").addClass("vmenu-open");
      })
      /* endanimation */
      $(document).ready(function() {
          /* end animation */
          /* animation */

          $('div.menu div.content select.form-control').change(function(){
          page = 0;
          element = $(this).val();
          total = $('ul.'+element+' > li').length;
          showPaginatedList(element, page, elements);
        });
        showPaginatedList(element, page, elements);
        //hide all elementes
        $('ul > div > .left').click(function() {
          if(page > 0) {
            page--;
          }
          showPaginatedList(element, page, elements);
        });
        $('ul > div > .right').click(function() {
          if(page < total/elements-1) {
            page++;
          }
          showPaginatedList(element, page, elements);
        });

      });
      function showPaginatedList(element, page, elements) {
        $('ul.'+element+' > li').hide();
        $('ul.'+element+' > li').each(function (index) {
          if(index >= page*elements && index < page*elements+elements) {
            $(this).show();
          }
        });
      }
        var deleted = false;
        var canvas = null;
        var textColor = "#000";
        window.onload=function(){

          $("#text-font-size").on('input', function () {
            $("#text-font-size-value").html($("#text-font-size").val());
          });
            $('input[type="range"]#sceneTime').rangeslider({
                polyfill : false,
                onInit : function() {
                    this.output = $( '<div id="sceneTime" style="margin:12px 12px 12px 0px; font-size: 14px; color: #33302F; font-weight: bold"/>' ).insertAfter( this.$range ).html( this.$element.val() + ' sekunden Szenendauer' );
                },
                onSlide : function( position, value ) {
                    this.output.html( value + ' sekunden Szenendauer' );
                }
            });
            $('input[type="range"]#sceneTime').rangeslider('update', true);
          $('input[type="range"]#shape-opacity').rangeslider({
            polyfill : false,
            onInit : function() {
                this.output = $( '<div class="range-output" />' ).insertAfter( this.$range ).html( this.$element.val() );
            },
            onSlide : function( position, value ) {
                this.output.html( value );
            }
          });
            $('input[type="range"]#shape-opacity').rangeslider('update', true);
          $(".jscolor").colorpicker();
          $(".jscolor").on("change.color", function(event, color){
            textColor = color;
            canvas.freeDrawingBrush.color = textColor;
            if (canvas.getActiveObject()) {
              canvas.getActiveObject().set("fill", textColor);
              canvas.getActiveObject().set("stroke", textColor);
            }
            canvas.renderAll();
            canvas_shape.forEachObject(function(o) {
              o.set('fill', textColor);
              o.set('stroke', textColor);
            });
            canvas_shape.renderAll();
          });
          /*
          $('.jscolor').ColorPicker({
            flat: true,
            livePreview: false,
            onChange: function(hsb, hex, rgb, el) {
              textColor = "#"+hex;
              canvas.freeDrawingBrush.color = textColor;
              if (canvas.getActiveObject()) {
                canvas.getActiveObject().set("fill", textColor);
                canvas.getActiveObject().set("fill", textColor);
              }
              canvas.renderAll();
              canvas_shape.forEachObject(function(o) {
                o.set('fill', textColor);
            		o.set('stroke', textColor);
            	});
              canvas_shape.renderAll();
          	}
          });
          */
            canvas = new fabric.Canvas('sheet');
            canvas.setHeight($(".objects").innerHeight());
            canvas.setWidth($(".objects").outerWidth());
            canvas.renderAll();
            canvas.isDrawingMode = false;



            // Define the URL where your background image is located
            var imageUrl = "vendor/gamefield/texture/Spielfeld/spielfeld.png";



            fabric.Image.fromURL(imageUrl, function(img) {
                img.scaleToWidth(canvas.width);
                img.scaleToHeight(canvas.height);
                canvas.setBackgroundImage(img);
                canvas.requestRenderAll();
            });


// Define

            /* animation */
            function makeLine(coords) {
                return new fabric.Line(coords, {
                    fill: 'black',
                    stroke: 'black',
                    strokeWidth: 2,
                    selectable: false,
                    evented: true,
                    originX: 'center',
                    originY: 'center',
                });
            }
            function makeCircle(id, left, top, line1, line2) {
                var c = new fabric.Circle({
                    left: left,
                    top: top,
                    strokeWidth: 2,
                    radius: 12,
                    cacheKey: 'circle ' + id,
                    fill: '#fff',
                    stroke: '#666',
                    originX: 'center',
                    originY: 'center',
                });
                c.objectCaching = true
                c.hasControls = c.hasBorders = false;

                c.line1 = line1;
                c.line2 = line2;

                return c;
            }
            /* endanimation */

            // free drawing
              var _$ = function(id){return document.getElementById(id)};
              fabric.Object.prototype.transparentCorners = false;
              fabric.Object.prototype.borderColor = "#0066ff";
              fabric.Object.prototype.cornerColor = "#0066ff";
              var drawingModeEl = $('#drawing-mode'),
                  drawingOptionsEl = $('#drawing-mode-options'),
                  drawingColorEl = $('#drawing-color'),
                  drawingShadowColorEl = $('#drawing-shadow-color'),
                  drawingLineWidthEl = $('#drawing-line-width'),
                  drawingShadowWidth = $('#drawing-shadow-width'),
                  drawingShadowOffset = $('#drawing-shadow-offset'),
                  clearEl = $('#clear-canvas');

              clearEl.click(function() { canvas.clear(); ctx.drawImage(background,0,0); });

              drawingModeEl.click(function() {
                canvas.isDrawingMode = !canvas.isDrawingMode;
                if (canvas.isDrawingMode) {
                  drawingModeEl.html('Zeichenmodus deaktivieren');
                }
                else {
                  drawingModeEl.html('Zeichenmodus aktivieren');
                }
              });
              if (fabric.PatternBrush) {
                var vLinePatternBrush = new fabric.PatternBrush(canvas);
                vLinePatternBrush.getPatternSrc = function() {
                  var patternCanvas = fabric.document.createElement('canvas');
                  patternCanvas.width = patternCanvas.height = 10;
                  var ctx = patternCanvas.getContext('2d');

                  ctx.strokeStyle = textColor;
                  ctx.lineWidth = 5;
                  ctx.beginPath();
                  ctx.moveTo(0, 5);
                  ctx.lineTo(10, 5);
                  ctx.closePath();
                  ctx.stroke();

                  return patternCanvas;
                };

                var hLinePatternBrush = new fabric.PatternBrush(canvas);
                hLinePatternBrush.getPatternSrc = function() {

                  var patternCanvas = fabric.document.createElement('canvas');
                  patternCanvas.width = patternCanvas.height = 10;
                  var ctx = patternCanvas.getContext('2d');

                  ctx.strokeStyle = textColor;
                  ctx.lineWidth = 5;
                  ctx.beginPath();
                  ctx.moveTo(5, 0);
                  ctx.lineTo(5, 10);
                  ctx.closePath();
                  ctx.stroke();

                  return patternCanvas;
                };

                var squarePatternBrush = new fabric.PatternBrush(canvas);
                squarePatternBrush.getPatternSrc = function() {

                  var squareWidth = 10, squareDistance = 2;

                  var patternCanvas = fabric.document.createElement('canvas');
                  patternCanvas.width = patternCanvas.height = squareWidth + squareDistance;
                  var ctx = patternCanvas.getContext('2d');

                  ctx.fillStyle = this.color;
                  ctx.fillRect(0, 0, squareWidth, squareWidth);

                  return patternCanvas;
                };

                var diamondPatternBrush = new fabric.PatternBrush(canvas);
                diamondPatternBrush.getPatternSrc = function() {

                  var squareWidth = 10, squareDistance = 5;
                  var patternCanvas = fabric.document.createElement('canvas');
                  var rect = new fabric.Rect({
                    width: squareWidth,
                    height: squareWidth,
                    angle: 45,
                    fill: this.color
                  });

                  var canvasWidth = rect.getBoundingRect().width;

                  patternCanvas.width = patternCanvas.height = canvasWidth + squareDistance;
                  rect.set({ left: canvasWidth / 2, top: canvasWidth / 2 });

                  var ctx = patternCanvas.getContext('2d');
                  rect.render(ctx);

                  return patternCanvas;
                };

                var img = new Image();
                img.src = '/imgs/honey_im_subtle.png';

                var texturePatternBrush = new fabric.PatternBrush(canvas);
                texturePatternBrush.source = img;
              }

              $('#drawing-mode-selector').change(function() {
                if (this.value === 'hline') {
                  canvas.freeDrawingBrush = vLinePatternBrush;
                }
                else if (this.value === 'vline') {
                  canvas.freeDrawingBrush = hLinePatternBrush;
                }
                else if (this.value === 'square') {
                  canvas.freeDrawingBrush = squarePatternBrush;
                }
                else if (this.value === 'diamond') {
                  canvas.freeDrawingBrush = diamondPatternBrush;
                }
                else if (this.value === 'texture') {
                  canvas.freeDrawingBrush = texturePatternBrush;
                }
                else {
                  canvas.freeDrawingBrush = new fabric[this.value + 'Brush'](canvas);
                }

                if (canvas.freeDrawingBrush) {
                  canvas.freeDrawingBrush.color = textColor;
                  canvas.freeDrawingBrush.width = parseInt(drawingLineWidthEl.value, 10) || 1;
                  canvas.freeDrawingBrush.shadow = new fabric.Shadow({
                    blur: parseInt(drawingShadowWidth.value, 10) || 0,
                    offsetX: 0,
                    offsetY: 0,
                    affectStroke: true,
                    color: drawingShadowColorEl.value,
                  });
                }
              });

              drawingShadowColorEl.change(function() {
                canvas.freeDrawingBrush.shadow.color = this.value;
              });
              drawingLineWidthEl.change(function() {
                canvas.freeDrawingBrush.width = parseInt(this.value, 10) || 1;
                this.previousSibling.innerHTML = this.value;
              });
              drawingShadowWidth.change(function() {
                canvas.freeDrawingBrush.shadow.blur = parseInt(this.value, 10) || 0;
                this.previousSibling.innerHTML = this.value;
              });
              drawingShadowOffset.change(function() {
                canvas.freeDrawingBrush.shadow.offsetX = parseInt(this.value, 10) || 0;
                canvas.freeDrawingBrush.shadow.offsetY = parseInt(this.value, 10) || 0;
                this.previousSibling.innerHTML = this.value;
              });

              if (canvas.freeDrawingBrush) {
                canvas.freeDrawingBrush.color = textColor;
                canvas.freeDrawingBrush.width = parseInt(drawingLineWidthEl.value, 10) || 1;
                canvas.freeDrawingBrush.shadow = new fabric.Shadow({
                  blur: parseInt(drawingShadowWidth.value, 10) || 0,
                  offsetX: 0,
                  offsetY: 0,
                  affectStroke: true,
                  color: drawingShadowColorEl.value,
                });
              }
            // endfreedrawing */

            /* animation */
            function addObjectElements (x , y) {
                addDeleteBtn(x, y);
                addCopyBtn(x, y);
                if ('cacheKey' in canvas.getActiveObject() || canvas.getActiveObject().type ==  "activeSelection") addMoveBtn(x, y);
                addSequenceSelector(x, y);
            }

            function removeObjectElements() {
                $(".moveBtn").remove();
                $(".deleteBtn").remove();
                $(".copyBtn").remove();
                $(".sequenceSelector").remove();
            }

            function addSequenceSelector(x, y) {
                $(".sequenceSelector").remove();
                if (canvas.getActiveObject().type != "activeSelection") {
                    if ('sequence' in canvas.getActiveObject()) {
                        let btnLeft = x - 30;
                        let btnTop = y + 100;
                        let o = canvas.getActiveObject()
                        let key = o.sequence
                        let sequenceSelector = '<input type="number" value="' + key + '" class="sequenceSelector form-control" title="sequence" style="position:absolute;top:' + btnTop + 'px;left:' + (btnLeft) + 'px; padding: 0px; cursor:pointer;width:40px;height:20px;">';
                        $(".canvas-container").append(sequenceSelector);
                    }
                }
            }

            function addMoveBtn(x, y){
                $(".moveBtn").remove();
                var btnLeft = x-30;
                var btnTop = y+70;
                var moveBtn = '<button class="moveBtn move btn btn-info" title="move" style="position:absolute;top:'+btnTop+'px;left:'+(btnLeft)+'px; padding: 0px; cursor:pointer;width:20px;height:20px;"><span class="glyphicon glyphicon-move"></span></button>';
                $(".canvas-container").append(moveBtn);
            }
            /* endanimation */

            function addCopyBtn(x, y){
                $(".copyBtn").remove();
                var btnLeft = x-30;
                var btnTop = y+30;
                var copyBtn = '<button class="copyBtn copy btn btn-info" title="kopieren" style="position:absolute;top:'+btnTop+'px;left:'+(btnLeft)+'px; padding: 0px; cursor:pointer;width:20px;height:20px;"><span class="glyphicon glyphicon-copy"></span></button>';
                $(".canvas-container").append(copyBtn);
            }

            function addDeleteBtn(x, y){
                $(".deleteBtn").remove();
                var btnLeft = x-30;
                var btnTop = y-10;
                var deleteBtn = '<button class="deleteBtn delete btn btn-info" title="löschen"  style="background-color: #FF3333; border-color: #FF3333; position:absolute;top:'+btnTop+'px;left:'+(btnLeft)+'px; padding: 0px; cursor:pointer;width:20px;height:20px;"><span class="glyphicon glyphicon-trash"></span></button>';
                $(".canvas-container").append(deleteBtn);
            }

            /* events */

            canvas.on('object:modified', function (e) {
                canvas.getActiveObjects().forEach(function(o) {
                    if ('_objects' in o) {
                        o._objects.forEach(function(childO) {
                            renderLines(o)
                        })
                    }
                });
            });
            /* animation */
            canvas.on('object:added', function (e) {
                // add object to scenes
                if (! (currentScene in scenes)) {
                    scenes[currentScene] = []
                }
                let updated = false
                scenes[currentScene].forEach(function (o, index) {
                    if (o == e.target) {
                        scenes[currentScene][index] = e.target
                        updated = true
                    }
                })
                if (!updated) scenes[currentScene].push(e.target)
            });
            canvas.on('object:removed', function (e) {
                /* scenes[currentScene] = scenes[currentScene].filter(function (o) {
                    return o != e.target
                }) */
            });
            /* endanimation */

            canvas.on('before:selection:cleared', function (e) {
              // bring to front
              if (canvas.getActiveObject() && !deleted) {
                canvas.bringToFront(canvas.getActiveObject());
              }
              removeObjectElements()
              deleted = false
            });

            canvas.on('object:selected',function(e){
                //console.log("object selected", e.target.oCoords.tl.x, e.target.oCoords.tl.y);
                addObjectElements(e.target.oCoords.tl.x, e.target.oCoords.tl.y)
            });

            canvas.on('selection:created',function(e){
                //console.log("multiple object selected", e.target.oCoords.tl.x, e.target.oCoords.tl.y);
                addObjectElements(e.target.oCoords.tl.x, e.target.oCoords.tl.y)
            });
            /* animation
            (function animate() {
                let pos = 100;
                let objcts = [];
                canvas.getObjects().forEach(function(obj) {
                    if(obj.left < pos + ((objcts.length+1) * 100)) {
                        obj.left += 1;
                    }
                    objcts.push(obj)
                });
                canvas.renderAll();
                fabric.util.requestAnimFrame(animate);
            })();
            */

            canvas.on('mouse:down',function(e){
                if(!canvas.getActiveObject())
                {
                    removeObjectElements()
                } else {
                    //console.log("object selected", e.target.oCoords.tl.x, e.target.oCoords.tl.y);
                    addObjectElements(e.target.oCoords.tl.x, e.target.oCoords.tl.y)
                }
            });

            canvas.on('object:modified',function(e){
                if ('_objects' in e.target && e.target.type != "group") {
                    e.target._objects.forEach(function(o) {
                        let group = o.group
                        var point = new fabric.Point(o.left, o.top)
                        var transform = group.calcTransformMatrix();
                        var actualCoordinates = fabric.util.transformPoint(point, transform);
                        renderLines(o, actualCoordinates)
                    })
                } else {
                    renderLines(e.target)
                }
                canvas.renderAll()
              //alert(e.target.get('angle'));
                addObjectElements(e.target.oCoords.tl.x, e.target.oCoords.tl.y)
            });

            canvas.on('object:scaling',function(e){
                if (e.target.type == "group") {
                    e.target.setCoords()
                }
                /*
                if ('_objects' in e.target && e.target.type != "group") {

                    canvas.getActiveObjects().forEach(function(o) {
                        let group = o.group
                        var point = new fabric.Point(o.left, o.top)
                        var transform = group.calcTransformMatrix();
                        var actualCoordinates = fabric.util.transformPoint(point, transform);
                        renderLines(o, actualCoordinates)
                    })
                } else {
                    renderLines(e.target)
                }
                */
                canvas.renderAll()
                removeObjectElements()
            });
            canvas.on('object:moving',function(e) {
                /* animation */
                if ('_objects' in e.target && e.target.type != "group") {
                    e.target._objects.forEach(function(o) {
                        if ('_objects' in o) {
                            o._objects.forEach(function(childO) {
                                let group = childO.group
                                var point = new fabric.Point(childO.left, childO.top)
                                var transform = group.calcTransformMatrix();
                                var actualCoordinates = fabric.util.transformPoint(point, transform);
                                renderLines(childO, actualCoordinates)
                            });
                        } else {
                            let group = o.group
                            var point = new fabric.Point(o.left, o.top)
                            var transform = group.calcTransformMatrix();
                            var actualCoordinates = fabric.util.transformPoint(point, transform);
                            renderLines(o, actualCoordinates)
                        }
                    })
                } else {
                    if ('_objects' in e.target) {
                        e.target._objects.forEach(function(childO) {
                            renderLines(e.target)
                        })
                    } else {
                        renderLines(e.target)
                    }
                }
                canvas.renderAll()
                /* endanimation */
                removeObjectElements()
            });
            canvas.on('object:rotating',function(e){
                if ('_objects' in e.target  && e.target.type != "group") {
                    canvas.getActiveObjects().forEach(function(o) {
                        let group = o.group
                        var point = new fabric.Point(o.left, o.top)
                        var transform = group.calcTransformMatrix();
                        var actualCoordinates = fabric.util.transformPoint(point, transform);
                        renderLines(o, actualCoordinates)
                    })
                } else {
                    renderLines(e.target)
                }
                canvas.renderAll()
                removeObjectElements()
            });
            function deleteActiveObjects (withActive = true) {
                // delete associated line + circle
                canvas.getActiveObjects().forEach(function (o) {

                    if ('cacheKey' in o) {
                        canvas.getActiveObjects().forEach(function (o) {

                            if ('cacheKey' in o) {
                                if (withActive) {
                                    canvas.remove(o)
                                    scenes[currentScene] = scenes[currentScene].filter(function (sceneO) {
                                        return sceneO != o
                                    })
                                }
                                //remove line associated with element
                                let element = lineArray[o.cacheKey]
                                canvas.remove(element)
                                let followCircleKey = o.cacheKey
                                let followCircle = elementCircleArray[followCircleKey]
                                canvas.remove(followCircle)
                                scenes[currentScene] = scenes[currentScene].filter(function (o) {
                                    return o != followCircle
                                })
                                let prevIndex = Object.keys(elementCircleArray).filter(function(key) {
                                    return elementCircleArray[key].cacheKey == followCircleKey
                                })
                                canvas.remove(lineArray[prevIndex])
                                scenes[currentScene] = scenes[currentScene].filter(function (o) {
                                    return o != lineArray[prevIndex]
                                })
                                delete lineArray[prevIndex]
                                canvas.remove(elementCircleArray[prevIndex])
                                scenes[currentScene] = scenes[currentScene].filter(function (o) {
                                    return o != elementCircleArray[prevIndex]
                                })
                                delete elementCircleArray[prevIndex]
                                while (followCircleKey) {
                                    canvas.remove(lineArray[followCircleKey])
                                    canvas.remove(elementCircleArray[followCircleKey])
                                    if (! (followCircleKey in elementCircleArray) ) {
                                        scenes[currentScene] = scenes[currentScene].filter(function (o) {
                                            return o != lineArray[followCircleKey]
                                        })
                                        delete lineArray[followCircleKey]
                                        scenes[currentScene] = scenes[currentScene].filter(function (o) {
                                            return o != elementCircleArray[followCircleKey]
                                        })
                                        delete elementCircleArray[followCircleKey]
                                        scenes[currentScene] = scenes[currentScene].filter(function (o) {
                                            return o != circleArray[followCircleKey]
                                        })
                                        delete circleArray[followCircleKey]
                                        followCircleKey = null
                                    } else {
                                        let oldCircleKey = followCircleKey
                                        followCircleKey = elementCircleArray[followCircleKey].cacheKey
                                        scenes[currentScene] = scenes[currentScene].filter(function (o) {
                                            return o != lineArray[oldCircleKey]
                                        })
                                        delete lineArray[oldCircleKey]
                                        scenes[currentScene] = scenes[currentScene].filter(function (o) {
                                            return o != elementCircleArray[oldCircleKey]
                                        })
                                        delete elementCircleArray[oldCircleKey]
                                        scenes[currentScene] = scenes[currentScene].filter(function (o) {
                                            return o != circleArray[oldCircleKey]
                                        })
                                        delete circleArray[oldCircleKey]
                                    }
                                }
                                if (withActive) {
                                    canvas.remove(o)
                                }
                                //clean lines and circles without element
                            } }) }
                })
                deleted = true;
                if (withActive) {
                    if(canvas.getActiveObjects()){
                        canvas.getActiveObjects().forEach(function(o){ canvas.remove(o) });
                    }
                    //unselect
                    canvas.discardActiveObject();
                }
            }
            $(document).on('click',".deleteBtn",deleteActiveObjects);
            $(document).on('click',".copyBtn",function(){
              canvas.getActiveObject().clone(function(cloned) {
                _clipboard = cloned;
                Paste();
              });
            });
            /* animation */
            $(document).on('click',".moveBtn",function(){
                let pointer = canvas.getActiveObject();
                let cacheKey = null
                if (canvas.getActiveObjects().length > 1) {
                    let new_top = pointer.top + (pointer.oCoords.ml.y - pointer.oCoords.tl.y)
                    let new_left = pointer.left + (pointer.oCoords.mt.x - pointer.oCoords.tl.x)
                    // remove old paths
                    deleteActiveObjects(false)
                    // group this objects
                    pointer = canvas.getActiveObject().toGroup();
                    pointer.hasControls = false
                    pointer.originX = "center"
                    pointer.originY = "center"
                    pointer.cacheKey = "group "+Object.keys(lineArray).length
                    pointer.top = new_top
                    pointer.left = new_left
                    cacheKey = "group "+Object.keys(lineArray).length
                    pointer.setCoords();
                    canvas.renderAll();
                } else {
                    cacheKey = pointer.cacheKey
                }
                /* animation */
                var points = [pointer.left, pointer.top, pointer.left, pointer.top];
                moving = true;

                // create new line or use existing
                if (pointer.cacheKey in lineArray) {
                    currentLine = lineArray[pointer.cacheKey]
                } else {
                    currentLine = makeLine(points)
                    canvas.add(currentLine);
                }
                lineArray[cacheKey] = currentLine

                // pin to mouse
                canvas.renderAll();
                /* end animation */
            });

            canvas.on('mouse:move', function(options) {
                if (moving) {
                    pointer = canvas.getActiveObject();
                    let p = canvas.getPointer(options.e)
                    currentLine.set({
                        x2: p.x,
                        y2: p.y
                    });
                    key = pointer.cacheKey
                    if (key in lineArray) {
                        currentLine = lineArray[pointer.cacheKey]
                    }
                    if (key in elementCircleArray) {
                        // get circle
                        let circle = elementCircleArray[pointer.cacheKey]
                        // move to mouse pointer
                        circle.top  = p.y
                        circle.left = p.x
                        circle.setCoords();
                        renderLines(circle)
                    }
                    canvas.on('mouse:down', function(options) {
                        if (moving) {
                            let p = canvas.getPointer(options.e)
                            if (! (key in elementCircleArray) ) {
                                // at a point here
                                let circle = makeCircle(Object.keys(circleArray).length, p.x, p.y, null, currentLine)
                                canvas.add(circle).renderAll()
                                circleArray[circle.cacheKey] = currentLine
                                let followEl = circle
                                let sequence = 0
                                elementCircleArray[key] = circle
                                while (followEl != null) {
                                    let key = Object.keys(elementCircleArray).filter(function(key, index) {
                                        return elementCircleArray[key].cacheKey == followEl.cacheKey
                                    })
                                    if (key != "") {
                                        //get circle of that
                                        let newFollowEl = null
                                        canvas.getObjects().forEach(function(object, index2) {
                                            if ('cacheKey' in object) {
                                                if (object.cacheKey == key)  {
                                                    newFollowEl = object;
                                                }
                                            }
                                        })
                                        followEl = newFollowEl
                                    } else {
                                        followEl = null
                                    }

                                    //circle in circleArray ? followEl = elementCircleArray[key] : followEl = null
                                    if (followEl) {
                                        sequence++
                                    }
                                }

                                circle.sequence = sequence
                            }

                            moving = false
                        }
                    });
                    canvas.renderAll();
                }
            });
            $('.canvas-container').on('change', 'input.sequenceSelector', function (e){
                let o = canvas.getActiveObject()
                let diff = $(this).val()-o.sequence
                o.sequence = $(this).val()
                canvas.renderAll()
                let followCircle = o
                do {
                    if (followCircle.cacheKey in elementCircleArray) {
                        followCircle = elementCircleArray[followCircle.cacheKey]
                        followCircle.set({sequence: followCircle.sequence + diff})
                        canvas.renderAll()
                    } else { followCircle = null }
                } while (followCircle)
                canvas.renderAll()
            })

            /* endanimation */

          function Paste() {
           	// clone again, so you can do multiple copies.
          	_clipboard.clone(function(clonedObj) {
          	    // circle
          		canvas.discardActiveObject();
          		clonedObj.set({
          			left: clonedObj.left + 10,
          			top: clonedObj.top + 10
          		});
          		if (clonedObj.type === 'activeSelection') {
          			// active selection needs a reference to the canvas.
          			clonedObj.canvas = canvas;
          			clonedObj.forEachObject(function(obj) {
          				canvas.add(obj);
          			});
          			// this should solve the unselectability
          			clonedObj.setCoords();
          		} else {
          			canvas.add(clonedObj);
          		}
          		_clipboard.top += 10;
          		_clipboard.left += 10;
          		canvas.setActiveObject(clonedObj);
          		canvas.requestRenderAll();
          	});
          }
        }
    </script>
    <script src="{{asset('vendor/gamefield/js/init.js')}}"></script>
    <script src="{{asset('vendor/gamefield/js/fabric-text.js')}}"></script>
    <script src="{{asset('vendor/gamefield/js/fabric-shapes.js')}}"></script>
@endsection
