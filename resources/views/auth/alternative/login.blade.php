@extends('layouts.auth.app')

@section('content')

    <div class="sponsor_wrapper">

        <div class="bg">
            <div class="image"></div>
            <div class="sponsor">
                <p>SPONSOR</p>
            </div>
        </div>

        <div class="container">



            <h1>
                Fussballtrainer Software
            </h1>

            @if(Session::has('message'))
                <div class="row">
                    <div class="col-sm-12">
                        <div class="section">
                            <div class="alert alert-warning">
                                {{ Session::get('message') }}
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-sm-12">
                    <div class="section">
                            <a href="{{ url('/register') }}" class="btn btn-primary col s12">Registrieren</a>
                        </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="section">
                            <div class="oder">Oder</div>
                    </div>
                </div>
            </div>


            <form method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-12">
                        <div class="section">
                            <!--
                                E-Mail
                            -->
                            <div class="form-group">
                                <label for="username">Benutzername</label>
                                <input class="form-control" type="text" name="username" id="username" value="{{ old('username') }}" required>
                            </div>
                            <!--
                                Passwort
                            -->
                            <div class="form-group">
                                <label for="password">Passwort</label>
                                <input class="form-control" type="password" name="password" id="password" value="">
                            </div>
                            <!--
                                Login
                            -->
                            <div class="col-sm-12">
                                @if($errors->has('email'))
                                    <div class="alert alert-danger">
                                        @foreach($errors->get('email') as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </div>
                                @endif
                                @if($errors->has('password'))
                                    <div class="alert alert-danger">
                                        @foreach($errors->get('password') as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Anmelden" class="btn btn-default">
                                <p>
                                    <a href="{{ url('/password/reset') }}">Passwort zurücksetzen</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </form>


        </div>

    </div>



@endsection
