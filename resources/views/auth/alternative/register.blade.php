@extends('layouts.auth.app')

@section('content')

    <div class="sponsor_wrapper">

        <div class="bg">
            <div class="image"></div>
            <div class="sponsor">
                <p>SPONSOR</p>
            </div>
        </div>

        <div class="container" style="overflow-y: scroll">



            <h1>
                Fussballtrainer Software
            </h1>

                @if(Session::has('message'))
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="section">
                                <div class="alert alert-warning">
                                    {{ Session::get('message') }}
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-sm-12">
                        <div class="section">
                            <h2 class="oder boder-bottom padding-bottom-25">Registrieren</h2>
                            <p><a href="{{ url('/login') }}">Zum Login</a></p>
                        </div>
                    </div>
                </div>



                <form method="POST" action="{{ url('/register') }}" id="inputForm" class="form-horizontal">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="section">

                                <h3 class="header">Benutzerdaten</h3>

                                <!--
                                    NAME
                                -->
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="name">Vor- und Nachname</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text" name="name" id="name" value="{{ old('name') }}" required>
                                    </div>
                                    @if($errors->has('name'))
                                        <ul>
                                            @foreach($errors->get('name') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="username">Benutzername</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text" name="username" id="username" value="{{ old('username') }}" required>
                                    </div>
                                    @if($errors->has('username'))
                                        <ul>
                                            @foreach($errors->get('username') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>

                                <!--
                                    E-Mail
                                -->
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="email">Email</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="email" name="email" id="email" value="{{ old('email') }}" required>
                                    </div>
                                    @if($errors->has('email'))
                                        <ul>
                                            @foreach($errors->get('email') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>


                                <!--
                                    Password
                                -->
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="password">Passwort</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="password" name="password" id="password" value="" required>
                                    </div>
                                    @if($errors->has('password'))
                                        <ul>
                                            @foreach($errors->get('password') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                                <!--
                                    Passwortbestätigung
                                -->
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="password_confirmation">Passwort bestätigen</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="password" name="password_confirmation" id="password_confirmation" required>
                                    </div>
                                    @if($errors->has('password_confirmation'))
                                        <ul>
                                            @foreach($errors->get('password_confirmation') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>

                                <!--
                                Registrieren
                            -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="submit" value="Registrieren" class="btn btn-default submitForm">
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </form>

            </div>


        </div>

    </div>



@endsection