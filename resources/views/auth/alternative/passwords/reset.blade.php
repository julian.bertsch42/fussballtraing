@extends('layouts.auth.app')

@section('content')

    <div class="sponsor_wrapper">

        <div class="bg">
            <div class="image"></div>
            <div class="sponsor">
                <p>SPONSOR</p>
            </div>
        </div>

        <div class="container">



            <h1>
                Fussballtrainer Software
            </h1>

            @if(Session::has('message') || Session::has('status'))
                <div class="row">
                    <div class="col-sm-12">
                        <div class="section">
                            <div class="primary-container">
                                {{ Session::get('message') ?? Session::get('status') }}
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <form method="POST" action="{{ url('/password/reset') }}">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="section">
                            <!--
                                E-Mail
                            -->
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input class="form-control" type="email" name="email" id="email" value="{{ old('email') }}" required>
                                @if($errors->has('email'))
                                    <ul>
                                        @foreach($errors->get('email') as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                            <!--
                                Passwort
                            -->
                            <div class="form-group">
                                <label for="password">Passwort</label>
                                <input class="form-control" type="password" name="password" id="password" value="">
                                @if($errors->has('password'))
                                    <ul>
                                        @foreach($errors->get('password') as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                            <!--
                                password_confirmation
                            -->
                            <div class="form-group">
                                <label for="password_confirmation">Passwort bestätigen</label>
                                <input class="form-control" type="password" name="password_confirmation" id="password_confirmation" value="">
                                @if($errors->has('password_confirmation'))
                                    <ul>
                                        @foreach($errors->get('password_confirmation') as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                            <!--
                                Login
                            -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="submit" value="Passwort zurücksetzen" class="btn btn-default">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>


        </div>

    </div>



@endsection












    <div class="container">
        <div class="row">
            <div class="col s12 m6 offset-m3 l4 offset-l4">
                <div class=" white-container">




                </div>
            </div>
        </div>
    </div>

