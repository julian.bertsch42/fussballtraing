@extends('layouts.auth.app')

@section('content')

    <div class="sponsor_wrapper">

        <div class="bg">
            <div class="image"></div>
            <div class="sponsor">
                <p>SPONSOR</p>
            </div>
        </div>

        <div class="container">



            <h1>
                Fussballtrainer Software
            </h1>

            @if(Session::has('message') || Session::has('status'))
                <div class="row">
                    <div class="col-sm-12">
                        <div class="section">
                            <div class="primary-container">
                                {{ Session::get('message') ?? Session::get('status') }}
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <form method="POST" action="{{ url('/password/email') }}">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-12">
                        <div class="section">
                            <!--
                                E-Mail
                            -->
                            <div class="form-group">
                                <label class="control-label col-sm-4" for="email">Email</label>

                                <input class="form-control" type="email" name="email" id="email" value="{{ old('email') }}" required>

                                @if($errors->has('email'))
                                    <ul>
                                        @foreach($errors->get('email') as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                            <!--

                            -->
                            <div class="input-field col s12">
                                <input type="submit" value="Passwort anfordern" class="btn btn-default">
                                <p>
                                    <a href="{{ url('/login') }}">Zurück zum Login</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </form>


        </div>

    </div>



@endsection
















    <div class="container">



                </div>
            </div>
        </div>
    </div>

