﻿@extends('layouts.app')

@section('content')
    <div class="main-container">

		<section class="cover fullscreen image-slider slider-all-controls controls-inside parallax">
		        <ul class="slides">
		            <li class="overlay image-bg">
		                <div class="background-image-holder">
		                    <img alt="image" class="background-image" src="vendor/main/bilder/slider-fussball-autogrammkarten.jpg">
		                </div>
		                <div class="container v-align-transform">
		                    <div class="row">
		                        <div class="col-md-6 col-sm-8">
		                            <h1 class="mb40 mb-xs-16 large">Mannschaft und Verein managen!</h1>
		                            <h6 class="uppercase mb16">Hebe deine Mannschaft auf einen professionellen Level!</h6>
		                            <p class="lead mb40">
		                                Entwickle Trainingsübungen, organisiere Strafen, Rücken- sowie Telefonnummern, gewinne neue Sponsoren und vieles mehr!
		                            </p>
		                          
		                        </div>
		                    </div>

		                </div>

		            </li>
		        </ul>
		    </section>


        <section class="image-bg parallax overlay">
            <div class="background-image-holder">
                <img alt="image" class="background-image" src="{{asset('vendor/main/bilder/newsletter-anmelden.jpg')}}">
            </div>
            <div class="container">
                <div class="row mb40 mb-xs-24">
                    <div class="col-sm-12 text-center">
                        <h3 class="mb8">Meld dich jetzt für den Newsletter an!</h3>
                        <p>Wir halten dich auf dem Laufenden! Du musst die E-Mail bestätigen, wenn du am Newsletter angemeldet sein willst.</p>
                    </div>
                </div>

                <div class="row mb80 mb-xs-24">
                    <div class="col-sm-6 col-sm-offset-3">
                        {{--<form class="form-newsletter halves" data-success="ACHTUNG: Sie müssen die E-Mail bestätigen!" data-error="Please fill all fields correctly." success-redirect="">
                            <input name="email" class="mb0 transparent validate-email validate-required signup-email-field" placeholder="Deine E-Mail Adresse" type="text">
                            <button type="submit" class="mb0">Anmelden</button>
                            <iframe srcdoc="<!-- Begin MailChimp Signup Form --> <div id=&quot;mc_embed_signup&quot;> <form action=&quot;https://fusballtrainer-software.us17.list-manage.com/subscribe/post?u=afbbe64932975c5135178592e&amp;amp;id=12f48dce72&quot; method=&quot;post&quot; id=&quot;mc-embedded-subscribe-form&quot; name=&quot;mc-embedded-subscribe-form&quot; class=&quot;validate&quot; target=&quot;_blank&quot; novalidate>     <div id=&quot;mc_embed_signup_scroll&quot;> 	<h2>Subscribe to our mailing list</h2> <div class=&quot;indicates-required&quot;><span class=&quot;asterisk&quot;>*</span> indicates required</div> <div class=&quot;mc-field-group&quot;> 	<label for=&quot;mce-EMAIL&quot;>Email Address  <span class=&quot;asterisk&quot;>*</span> </label> 	<input type=&quot;email&quot; value=&quot;&quot; name=&quot;EMAIL&quot; class=&quot;required email&quot; id=&quot;mce-EMAIL&quot;> </div> 	<div id=&quot;mce-responses&quot; class=&quot;clear&quot;> 		<div class=&quot;response&quot; id=&quot;mce-error-response&quot; style=&quot;display:none&quot;></div> 		<div class=&quot;response&quot; id=&quot;mce-success-response&quot; style=&quot;display:none&quot;></div> 	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->     <div style=&quot;position: absolute; left: -5000px;&quot; aria-hidden=&quot;true&quot;><input type=&quot;text&quot; name=&quot;b_afbbe64932975c5135178592e_12f48dce72&quot; tabindex=&quot;-1&quot; value=&quot;&quot;></div>     <div class=&quot;clear&quot;><input type=&quot;submit&quot; value=&quot;Subscribe&quot; name=&quot;subscribe&quot; id=&quot;mc-embedded-subscribe&quot; class=&quot;button&quot;></div>     </div> </form> </div>  <!--End mc_embed_signup-->" class="mail-list-form">
                            </iframe>
                        </form>--}}
                        <a class="btn btn-sucess" style="text-align: center" href="{{route('main.newsletter.anmelden')}}">Newsletter anmelden</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 text-center spread-children-large">
                        {{--<img alt="pic" class="image-xxs mb-xs-8 fade-1-4" src="{{asset('vendor/main/bilder/c1.png')}}">
                        <img alt="pic" class="image-xxs mb-xs-8 fade-1-4" src="{{asset('vendor/main/bilder/c2.png')}}">
                        <img alt="pic" class="image-xxs mb-xs-8 fade-1-4" src="{{asset('vendor/main/bilder/c3.png')}}">
                        <img alt="pic" class="image-xxs mb-xs-8 fade-1-4" src="{{asset('vendor/main/bilder/c4.png')}}">--}}
                    </div>
                </div>

            </div>

        </section>
    </div>

    {{--<section>
        <div class="container">
            <div class="row v-align-children">
                <div class="col-md-7 col-sm-6 text-center mb-xs-24">
                    <img class="cast-shadow" alt="Screenshot" src="{{asset('vendor/main/bilder/screenshot.jpg')}}">
                </div>
                <div class="col-md-4 col-md-offset-1 col-sm-5 col-sm-offset-1">
                    <h3>Fussballsoftware der modernen Art!<br></h3>
                    <p>Mit der Fussballtrainer Software startet eine neue Ära des Managements im Fussball. Verschiedene Versionen um im Fussball erfolgreich zu sein.</p>

                </div>
            </div>

        </div>

    </section>--}}
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-1">
                    <div class="feature bordered">
                        <h2 class="mb64 mb-xs-24">Trainingsübungen</h2>
                        <p class="mb80 mb-xs-24">Hier kreierst Du neue Taktiken und Trainingsübungen für Jugendmannschaften von der G- bis zur A-Jugend, die Erste Mannschaft und das Alte-Herren-Team. Erstelle, speichere und drucke individuelle Taktik- sowie Trainingspläne aus und stelle so die Weiterentwicklung von Einzelspielern und Mannschaften sicher.</p>
                        <a class="btn btn-lg btn-filled inner-link" href="{{route('main.fussballtraining')}}">Mehr Infos</a>
                    </div>
                </div>
                <div class="col-sm-5">
                    <img class="mt80 mt-xs-0 mb24" alt="Pic" src="{{asset('vendor/main/bilder/funktionen/fussballtraining-trainingsuebung.png')}}">
                    <img class="col-md-pull-4 relative" alt="Pic" src="{{asset('vendor/main/bilder/fussballtrainer-software.jpg')}}">
                </div>
            </div>

        </div>

    </section><section class="bg-primary">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3 class="mb0 inline-block p32 p0-xs"><i class="fa fa-futbol-o" style="color: black;" aria-hidden="true"></i> Wechsel jetzt zur zukünftigen Nummer Eins! <i class="fa fa-futbol-o" style="color: black;" aria-hidden="true"></i><br>
                        ... Und erreiche ein neues Management-Level!<br></h3>
                    {{--<a class="btn btn-lg btn-white mb8 mt-xs-24" href="#">zum portal</a>--}}
                </div>
            </div>

        </div>

    </section>

    <section class="image-edge">
        <div class="col-md-6 col-sm-4 p0">
            <img alt="autogrammkarten" class="mb-xs-24" src="{{asset('vendor/main/bilder/funktionen/fussball-autogrammkarten.png')}}">
        </div>
        <div class="container">
            <div class="col-md-5 col-md-offset-1 col-sm-7 col-sm-offset-1 v-align-transform right">
                <h2 class="mb40 mb-xs-16 bold">Autogrammkarten<br></h2>
                <h6 class="uppercase mb16">Vermarktung wie bei den Profis? Kein Problem!<br></h6>
                <p class="lead mb40">Mit individuell designbaren Autogrammkarten werden auch Amateurspieler zu echten Stars – das freut auch die Sponsoren. Erhöhe die Reichweite Deines Vereins und Sponsors, gewinne neue Fans und Zuschauer durch professionelle Autogrammkarten mit Wiedererkennungswert.<br><br>“Tipp: Verkaufen Sie die Autogrammkarten innerhalb Ihrer Mannschaft oder der Teilnehmer, somit haben Sie eine weitere Einnahmequelle für Ihr Fussballcamp oder der Fußballschule!"<br><br></p>

            </div>
        </div>

    </section>

    <section class="bg-primary">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3 class="mb0 inline-block p32 p0-xs bold">Wir werden nach aktuellem Stand insgesamt 13 Alleinstellungsmerkmale haben, welche du bei keinem Konkurrenten finden wirst.<br></h3><h3 class="mb0 inline-block p32 p0-xs">*Stand 12.11.17*<br></h3>

                </div>
            </div>

        </div>

    </section>

    <section class="image-square left">
        <div class="col-md-6 image">
            <div class="background-image-holder">
                <img alt="image" class="background-image" src="{{asset('vendor/main/bilder/small1.jpg')}}">
            </div>
        </div>
        <div class="col-md-6 col-md-offset-1 content">
            <h3 class="bold">VERLETZUNGSPORTAL</h3><h3>Kreuzbandriss, Zerrung, Prellung<br></h3>
            <p class="mb0">Im Verletzungsportal legst Du individuell für jeden Spieler eine Verletzungshistorie an. Zudem wird neben Hintergrundinformationen über Art und mögliche Therapiemaßnahmen der Verletzung die prognostizierte Ausfalldauer eines Spielers angezeigt.</p>
        </div>
    </section>

    <section class="image-square right">
        <div class="col-md-6 image">
            <div class="background-image-holder">
                <img alt="image" class="background-image" src="{{asset('vendor/main/bilder/small2.jpg')}}">
            </div>
        </div>
        <div class="col-md-6 content">
            <h3 class="bold">ERNÄHRUNGSPORTAL</h3><h3>Nie wieder unfitte oder übergewichtige Spieler!</h3>
            <p class="mb0">Mithilfe des Ernährungsportals werden für jeden angelegten Spieler auf Basis seiner Körper- und Fitnesswerte individuelle Ernährungspläne erzeugt – vollautomatisch, fußballspezifisch und auf Basis ernährungsphysiologischer Erkenntnisse.</p>
        </div>
    </section>

    <section class="bg-primary">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3 class="mb0 inline-block p32 p0-xs">"Verpasse keine Neuigkeiten rund um die Fussballtrainer-Software mehr!"<br></h3>
                    <br /><br />
                    <a class="btn btn-lg btn-white mb8 mt-xs-24" href="{{route('main.newsletter.anmelden')}}">Für den Newsletter anmelden</a>

                </div>
            </div>

        </div>

    </section>

    <section class="image-bg bg-light parallax overlay pt160 pb160 pt-xs-80 pb-xs-80">
        <div class="background-image-holder">
            <img alt="image" class="background-image" src="{{asset('vendor/main/bilder/particle.jpg')}}">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-8">

                    <h3 class="mb32 bold">Mehr Sponsorengelder einnehmen!<br></h3><h3 class="mb32">"Wir fangen gerade erst an die ultimative Plattform zu erschaffen!<br></h3>
                    <p>Durch unsere Features haben Sie die Möglichkeit die Mannschaftskasse oder Vereinskasse zu verbessern. Zeigen Sie Ihren Sponsoren wie Sie Werbung machen können und verlangen Sie so mehr Geld. Und schon haben Sie die Kosten für die Fussballtrainer Software schon wieder reingeholt. Wir können jetzt noch nicht viel verraten, aber wir haben verdammt viele Features die Sie mit den Sponsoren kombinieren können.<br><br>Nach aktuellen Stand haben wir 22 Funktionen die man veröffentlichen kann. Aber auch Rom wurde nicht an einem Tag erbaut. Mit der Zeit werden wir immer mehr Features veröffentlichen. Und wir versprechen euch das einige Features von uns zum jetzigen Zeitpunkt(Stand: 12.11.17) in keiner anderen Software vorhanden ist.<br></p>
                </div>
            </div>

        </div>

    </section>

    <section class="image-bg overlay parallax">
        <div class="background-image-holder">
            <img alt="Background" class="background-image" src="{{asset('vendor/main/bilder/cover1.jpg')}}">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2 class="mb16">Gestalte deine Plattform so, wie du sie brauchst!</h2>
                    <p class="lead mb64">Wir senken die Kosten und du kannst zusätzlich noch Geld für die Mannschaftskasse erwitschaften!<br></p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="feature feature-1 boxed" style="height:300px;">
                        <div class="text-center">
                            <i class="icon ti ti-pencil-alt"></i>
                            <h5 class="uppercase">Grundfunktionen</h5>
                        </div>
                        <p>Alle Grundfunktionen sind bei uns Kostenlos! Melden dich noch heute an und nutze unsere Grundfunktionen absolut kostenlos und so lange du willst.<br></p>
                    </div>

                </div><div class="col-sm-4">
                    <div class="feature feature-1 boxed" style="height:300px;">
                        <div class="text-center">
                            <i class="icon ti ti-unlock"></i>
                            <h5 class="uppercase">funktionen freischalten</h5>
                        </div>
                        <p>Warum mehr Geld bezahlen, wenn man einige Funktionen nicht braucht? Schalte jetzt selbst die Funktionen frei, die du auch wirklich brauchst. So sparst du nicht nur bares Geld, sondern auch Nerven.</p>
                    </div>

                </div>
                <div class="col-sm-4">
                    <div class="feature feature-1 boxed" style="height:300px;">
                        <div class="text-center">
                            <i class="icon fa fa-euro"></i>
                            <h5 class="uppercase">sponsoren</h5>
                        </div>
                        <p>Viele Funktionen können mit deinen eigenen Sponsoren verwendet werden. Verlange smoit mehr Geld von den Sponsoren.<br></p>
                    </div>

                </div>

            </div>

        </div>

    </section>

    <section>
        <div class="container">
            <div class="row v-align-children">
                <div class="col-sm-4 col-md-offset-1 mb-xs-24">
                    <h2 class="mb64 mb-xs-32">Kritisiere uns!<br></h2>
                    <div class="mb40 mb-xs-24">
                        <h5 class="uppercase bold mb16">Keine Angst, wir grätschen dich nicht weg<br></h5>
                        <p class="fade-1-4">Wir wollen das du uns ohne Gnade sagst was wir besser machen müssen. Egal ob Funktionen fehlen oder du Ideen hast.<br></p>
                        <p class="fade-1-4">Schreib uns einfach eine E-Mail, wir antworten dir garantiert.
                        <br><a href="mailto:support@fussbaltrainer-software.de">support@fussbaltrainer-software.de</a>
                        </p>
                    </div>
                    {{--<div class="mb40 mb-xs-24">
                        <h5 class="uppercase bold mb16">was muss ich dafür tun?<br></h5>
                        <p class="fade-1-4">Du musst jeden Monat 5 eigene vollständige Trainingsübungen inklusive Zeichnung erstellen.<br><br>Solltest du Interesse haben, so schreib uns eine E-Mail an: support@fussballtrainer-software.de<br></p>
                    </div>--}}
                </div>
                <div class="col-sm-5 col-sm-6 col-sm-offset-1 text-center">
                    <img alt="Screenshot" src="{{asset('vendor/main/bilder/kritikfaehigkeit.jpg')}}">
                </div>
            </div>

        </div>

    </section>

    <section class="bg-secondary">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="feature feature-3 mb-xs-24">
                        <div class="left">
                            <i class="ti ti-flag-alt icon-sm"></i>
                        </div>
                        <div class="right">
                            <h5 class="uppercase mb16">made in germany<br></h5>
                            <p>Made in Germany bedeutet Qualität. Erstellung, Qualitätskontrolle und Abnahme für unsere Kunden sind von enormer Bedeutung.<br></p>
                        </div>
                    </div>

                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="feature feature-3 mb-xs-24">
                        <div class="left">
                            <i class="ti ti-lock icon-sm"></i>
                        </div>
                        <div class="right">
                            <h5 class="uppercase mb16">256 Bit Verschlüsselung</h5>
                            <p>Eine 256 Bit Verschlüsselung ist selbstverständlich. So kann keiner Ihre sensiblen Daten abfangen.<br></p>
                        </div>
                    </div>

                </div>


                <div class="col-md-4 col-sm-6">
                    <div class="feature feature-3 mb-xs-24">
                        <div class="left">
                            <i class="fa fa-support icon-sm"></i>
                        </div>
                        <div class="right">
                            <h5 class="uppercase mb16">deutscher support<br></h5>
                            <p>Wir verlagern unseren Support nicht ins Ausland, sondern werden unseren Support mitten im Ruhrgebiet haben.<br></p>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-5">
                    <h4 class="uppercase">Schreiben Sie uns!</h4>
                    <p>Wenn Sie Anregungen, Ideen oder Kritik an uns richten möchten, so schreiben Sie uns einfach eine Nachricht! Wir antworten garantiert!</p>
                    <hr>
                    <p>E-Mail Adresse: support@fussballtrainer-software.de </p>
                </div>
                <div class="col-sm-6 col-md-5 col-md-offset-1">
                    <form method="POST" action="{{route('main.kontakt.send')}}">
                        {{csrf_field()}}
                        <input required name="name" placeholder="Dein Name" type="text">
                        <input required name="email" placeholder="Email Addresse" type="email">
                        <textarea required name="message" rows="4" placeholder="Nachricht"></textarea>
                        <button type="submit">Sende Nachricht</button>
                    </form>
                </div>
            </div>

        </div>

    </section>
@endsection