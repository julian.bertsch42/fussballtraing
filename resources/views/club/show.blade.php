@extends('layouts.vereinsverzeichnis.app')

@section('seo-title',$club->title." - ".env('APP_NAME'))
@section('seo-description', $seodescription)
@section('twitter',"@".$club->twitter)

@section('content')
    <div class="container" id="club-show">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-transparent">
                    <div class="panel-heading">Ergebnisse:</div>

                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <form method="GET" action="{{route('submissions.edit',$club->id)}}">
                                    <button class="btn btn-default pull-right" type="submit">Änderungsvorschlag</button>
                                </form>
                            </div>
                        </div>


                        @if(session()->has('message') && session('message') == 'submissionSuccess')
                            <div class="alert alert-success" style="margin: 10px 0">
                                Änderungsvorschlag wurde erfolgreich eingereicht.
                            </div>
                        @endif

                        <h1>Verein: {{$club->title}}</h1>

                        <div class="row">

                            <div class="col-md-3 col-md-push-9 sidebar">


                                <div class="table-responsive">
                                    <table class="table table-hover table-condensed borderless">

                                        <tr>
                                            <th>Vereinsname</th>
                                            <td>{{$club->title}}</td>
                                        </tr>

                                        <tr>
                                            <th>Strasse</th>
                                            <td>{{$club->street}}</td>
                                        </tr>

                                        <tr>
                                            <th>Stadt</th>
                                            <td>{{$club->city}}</td>
                                        </tr>

                                        <tr>
                                            <th>Telefon</th>
                                            <td>{{$club->phone}}</td>
                                        </tr>

                                        <tr>
                                            <th>PLZ</th>
                                            <td>{{$club->zip}}</td>
                                        </tr>

                                        <tr>
                                            <th>FAX</th>
                                            <td>{{$club->fax}}</td>
                                        </tr>

                                        <tr>
                                            <th>Email</th>
                                            <td>{{$club->email}}</td>
                                        </tr>

                                        <tr>
                                            <th>Stadion</th>
                                            <td>{{$club->stadium}}</td>
                                        </tr>

                                        <tr>
                                            <th>Webseite</th>
                                            <td><a href="{{$club->uri}}" target="_blank">{{$club->uri}}</a></td>
                                        </tr>

                                        <tr>
                                            <th>Facebook</th>
                                            <td>{{$club->facebook}}</td>
                                        </tr>

                                        <tr>
                                            <th>Twitter</th>
                                            <td>{{$club->twitter}}</td>
                                        </tr>

                                        <tr>
                                            <th>Youtube</th>
                                            <td>{{$club->youtube}}</td>
                                        </tr>

                                        <tr>
                                            <th>Instagram</th>
                                            <td>{{$club->instagram}}</td>
                                        </tr>

                                        <tr>
                                            <th>Verband</th>
                                            <td>{{$association->title}}</td>
                                        </tr>

                                    </table>
                                </div>

                            </div>

                            <div class="col-md-9 col-md-pull-3 content">

                                {!! $club->description !!}

                                @if(empty($club->description))
                                    <div class="alert alert-warning">
                                        <p>Für diesen Verein wurde noch keine Beschreibung hinzugefügt. <a href="{{route('submissions.edit',$club->id)}}">Teile dein Wissen und füge jetzt eine Beschreibung hinzu</a>.</p>
                                    </div>
                                @endif

                            </div>

                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
