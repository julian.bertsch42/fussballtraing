@extends('layouts.blog.app')

@section('seo-title', ($post->{"seo-title"} ?? $post->title))
@section('seo-description', $post->bodyText())
@section('seo-image', asset($post->image))

@section('content')

    <div class="st--post--area sp" style="padding-top: 25px;">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <div class="breadcrumb" style="width:100%">
                        <a class="breadcrumb__step {{empty($category) ? 'breadcrumb__step--active' : ''}}" href="{{route('blog.index')}}">Blog</a>
                        @if(!empty($category) && !empty($subCategory))
                            <a class="breadcrumb__step" href="{{$category->link()}}">{{$category->name}}</a>
                        @endif
                        @if(!empty($subCategory))
                            <a class="breadcrumb__step" href="{{$subCategory->link()}}">{{$subCategory->name}}</a>
                        @endif
                        @if(!empty($post))
                            <a class="breadcrumb__step breadcrumb__step--active" href="{{$post->link()}}">{{$post->title}}</a>
                        @endif
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="post--content--single">
                        <div class="post_img">
                            <a href="{{asset($post->image)}}" data-lightbox="post">
                                <img src="{{asset($post->image)}}" alt="">
                            </a>
                            <span style="margin-top: 10px;" class="image-copyright">Quelle: <a href="http://www.fussballtrainer-software.de">www.fussballtrainer-software.de</a></span>
                        </div>
                        <div class="st--tags st--info bg-{{$category->color}}">
                            <a href="#">{{$category->name}}</a>
                        </div>
                        <h2><a href="#">{{$post->title}}</a></h2>
                        <div class="st--meta st--info">
                            <div class="st--author--info clearfix">
                                <div class="author--img pull-left">
                                    <img src="{{asset('vendor/blog/img/author.jpg')}}" alt="">
                                </div>
                                <a href="#" class="st--author">{{$post->user->username}}</a>
                            </div>
                            <a href="#" class="st--post--date">{{$post->created_at->format('d.m.Y H:i')}}</a>
                        </div>
                        <div>
                            {!! $post->body !!}
                        </div>
                        <div class="tag--list">
                            @foreach($post->tags as $tag)
                                <span class="label label-default"><a href="#">{{$tag}}</a></span>
                            @endforeach
                        </div>
                    </div>


                    <!-- Sharingbutton Facebook -->
                    <a class="resp-sharing-button__link" href="https://facebook.com/sharer/sharer.php?u={{Request::url()}}" target="_blank" aria-label="Share on Facebook">
                        <div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--large">
                            <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                <i class="zmdi zmdi-facebook"></i>
                            </div>
                            Share on Facebook
                        </div>
                    </a>

                    <!-- Sharingbutton Twitter -->
                    <a class="resp-sharing-button__link" href="https://twitter.com/intent/tweet/?text=Fussball%20Software&amp;url={{Request::url()}}" target="_blank" aria-label="Share on Twitter">
                        <div class="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--large">
                            <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                <i class="zmdi zmdi-twitter"></i>
                            </div>
                            Share on Twitter
                        </div>
                    </a>

                    <!-- Sharingbutton Google+ -->
                    <a class="resp-sharing-button__link" href="https://plus.google.com/share?url={{Request::url()}}" target="_blank" aria-label="Share on Google+">
                        <div class="resp-sharing-button resp-sharing-button--google resp-sharing-button--large">
                            <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                <i class="zmdi zmdi-google"></i>
                            </div>
                            Share on Google+
                        </div>
                    </a>

                    <!-- Sharingbutton E-Mail -->
                    <a class="resp-sharing-button__link" href="mailto:?subject=Fussball%20Software&amp;body={{Request::url()}}" target="_self" aria-label="Share by E-Mail">
                        <div class="resp-sharing-button resp-sharing-button--email resp-sharing-button--large">
                            <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                <i class="zmdi zmdi-email"></i>
                            </div>
                            Share by E-Mail
                        </div>
                    </a>

                    <!-- Sharingbutton WhatsApp -->
                    <a class="resp-sharing-button__link" href="whatsapp://send?text=Fussball%20Software%20{{Request::url()}}" target="_blank" aria-label="Share on WhatsApp">
                        <div class="resp-sharing-button resp-sharing-button--whatsapp resp-sharing-button--large">
                            <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                <i class="zmdi zmdi-whatsapp"></i>
                            </div>
                            Share on WhatsApp
                        </div>
                    </a>

                </div>
                <!-- /.st-sidebar-column -->
                <div class="col-md-4 st--sidebar--column col-sm-12">

                    <aside class="widget  st--widget">
                        <div class="st--widget--title">
                            <h4>Suchen</h4>
                        </div>
                        <div class="st--widget--inner--box sidebar-box">
                            <div class="search--column">
                                <form class="st--search--box" action="{{route('blog.search.redirect')}}" method="POST">
                                    {{csrf_field()}}
                                    <div>
                                        <input type="search" name="search" placeholder="Enter Keyword to Search" value="{{$search ?? null}}">
                                        <button type="submit"><i class="zmdi zmdi-search"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </aside>

                    <aside class="widget st--widget">
                        <div class="st--widget--title">
                            <h4>Teile uns</h4>
                        </div>

                        <div class="st--social-follow sidebar-box" style="padding: 7px;">
                            <!-- Sharingbutton Facebook -->
                            <a class="resp-sharing-button__link" href="https://facebook.com/sharer/sharer.php?u={{Request::url()}}" target="_blank" aria-label="Share on Facebook">
                                <div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--large">
                                    <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                        <i class="zmdi zmdi-facebook"></i>
                                    </div>
                                    Share on Facebook
                                </div>
                            </a>

                            <!-- Sharingbutton Twitter -->
                            <a class="resp-sharing-button__link" href="https://twitter.com/intent/tweet/?text=Fussball%20Software&amp;url={{Request::url()}}" target="_blank" aria-label="Share on Twitter">
                                <div class="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--large">
                                    <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                        <i class="zmdi zmdi-twitter"></i>
                                    </div>
                                    Share on Twitter
                                </div>
                            </a>

                            <!-- Sharingbutton Google+ -->
                            <a class="resp-sharing-button__link" href="https://plus.google.com/share?url={{Request::url()}}" target="_blank" aria-label="Share on Google+">
                                <div class="resp-sharing-button resp-sharing-button--google resp-sharing-button--large">
                                    <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                        <i class="zmdi zmdi-google"></i>
                                    </div>
                                    Share on Google+
                                </div>
                            </a>

                            <!-- Sharingbutton E-Mail -->
                            <a class="resp-sharing-button__link" href="mailto:?subject=Fussball%20Software&amp;body={{Request::url()}}" target="_self" aria-label="Share by E-Mail">
                                <div class="resp-sharing-button resp-sharing-button--email resp-sharing-button--large">
                                    <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                        <i class="zmdi zmdi-email"></i>
                                    </div>
                                    Share by E-Mail
                                </div>
                            </a>

                            <!-- Sharingbutton WhatsApp -->
                            <a class="resp-sharing-button__link" href="whatsapp://send?text=Fussball%20Software%20{{Request::url()}}" target="_blank" aria-label="Share on WhatsApp">
                                <div class="resp-sharing-button resp-sharing-button--whatsapp resp-sharing-button--large">
                                    <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                        <i class="zmdi zmdi-whatsapp"></i>
                                    </div>
                                    Share on WhatsApp
                                </div>
                            </a>
                        </div>

                    </aside>

                    <aside class="widget st--widget">
                        <div class="st--widget--title">
                            <h4>Folge uns</h4>
                        </div>

                        <div class="st--social-follow sidebar-box" style="padding: 7px;">
                            <a href="https://www.facebook.com/fussballtrainer.software/" target="_blank" class="resp-sharing-button__link">
                                <div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--large">
                                    <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                        <i class="zmdi zmdi-facebook"></i>
                                    </div>
                                    Like on Facebook
                                </div>
                            </a>

                            <a href="https://www.instagram.com/fussballtrainer.software/" target="_blank" class="resp-sharing-button__link">
                                <div class="resp-sharing-button resp-sharing-button--instagram resp-sharing-button--large">
                                    <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                        <i class="zmdi zmdi-instagram"></i>
                                    </div>
                                    Like on Instagram
                                </div>
                            </a>

                            <a href="https://www.youtube.com/channel/UCBEOxXb6CdX38cSvcQtfXeg?view_as=subscriber" target="_blank" class="resp-sharing-button__link">
                                <div class="resp-sharing-button resp-sharing-button--pinterest resp-sharing-button--large">
                                    <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                        <i class="zmdi zmdi-youtube"></i>
                                    </div>
                                    Like on Youtube
                                </div>
                            </a>

                            <a href="https://plus.google.com/112379064220231236403" target="_blank" class="resp-sharing-button__link">
                                <div class="resp-sharing-button resp-sharing-button--google resp-sharing-button--large">
                                    <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                        <i class="zmdi zmdi-google"></i>
                                    </div>
                                    Like on Google+
                                </div>
                            </a>

                        </div>

                    </aside>
                    <!-- /.widget -->

                    <aside class="widget  st--widget">
                        <div class="st--widget--title">
                            <h4>Kategorien</h4>
                        </div>
                        <div class="st--widget--inner--box sidebar-box">
                            <ul>
                                @foreach($blogCategories as $cat)
                                    <li><a href="{{$cat->link()}}">{{$cat->name}} ({{$cat->active_posts_count}})</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </aside>
                    <!-- /.widget -->

                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script>

        var breadcrumb = document.querySelector('.breadcrumb');
        var breadcrumbSteps = document.querySelectorAll('.breadcrumb__step');
        [].forEach.call(breadcrumbSteps, function (item, index) {
            item.onclick = function () {
                item.classList.toggle('breadcrumb__step--active');
            };
        });

    </script>

@endsection