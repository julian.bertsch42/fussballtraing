@extends('layouts.blog.app')

@if(!empty($category->seotitle) || !empty($subCategory->seotitle))
    @section('seo-title', $subCategory->seotitle ?? $category->seotitle)
@endif
@if(!empty($category->seodescription) || !empty($subCategory->seodescription))
    @section('seo-description', $subCategory->seodescription ?? $category->seodescription)
@endif

@section('content')
<div class="home--area--6">
    <div class="home--carousel--6 st--slide--arrow--2">

        @foreach($last5 as $post)

            <div class="st--single--slide" style="background-image: url({{asset($post->image)}});">
                <div class="st--inner">
                    <div class="st--content" style="width: 100%">
                        <div class="st--tags st--info bg-{{$post->category->color}}">
                            <a href="#">{{$post->category->name}}</a>
                        </div>
                        <h5 style="font-size: 22px;"><a href="{{$post->link()}}">{{$post->title}}</a></h5>
                    </div>
                </div>
            </div>
        @endforeach

    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumb" style="width:100%; margin-top:25px;">
                <a class="breadcrumb__step {{empty($category) ? 'breadcrumb__step--active' : ''}}" href="{{route('blog.index')}}">Blog</a>
                @if(!empty($category))
                <a class="breadcrumb__step" href="{{$category->link()}}">{{$category->name}}</a>
                @endif
                @if(!empty($subCategory))
                    <a class="breadcrumb__step breadcrumb__step--active" href="{{$subCategory->link()}}">{{$subCategory->name}}</a>
                @endif
            </div>
        </div>
    </div>
</div>


<div style="margin-top: 40px;"></div>

<!-- /.st-category-area -->
<div class="st--blog--area spb">
    <div class="container">
        <div class="row">
            <div class="col-md-8 st--post--column col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="st--block--title--1">
                            <h3>Latest Post</h3>
                        </div>
                    </div>
                </div>
                <div class="row st--common--masonry">

                    @foreach($posts as $post)
                    <!-- /.st-single-post -->
                    <div class="col-sm-6 st--single--post">
                        <div class="st--inner">
                            <div class="st--post--top relative">
                                <div class="st--post--img">
                                    <img src="{{asset($post->image)}}" alt="">
                                </div>
                                <a href="#" class="st--tags st--sticky--cat--1 bg-{{$post->category->color}}">{{$post->category->name}}</a>
                            </div>
                            <div class="st--post--content">
                                <h4><a href="{{$post->link()}}">{{$post->title}}</a></h4>
                                <div class="st--post--meta st--info">
                                            <span class="posted-by">
                                        Posted by <a href="#" class="st--author">{{$post->user->username}}</a>
                                    </span>
                                    <span class="post-date">
                                        <a href="#">{{$post->created_at->format('d.m.Y')}}</a>
                                    </span>
                                </div>
                                <p>{!! str_limit($post->bodyText()) !!}</p>
                                <a href="{{$post->link()}}" class="st--button">Read More <i class="zmdi zmdi-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- /.st-single-post -->
                    @endforeach


                </div>
                {{--<div class="st--pagination--1">
                    <ul>
                        <li><a href="#">1</a></li>
                        <li><span>2</span></li>
                        <li><a href="#"><i class="zmdi zmdi-long-arrow-right"></i></a></li>
                    </ul>
                </div>--}}
                {{$posts->links()}}
            </div>
            <div class="col-md-4 st--sidebar--column col-sm-12">

                <aside class="widget  st--widget">
                    <div class="st--widget--title">
                        <h4>Login / Logout</h4>
                    </div>
                    <div class="st--widget--inner--box sidebar-box">
                        @guest
                            <a class="btn btn-sm st--button" href="{{route('register')}}">Registrieren</a>
                            <span style="width: 10px;"></span>
                            <a class="btn btn-sm st--button c2-bg" href="{{route('login')}}">Einloggen</a>
                        @endguest
                        @auth
                            <a class="btn btn-sm st--button" href="{{ route('logout') }}"onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Ausloggen</a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        @endauth
                    </div>
                </aside>


                <aside class="widget  st--widget">
                    <div class="st--widget--title">
                        <h4>Suchen</h4>
                    </div>
                    <div class="st--widget--inner--box sidebar-box">
                        <div class="search--column">
                            <form class="st--search--box" action="{{route('blog.search.redirect')}}" method="POST">
                                {{csrf_field()}}
                                <div>
                                    <input type="search" name="search" placeholder="Enter Keyword to Search" value="{{$search ?? null}}">
                                    <button type="submit"><i class="zmdi zmdi-search"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </aside>

                <aside class="widget st--widget">
                    <div class="st--widget--title">
                        <h4>Teile uns</h4>
                    </div>

                    <div class="st--social-follow sidebar-box" style="padding: 7px;">
                        <!-- Sharingbutton Facebook -->
                        <a class="resp-sharing-button__link" href="https://facebook.com/sharer/sharer.php?u={{Request::url()}}" target="_blank" aria-label="Share on Facebook">
                            <div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--large">
                                <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                    <i class="zmdi zmdi-facebook"></i>
                                </div>
                                Share on Facebook
                            </div>
                        </a>

                        <!-- Sharingbutton Twitter -->
                        <a class="resp-sharing-button__link" href="https://twitter.com/intent/tweet/?text=Fussball%20Software&amp;url={{Request::url()}}" target="_blank" aria-label="Share on Twitter">
                            <div class="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--large">
                                <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                    <i class="zmdi zmdi-twitter"></i>
                                </div>
                                Share on Twitter
                            </div>
                        </a>

                        <!-- Sharingbutton Google+ -->
                        <a class="resp-sharing-button__link" href="https://plus.google.com/share?url={{Request::url()}}" target="_blank" aria-label="Share on Google+">
                            <div class="resp-sharing-button resp-sharing-button--google resp-sharing-button--large">
                                <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                    <i class="zmdi zmdi-google"></i>
                                </div>
                                Share on Google+
                            </div>
                        </a>

                        <!-- Sharingbutton E-Mail -->
                        <a class="resp-sharing-button__link" href="mailto:?subject=Fussball%20Software&amp;body={{Request::url()}}" target="_self" aria-label="Share by E-Mail">
                            <div class="resp-sharing-button resp-sharing-button--email resp-sharing-button--large">
                                <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                    <i class="zmdi zmdi-email"></i>
                                </div>
                                Share by E-Mail
                            </div>
                        </a>

                        <!-- Sharingbutton WhatsApp -->
                        <a class="resp-sharing-button__link" href="whatsapp://send?text=Fussball%20Software%20{{Request::url()}}" target="_blank" aria-label="Share on WhatsApp">
                            <div class="resp-sharing-button resp-sharing-button--whatsapp resp-sharing-button--large">
                                <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                    <i class="zmdi zmdi-whatsapp"></i>
                                </div>
                                Share on WhatsApp
                            </div>
                        </a>
                    </div>

                </aside>
                <!-- /.widget -->

                <aside class="widget st--widget">
                    <div class="st--widget--title">
                        <h4>Folge uns</h4>
                    </div>

                    <div class="st--social-follow sidebar-box" style="padding: 7px;">

                        <a href="https://www.facebook.com/fussballtrainer.software/" target="_blank" class="resp-sharing-button__link">
                            <div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--large">
                                <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                    <i class="zmdi zmdi-facebook"></i>
                                </div>
                                Like on Facebook
                            </div>
                        </a>

                        <a href="https://www.instagram.com/fussballtrainer.software/" target="_blank" class="resp-sharing-button__link">
                            <div class="resp-sharing-button resp-sharing-button--instagram resp-sharing-button--large">
                                <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                    <i class="zmdi zmdi-instagram"></i>
                                </div>
                                Like on Instagram
                            </div>
                        </a>

                        <a href="https://www.youtube.com/channel/UCBEOxXb6CdX38cSvcQtfXeg?view_as=subscriber" target="_blank" class="resp-sharing-button__link">
                            <div class="resp-sharing-button resp-sharing-button--pinterest resp-sharing-button--large">
                                <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                    <i class="zmdi zmdi-youtube"></i>
                                </div>
                                Like on Youtube
                            </div>
                        </a>

                        <a href="https://plus.google.com/112379064220231236403" target="_blank" class="resp-sharing-button__link">
                            <div class="resp-sharing-button resp-sharing-button--google resp-sharing-button--large">
                                <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
                                    <i class="zmdi zmdi-google"></i>
                                </div>
                                Like on Google+
                            </div>
                        </a>

                    </div>

                </aside>
                <!-- /.widget -->

                <aside class="widget  st--widget">
                    <div class="st--widget--title">
                        <h4>Kategorien</h4>
                    </div>
                    <div class="st--widget--inner--box sidebar-box">
                        <ul>
                            @foreach($blogCategories as $cat)
                                <li><a href="{{$cat->link()}}">{{$cat->name}} ({{$cat->active_posts_count}})</a></li>
                            @endforeach
                        </ul>
                    </div>
                </aside>
                <!-- /.widget -->

            </div>
            <!-- /.st-sidebar-column -->
        </div>
    </div>
</div>
@endsection

@section('script')

    <script>

        var breadcrumb = document.querySelector('.breadcrumb');
        var breadcrumbSteps = document.querySelectorAll('.breadcrumb__step');
        [].forEach.call(breadcrumbSteps, function (item, index) {
            item.onclick = function () {
                item.classList.toggle('breadcrumb__step--active');
            };
        });

    </script>

@endsection