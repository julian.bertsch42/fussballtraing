@extends('layouts.vereinsverzeichnis.app')

@section('content')
    <div class="container" id="search-results">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-transparent">
                    <div class="panel-heading">Ergebnisse:</div>

                    <div class="panel-body">

                        <div class="search-field">
                            <form method="GET" action="{{route('search.search')}}">

                                <input type="text" name="search" placeholder="Verein eingeben . . ." value="{{$text}}" />

                            </form>
                        </div>

                        <hr />


                        <div class="results">

                            @foreach($clubs as $result)

                                <div class="result">
                                    <p><a href="{{route('club.show', str_replace([' ','/'],'-',$result->title))}}" target="_blank">{{$result->title}}</a></p>
                                    <small><i>{{$result->type == 1 ? "Verein" : "Verband"}}</i> | <i>{{$result->zip}} {{$result->city}}</i> | <i><a href="{{$result->uri}}" target="_parent">{{$result->uri}}</a> </i></small>
                                </div>
                                <hr />
                            @endforeach

                            {{ $clubs->links() }}

                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#myTabs a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        })
    </script>
@endsection
