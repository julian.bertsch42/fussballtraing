@extends('layouts.vereinsverzeichnis.start')

@section('content')

    @if(session()->has('message') && session('message') == 'submissionSuccess')
        <div class="col-md-8 col-md-offset-2">
            <div class="alert alert-success" style="margin: 10px 0">
                Vorschlag für neuen Verein wurde erfolgreich eingereicht.
            </div>
        </div>
    @endif

    <div class="search">
        <div>

            <div class="logo">
                Fussballvereine Suchen
            </div>

            <form method="GET" action="{{route('search.search')}}">

                <input type="text" name="search" placeholder="Verein eingeben . . ." />

            </form>

            <div class="characters">
                <span>
                    @foreach(range('A', 'Z') as $char)
                        <a href="{{route('search.search',['text' => "begins::".$char])}}">{{$char}}</a>
                    @endforeach
                </span>
            </div>

            {{--<div class="links">
                <span>Erweiterte Suche</span>
            </div>--}}
        </div>
    </div>

@endsection
