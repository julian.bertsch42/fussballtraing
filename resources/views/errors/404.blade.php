@extends('layouts.error.app')

@section('content')
    <noscript>
        <div id="noscript-warning">
            This site works best with Javascript enabled, as you can plainly see.
        </div>
    </noscript>

    <div id="wrap">

        <!-- === WORDSEARCH === -->
        <div id="wordsearch">
            <ul>
                <li>F</li>
                <li>Q</li>
                <li>U</li>
                <li>T</li>
                <li>G</li>
                <li>O</li>
                <li>B</li>
                <li>X</li>
                <li>D</li>
                <li>G</li>
                <li>U</li>
                <li>P</li>
                <li class="one">4</li>
                <li class="two">0</li>
                <li class="three">4</li>
                <li>W</li>
                <li>R</li>
                <li>E</li>
                <li>V</li>
                <li>I</li>
                <li>K</li>
                <li>N</li>
                <li>Y</li>
                <li>C</li>
                <li>A</li>
                <li>S</li>
                <li class="four">p</li>
                <li class="five">a</li>
                <li class="six">g</li>
                <li class="seven">e</li>
                <li>O</li>
                <li>I</li>
                <li>F</li>
                <li class="eight">n</li>
                <li class="nine">o</li>
                <li class="ten">t</li>
                <li>J</li>
                <li>G</li>
                <li>K</li>
                <li>T</li>
                <li>Y</li>
                <li>B</li>
                <li>X</li>
                <li>P</li>
                <li>O</li>
                <li>K</li>
                <li>A</li>
                <li>Q</li>
                <li>H</li>
                <li class="eleven">f</li>
                <li class="twelve">o</li>
                <li class="thirteen">u</li>
                <li class="fourteen">n</li>
                <li class="fifteen">d</li>
                <li>J</li>
                <li>U</li>
                <li>Y</li>
                <li>T</li>
                <li>N</li>
                <li>K</li>
                <li>S</li>
                <li>Q</li>
                <li>V</li>
                <li>Y</li>
            </ul>
        </div>

        <!-- === MAIN TEXT CONTENT === -->
        <div id="main-content">
            <h1>Die Seite wurde leider nicht gefunden!</h1>

            <!-- === SEARCH FORM === -->
            <div id="search">
                <form action="{{route('blog.search.redirect')}}" method="POST">
                    {{csrf_field()}}
                    <input type="text" placeholder="Search" name="search"  />
                    <button type="submit" class="input-search">
                        <i class="fa fa-search"></i>
                    </button>
                </form>
            </div>

            <!-- === NAVIGATION BUTTONS === -->
            <div id="navigation">
                <a class="navigation" href="{{route('welcome')}}">Startseite</a>
                <a class="navigation" href="{{route('blog.index')}}">Blog</a>
                @foreach($blogCategories as $cat)
                    <a class="navigation" href="{{route('blog.index',$cat->slug)}}">{{$cat->name}}</a>
                @endforeach
                @guest
                <a class="navigation" href="{{route('login')}}">Login</a>
                @endguest
                <a class="navigation" href="#">Preise</a>

            </div>


        </div>

    </div>
@endsection