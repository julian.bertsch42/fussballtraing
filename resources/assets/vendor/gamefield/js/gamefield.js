var gamefield = function(options){

    var vars = {
        image : 'vendor/gamefield/texture/Spielfeld/spielfeld_natur.png'
    };

    this.construct = function(options){
        $.extend(vars , options);
    };

    //Public
    this.loadGamefield = function(){

        //load image
        $('div.gamefield > img').attr('src',vars.image);

        //set click event
        $('div.gamefield').click(function(){
            $('.object img.ui-resizable').resizable('destroy').parent().draggable('destroy').removeClass('active');
        });


    };

    //Constructor
    this.construct(options);
};
