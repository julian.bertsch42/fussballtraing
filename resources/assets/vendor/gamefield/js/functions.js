/*
Author:Dr.McHack
Description:Set Objects
Date:22.11.2017
*/



function moveObject(type, id, x, y){
  if(type=="player"){
    for(i=0; i<allPlayers.length; i++){
      if(allPlayers[i].id==id){
        allPlayers[i].move(x, y);
        break;
      }
    }

  }else if(type=="pin"){

    for(i=0; i<allPins.length; i++){
      if(allPins[i].id==id){
        allPins[i].move(x, y);
        break;
      }
    }

  }else if(type=="ball"){

    for(i=0; i<allBalls.length; i++){
      if(allBalls[i].id==id){
        allBalls[i].move(x, y);
        break;
      }
    }

  }else{

    for(i=0; i<allOther.length; i++){
      if(allOther[i].id==id){
        allOther[i].move(x, y);
        break;
      }
    }

  }

}


function visibleItems(type){
  document.getElementById("items_player").style.visibility="hidden";
  document.getElementById("items_pins").style.visibility="hidden";
  document.getElementById("items_balls").style.visibility="hidden";
  document.getElementById("items_pfeile").style.visibility="hidden";

  document.getElementById("items_player").style.position="absolute";
  document.getElementById("items_pins").style.position="absolute";
  document.getElementById("items_balls").style.position="absolute";
  document.getElementById("items_pfeile").style.position="absolute";

  document.getElementById(type).style.visibility="visible";
  document.getElementById(type).style.position="";
}




function save(){
  net=new ajax("POST","http://fs.it-solutions-fs.de/spielfeldplaner/save", document.getElementById("main").innerHTML)
  net.request.onreadystatechange=function(){
    if(net.request.status==200){
      console.log(net.request.responseText);
    }
  }
}







function switchbackground(elem){
  if(moveelem.className=='undefined'){
      lastmoveelem.style.backgroundImage=elem.style.backgroundImage;
  }else{
    if(moveelem.className=='player'){
      moveelem.style.backgroundImage=elem.style.backgroundImage;
    }else{
      lastmoveelem.style.backgroundImage=elem.style.backgroundImage;
    }

  }

}

function editheightopeb(){


  document.getElementById("obj-data").style.visibility="visible";

  if(moveelem.className=='undefined'){
    document.getElementById("obj-height").value=lastmoveelem.style.height.split("px")[0];
    document.getElementById("obj-width").value=lastmoveelem.style.width.split("px")[0];
  }else{
    document.getElementById("obj-height").value=moveelem.style.height.split("px")[0];
    document.getElementById("obj-width").value=moveelem.style.width.split("px")[0];
  }

}


function objectDrop(){
  if(document.getElementById("dropdown").style.visibility=="visible"){
    document.getElementById("dropdown").style.visibility="hidden";
    document.getElementById("dropdown").style.position="absolute";
  }else{
    document.getElementById("dropdown").style.visibility="visible";
    document.getElementById("dropdown").style.position="";
  }

}

function delObj(){
  if(moveelem.className=='undefined'){
      lastmoveelem.parentNode.removeChild(lastmoveelem);
  }else{
    moveelem.parentNode.removeChild(moveelem);
  }
}



function saveAll(){

  elem=document.getElementById("main");
  canvas=document.createElement("canvas");
  canvas.style.position=elem.style.position;
  canvas.style.left=elem.style.left;
  canvas.style.top=elem.style.top;
  canvas.style.marginLeft=elem.style.marginLeft;
  canvas.style.marginTop=elem.style.marginTop;
  canvas.style.height=elem.style.height;
  canvas.style.width=elem.style.width;
  canvas.style.height=elem.style.height;
  canvas.id="img";
  elem.parentNode.replaceChild(canvas, document.getElementById("main"));
  canvas.appendChild(elem);


}







function saveData(){
  if(moveelem.className=='undefined'){
      lastmoveelem.style.height=document.getElementById("obj-height").value;
      lastmoveelem.style.width=document.getElementById("obj-width").value;
  }else{
    if(moveelem.className=='player'){
      moveelem.style.height=document.getElementById("obj-height").value;
      moveelem.style.width=document.getElementById("obj-width").value;
    }else{
      lastmoveelem.style.height=document.getElementById("obj-height").value;
      lastmoveelem.style.width=document.getElementById("obj-width").value;
    }

  }
}


function moveObjectDirect(type, id, x, y){
  if(type=="player"){
    for(i=0; i<allPlayers.length; i++){
      if(allPlayers[i].id==id){
        allPlayers[i].movedirect(x, y);
        break;
      }
    }

  }else if(type=="pin"){

    for(i=0; i<allPins.length; i++){
      if(allPins[i].id==id){
        allPins[i].movedirect(x, y);
        break;
      }
    }

  }else if(type=="ball"){

    for(i=0; i<allBalls.length; i++){
      if(allBalls[i].id==id){
        allBalls[i].movedirect(x, y);
        break;
      }
    }

  }else{

    for(i=0; i<allOther.length; i++){
      if(allOther[i].id==id){
        allOther[i].movedirect(x, y);
        break;
      }
    }

  }

}
