var object = function(options){

    var vars = {
        type : 'Spieler',
        image : 'keeper_1_left.png',
        id : ''
    };

    this.construct = function(options){
        $.extend(vars , options);
        newObject();
    };

    //Public

    //Private
    var newObject = function(){

        $('div.objects').append('' +
            '<div class="object object-'+vars.id+'" style="left: 17%; top: 8%;">' +
            '<button class="delete"><span class="glyphicon glyphicon-trash"></span></button>' +
            '<img src="'+vars.image+'" width="64" height="64" />' +
            '</div>' +
            '');
        InitObjectEvents();

    };

    var InitObjectEvents = function(){

        //Object Click Event
        $('.object-'+vars.id+' img').click(function(e){
            e.stopPropagation();

            $('.object img.ui-resizable').resizable('destroy').parent().draggable('destroy').removeClass('active');
            $(this).parent().addClass('active');
            $(this).resizable();

            //Object Drag Event
            $(this).parent().parent().draggable({
                stop: function( event, ui ) {
                    $(this).css("left",parseInt($(this).css("left")) / ($(".objects").width() / 100)+"%");
                    $(this).css("top",parseInt($(this).css("top")) / ($(".objects").height() / 100)+"%");
                }
            });
        });

        $('.object-'+vars.id+' button').click(function(e){
            e.stopPropagation();

            removeObject();
        });

    };

    var removeObject = function(){
        $('.object-'+vars.id).remove();
    };

    //Constructor
    this.construct(options);
};