$(document).ready(function(){

    $("#btnSave").click(function () {
      html2canvas($("div.gamefield"), {
          onrendered: function(canvas) {
              theCanvas = canvas;
              document.body.appendChild(canvas);

              // Convert and download as image
              Canvas2Image.saveAsPNG(canvas);
            }
        }
      
    });

    var id = 1;
    var objects = [];

    //Hide every Objectsite
    $('div.menu div.content ul').not('div.menu div.content ul.spieler').hide();

    //Menu Show-Hide Button
    $('#menu-control').click(function(){

        if($('div.menu').hasClass('expanded')){
            $('div.menu').css('display','none').toggleClass('expanded');
        }
        else {
            $('div.menu').css('display','inherit').toggleClass('expanded');
        }

    });

    //Select Change Event
    $('div.menu div.content select').change(function(){

        $('div.menu div.content ul').hide();
        $('div.menu div.content ul.'+$(this).val()).show();

    });

    /*Save Button
    $("#btnSave").click(function() {
        html2canvas($("div.gamefield"), {
            onrendered: function(canvas) {
                theCanvas = canvas;
                document.body.appendChild(canvas);

                // Convert and download as image
                Canvas2Image.saveAsPNG(canvas);
                $("#img-out").append(canvas).hide();
                // Clean up
                //document.body.removeChild(canvas);
            }
        });
    });*/

    //Gamefield Initiation
    var gamefield1 = new gamefield({});
    gamefield1.loadGamefield();


    //Objectsite's Object Click Event
    $('div.menu div.content ul li img').click(function(){
        var type = $(this).parents('ul').attr('class');
        objects[id] = new object({
            id: id,
            image: $(this).attr('src'),
            type: type
        });
        id++;
    });


});
