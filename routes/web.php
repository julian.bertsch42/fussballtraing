<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/exceltopdf', 'ExcelController@index')->name('exceltopdf');
Route::get('/excel', 'ExcelController@getXlsx')->name('xlsx');

Route::get('/register', function(){return redirect('/');})->name('register');
Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');

// Startpage
Route::get('/', function () { return view('welcome'); })->name('welcome');
Route::get('/impressum', function(){ return view('main.impressum'); })->name('main.impressum');
Route::get('/agb', function(){ return view('main.agb'); })->name('main.agb');
Route::get('/datenschutz', function(){ return view('main.datenschutz'); })->name('main.datenschutz');
Route::get('/faq', function(){ return view('main.faq'); })->name('main.faq');
Route::get('/versand-und-zahlungsbedingungen', function(){ return view('main.versand-und-zahlungsbedingungen'); })->name('main.versand-und-zahlungsbedingungen');
Route::get('/widerrufsrecht', function(){ return view('main.widerrufsrecht'); })->name('main.widerrufsrecht');
Route::get('/zahlungsarten', function(){ return view('main.zahlungsarten'); })->name('main.zahlungsarten');
Route::get('/newsletter-anmelden', function(){ return view('main.newsletter.anmelden'); })->name('main.newsletter.anmelden');
Route::get('/autogrammkarten', function(){ return view('main.autogrammkarten'); })->name('main.autogrammkarten');
Route::get('/ernaehrungsportal', function(){ return view('main.ernaehrungsportal'); })->name('main.ernaehrungsportal');
Route::get('/fussballtraining', function(){ return view('main.fussballtraining'); })->name('main.fussballtraining');
Route::get('/verletzungsportal', function(){ return view('main.verletzungsportal'); })->name('main.verletzungsportal');
Route::get('/preise', function(){ return view('main.preise'); })->name('main.preise');
Route::get('/telefonliste', function(){ return view('main.telefonliste'); })->name('main.telefonliste');
Route::get('/rueckennummern', function(){ return view('main.rueckennummern'); })->name('main.rueckennummern');
Route::get('/strafen-und-mannschaftskasse', function(){ return view('main.strafen-und-mannschaftskasse'); })->name('main.strafen-und-mannschaftskasse');

//Kontakt
Route::get('/kontakt', function(){ return view('main.kontakt.index'); })->name('main.kontakt.index');
Route::post('/kontakt', 'ContactController@send' )->name('main.kontakt.send');

// BLOG
Route::post('/fussball-news/suchen', 'BlogController@searchRedirect')->name('blog.search.redirect');
Route::get('/fussball-news/suchen/{search?}', 'BlogController@search')->name('blog.search');
Route::get('/fussball-news/{category?}/{subcategory?}', 'BlogController@index')->name('blog.index');
Route::get('/fussball-news/{category}/{subcategory}/{slug}-{id}', 'BlogController@show')->name('blog.show')->where('slug', '.*(?=-)');

// TRAININGSÜBUNGEN
Route::post('/trainingsuebungen/suchen', 'WorkoutController@searchRedirect')->name('workout.search.redirect');
Route::get('/trainingsuebungen/suchen/{search?}', 'WorkoutController@search')->name('workout.search');
Route::get('/trainingsuebungen/{category?}/{subcategory?}', 'WorkoutController@index')->name('workout.index');
Route::get('/trainingsuebungen/{category}/{subcategory}/{slug}-{id}', 'WorkoutController@show')->name('workout.show')->where('slug', '.*(?=-)');

// GAMEFIELD-PLANER
Route::get('/spielfeldplaner', 'GamefieldController@index')->name('gamefield');


//Search Engine
Route::get('/vereinsverzeichnis/', 'SearchController@index')->name('search.index');
Route::get('/vereinsverzeichnis/search/{text?}', 'SearchController@search')->name('search.search');
Route::get('/vereinsverzeichnis/verein/{club}', 'ClubController@index')->name('club.show');
Route::get('/vereinsverzeichnis/verband/{association}', 'AssociationController@index')->name('association.show');

//Submissions
Route::get('/vereinsverzeichnis/submissions/create', 'SubmissionController@create')->name('submissions.create');
Route::get('/vereinsverzeichnis/submissions/{club}/edit', 'SubmissionController@edit')->name('submissions.edit');
Route::post('/vereinsverzeichnis/submissions/{club}', 'SubmissionController@update')->name('submissions.update');
Route::post('/vereinsverzeichnis/submissions', 'SubmissionController@store')->name('submissions.store');


// AUTH
Route::group(['middleware' => 'auth'],function() {

    // BACKEND
    Route::group(['middleware' => 'isAdmin', 'namespace' => 'Backend', 'prefix' => 'backend', 'as' => 'backend.'], function () {

        // DASHBOARD
        Route::get('/', function () {
            return redirect()->route('backend.dashboard.index');
        })->name('home');

        Route::resource('/dashboard', 'DashboardController');

        // ADMINISTRATION
        Route::group(['namespace' => 'Admin', 'as' => 'admin.', 'prefix' => 'admin'], function () {
            Route::group(['namespace' => 'Settings', 'as' => 'settings.', 'prefix' => 'einstellungen'], function () {
                Route::group(['namespace' => 'System', 'as' => 'system.', 'prefix' => 'system'], function () {
                    Route::get('/newsticker', 'NewsTickerController@edit')->name('newsticker.edit');
                    Route::post('/newsticker', 'NewsTickerController@update')->name('newsticker.update');
                    Route::get('blog-hintergrund/{setting}', 'SettingController@edit')->name('blog-background.edit');
                    Route::put('blog-hintergrund/{setting}', 'SettingController@update')->name('blog-background.update');
                });

                Route::group(['namespace' => 'Functions', 'as' => 'functions.', 'prefix' => 'funktionen'], function () {
                    Route::group(['namespace' => 'Blog', 'as' => 'blog.', 'prefix' => 'blog'], function () {
                        Route::resource('/categories', 'CategoryController');
                        Route::resource('/subcategories', 'SubCategoryController');
                    });
                    Route::group(['namespace' => 'Workout', 'as' => 'workout.', 'prefix' => 'trainingsuebungen'], function () {
                        Route::resource('/categories', 'CategoryController');
                        Route::resource('/subcategories', 'SubCategoryController');
                    });
                });
            });


            Route::resource('/submissions', 'SubmissionController');
            Route::resource('/clubs', 'ClubController');
            Route::resource('/associations', 'AssociationController');


            Route::resource('/benutzer', 'UserController',['parameters' => ['benutzer' => 'user'], 'names' => 'users']);
        });

        // BLOG
        Route::group(['as' => 'blog.', 'prefix' => 'blog'], function () {
            Route::resource('/posts', 'BlogController',['except' => ['show','index','create']]);
            Route::get('/posts/{cat?}', 'BlogController@index')->name('posts.index');
            Route::get('/posts/create/{cat?}', 'BlogController@create')->name('posts.create');
        });

        // TRAININGSÜBUNGEN
        Route::group(['as' => 'workout.', 'prefix' => 'trainingsuebungen'], function () {
            Route::resource('/posts', 'WorkoutController',['except' => ['show','index','create']]);
            Route::get('/posts/{cat?}', 'WorkoutController@index')->name('posts.index');
            Route::get('/posts/create/{cat?}', 'WorkoutController@create')->name('posts.create');
        });




    });

});

