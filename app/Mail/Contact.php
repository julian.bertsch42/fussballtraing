<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    public $subject, $name, $content, $address;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->content = $request->input('message');
        $this->subject = 'Kontaktformular | '.env('APP_NAME');
        $this->address = $request->input('email');
        $this->name = $request->input('name');

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = env('APP_EMAIL');
        $name = env('APP_NAME');

        return $this->view('email.send')
            ->from($this->address, $this->name)
            ->replyTo($this->address, $this->name)
            ->to($address, $name)
            ->subject($this->subject);
    }
}
