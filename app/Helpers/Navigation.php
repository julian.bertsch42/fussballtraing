<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;

class Navigation{

    public static function contains_array ($string, $array = array ())
    {
        foreach ($array as $key => $value)
            if (strpos($string, $value) !== false)
                return true;

        return false;
    }

    public static function parametersToArray($array){
        $array = array_values($array);

        foreach($array as $k => $v)
            if($v instanceof Model || $v instanceof Collection)
                $array[$k] = $v->getKey();

        return $array;
    }

    public static function isActiveRoute($routes, $relative = false){

        $func = $relative ? 'Navigation::contains_array' : 'in_array';

        if (array_keys($routes) !== range(0, count($routes) - 1)){
            if ($func(Route::currentRouteName(), array_keys($routes)))
                if(!$relative && $routes[Route::currentRouteName()] === self::parametersToArray(Route::getCurrentRoute()->parameters())) return 'active';
                else if($relative && !array_diff($routes[Route::currentRouteName()], self::parametersToArray(Route::getCurrentRoute()->parameters()))) return 'active';

        }
        else if ($func(Route::currentRouteName(), $routes) && ( empty(Route::getCurrentRoute()->parameters()) || $relative )) return 'active';

        return '';

    }

}