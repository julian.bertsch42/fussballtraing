<?php

namespace App\Http\Controllers;

use App\Models\Association;
use App\Models\Club;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    //
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('search.index');
    }


    /**
     * @param Request $request
     * @param null $text
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function search(Request $request, $text = null){

        if(!empty($request->input('search')))
            return redirect()->route('search.search',$request->input('search'));

        if(empty($text) || empty(str_after($text,'::')))
            return redirect()->route('search.index');


        // SEARCH TYPES

        switch(str_before($text,'::')){
            case 'begins':
                $clubs = Club::Where('title','like',str_after($text,'::').'%');
                    break;
            default:
                $clubs = Club::Where('title','like','%'.$text.'%');
        }

        $clubs = $clubs->orderByRaw('title')->paginate(15);


        // SET TYPE (CLUB)

        foreach($clubs as $clubid => $club)
            $clubs[$clubid]['type'] = 1;


        return view('search.results',['clubs' => $clubs, 'text' => $text]);
    }
}
