<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Models\Association;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AssociationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->input('search') !== null )
            $associations = Association::Where('title','like','%'.$request->input('search').'%')->orderBy('title');
        else
            $associations = Association::orderBy('title');

        $associations = $associations->paginate('15');

        return view('backend.admin.associations.index',['associations' => $associations]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $associations = Association::all();

        return view('backend.admin.associations.create',['associations' => $associations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'parent_id' => 'nullable|integer|exists:associations,id'
        ]);

        $association = new Association();

        $association->title = $request->input('title');
        $association->parent_id = $request->input('parent_id');

        $association->save();

        return redirect()->route('backend.admin.associations.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Association  $association
     * @return \Illuminate\Http\Response
     */
    public function show(Association $association)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Association  $association
     * @return \Illuminate\Http\Response
     */
    public function edit(Association $association)
    {
        $associations = Association::all();

        return view('backend.admin.associations.edit',['association' => $association, 'associations' => $associations]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Association  $association
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Association $association)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'parent_id' => 'nullable|integer|exists:associations,id'
        ]);

        $association->title = $request->input('title');
        $association->parent_id = $request->input('parent_id');

        $association->save();

        return redirect()->route('backend.admin.associations.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Association  $association
     * @return \Illuminate\Http\Response
     */
    public function destroy(Association $association)
    {
        $association->delete();
        return redirect()->route('backend.admin.associations.index');
    }
}
