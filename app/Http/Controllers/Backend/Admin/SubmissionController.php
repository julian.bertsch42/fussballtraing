<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Models\Association;
use App\Models\Club;
use App\Models\Submission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $submissions = Submission::all();

        return view('backend.admin.submissions.index',['submissions' => $submissions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Submission  $submission
     * @return \Illuminate\Http\Response
     */
    public function show(Submission $submission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Submission  $submission
     * @return \Illuminate\Http\Response
     */
    public function edit(Submission $submission)
    {
        $club = !empty($submission->club_id) ? Club::Where('id',$submission->club_id)->first() : Club::Where('title','like','%'.$submission->title.'%')->first();
        $associations = Association::all();
        $association = Association::Where('title','like','%'.$submission->association.'%')->first();

        return view('backend.admin.submissions.edit',['submission' => $submission,'club' => $club,'associations' => $associations, 'associationSelected' => $association]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Submission  $submission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Submission $submission)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'street' => 'nullable|string|max:255',
            'zip' => 'nullable|string|max:6',
            'city' => 'nullable|string|max:255',
            'phone' => 'nullable|string|max:15',
            'fax' => 'nullable|string|max:15',
            'email' => 'nullable|string|max:100',
            'uri' => 'nullable|string|max:255',
            'stadium' => 'nullable|string|max:255',
            'association_id' => 'nullable|integer|exists:associations,id',
            'facebook' => 'nullable|string|max:255',
            'twitter' => 'nullable|string|max:255',
            'youtube' => 'nullable|string|max:255',
            'instagram' => 'nullable|string|max:255',
            'description' => 'nullable|string',
        ]);

        if($request->input('action') == "approve" || $request->input('action') == "new" && $submission->status !== 1){

            if($request->input('action') == "new" && empty($submission->club_id))
                $club = new Club();
            else{
                $club = !empty($submission->club_id) ? Club::Where('id',$submission->club_id)->first() : Club::Where('title','like','%'.$submission->title.'%')->first();
                $club = $club ?? new Club();
            }



            $club->title = $request->input('title');
            $club->street = $request->input('street');
            $club->zip = $request->input('zip');
            $club->city = $request->input('city');
            $club->phone = $request->input('phone');
            $club->fax = $request->input('fax');
            $club->email = $request->input('email');
            $club->uri = $request->input('uri');
            $club->stadium = $request->input('stadium');
            $club->association_id = $request->input('association_id');
            $club->facebook = $request->input('facebook');
            $club->twitter = $request->input('twitter');
            $club->youtube = $request->input('youtube');
            $club->instagram = $request->input('instagram');
            $club->description = $request->input('description');

            $club->save();

            $submission->status = 1;

        }
        elseif($submission->status !== 2)
            $submission->status = 2;

        $submission->save();

        return redirect()->route('backend.admin.submissions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Submission  $submission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Submission $submission)
    {
        //
    }
}
