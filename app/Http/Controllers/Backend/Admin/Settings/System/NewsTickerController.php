<?php

namespace App\Http\Controllers\Backend\Admin\Settings\System;

use App\Models\Newsticker;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsTickerController extends Controller
{
    public function edit(){
        $newsticker = Newsticker::all();
        return view('backend.admin.settings.system.newsticker.edit', ['newsticker' => $newsticker]);
    }

    public function update(Request $request){

        $request->validate([
            'body1' => 'nullable|string',
            'body2' => 'nullable|string',
            'body3' => 'nullable|string'
        ]);

        //SAVE ALL Newsticker (1,2,3)
        $newsticker1 = Newsticker::where('id',1)->first();
        $newsticker2 = Newsticker::where('id',2)->first();
        $newsticker3 = Newsticker::where('id',3)->first();

        $newsticker1->body = $request->input('body1');
        $newsticker2->body = $request->input('body2');
        $newsticker3->body = $request->input('body3');

        $newsticker1->save();
        $newsticker2->save();
        $newsticker3->save();

        return redirect()->route('backend.dashboard.index');

    }
}
