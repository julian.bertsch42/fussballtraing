<?php

namespace App\Http\Controllers\Backend\Admin\Settings\System;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{

    public function edit(Setting $setting){

        return view('backend.admin.settings.system.'.$setting->name, ['setting' => $setting]);

    }

    public function update(Request $request, Setting $setting){
        //CALL THE METHOD WITH IDENTICAL SETTING NAME
        return $this->{camel_case(str_replace('.','',$setting->name))}($request, $setting);
    }

    public function background($request, $setting){

        $request->validate([
            'value' => 'image|mimes:jpeg,png,jpg,gif,svg,bmp|max:2048',
        ]);

        //UPLOAD IMAGE
        if($request->hasFile('value')){
            $image = $request->file('value');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/upload/system/images/');
            $image = $image->move($destinationPath, $input['imagename']);
        }

        //SAVE
        if(isset($image))
            $setting->value = "upload/system/images/".$image->getFilename();

        $setting->save();
        $setting->updateCache();

        return redirect()->route('backend.dashboard.index');

    }
}
