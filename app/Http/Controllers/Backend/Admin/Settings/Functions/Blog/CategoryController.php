<?php

namespace App\Http\Controllers\Backend\Admin\Settings\Functions\Blog;

use App\Models\Blog\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.admin.settings.functions.blog.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $headerimages = scandir('vendor/blog/img');
        return view('backend.admin.settings.functions.blog.categories.create',['headerimages' => $headerimages]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //VALIDATE
        $request->validate([
            'name' => 'required|string',
            'ordering' => 'nullable|integer',
            'seotitle' => 'nullable|string',
            'seodescription' => 'nullable|string',
            'color' => 'required|integer|min:1|max:5',
            'headerimage' => 'nullable|string'
        ]);

        //SAVE
        $cat = new Category();

        $cat->name = $request->input('name');
        $cat->ordering = $request->input('ordering');
        $cat->seotitle = $request->input('seotitle');
        $cat->seodescription = $request->input('seodescription');
        $cat->color = $request->input('color');
        $cat->headerimage = $request->input('headerimage');
        $cat->save();
        $cat->updateCache();

        return redirect()->route('backend.admin.settings.functions.blog.categories.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blog\Category  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $headerimages = scandir('vendor/blog/img');
        return view('backend.admin.settings.functions.blog.categories.edit',['cat' => $category, 'headerimages' => $headerimages]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blog\Category  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //VALIDATE
        $request->validate([
            'name' => 'required|string',
            'ordering' => 'nullable|integer',
            'seotitle' => 'nullable|string',
            'seodescription' => 'nullable|string',
            'color' => 'required|integer|min:1|max:5',
            'headerimage' => 'nullable|string'
        ]);

        //SAVE
        $category->name = $request->input('name');
        $category->ordering = $request->input('ordering');
        $category->seotitle = $request->input('seotitle');
        $category->seodescription = $request->input('seodescription');
        $category->color = $request->input('color');
        $category->headerimage = $request->input('headerimage');
        $category->save();
        $category->updateCache();

        return redirect()->route('backend.admin.settings.functions.blog.categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blog\Category  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        $category->updateCache();

        return redirect()->route('backend.admin.settings.functions.blog.categories.index');
    }
}
