<?php

namespace App\Http\Controllers\Backend\Admin\Settings\Functions\Workout;

use App\Models\Workout\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.admin.settings.functions.workout.subcategories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $headerimages = scandir('vendor/blog/img');
        return view('backend.admin.settings.functions.workout.subcategories.create',['headerimages' => $headerimages]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //VALIDATE
        $request->validate([
            'name' => 'required|string',
            'ordering' => 'nullable|integer',
            'seotitle' => 'nullable|string',
            'seodescription' => 'nullable|string',
            'color' => 'required|integer|min:1|max:5',
            'parent' => 'nullable|exists:workout__categories,id',
            'headerimage' => 'nullable|string'
        ]);

        //SAVE
        $cat = new SubCategory();

        $cat->name = $request->input('name');
        $cat->ordering = $request->input('ordering');
        $cat->seotitle = $request->input('seotitle');
        $cat->seodescription = $request->input('seodescription');
        $cat->color = $request->input('color');
        $cat->category_id = $request->input('parent');
        $cat->headerimage = $request->input('headerimage');
        $cat->save();
        $cat->updateCache();

        return redirect()->route('backend.admin.settings.functions.workout.subcategories.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blog\SubCategory  $blogSubCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(SubCategory $subcategory)
    {
        $headerimages = scandir('vendor/blog/img');
        return view('backend.admin.settings.functions.workout.subcategories.edit',['cat' => $subcategory, 'headerimages' => $headerimages]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blog\SubCategory  $blogSubCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubCategory $subcategory)
    {
        //VALIDATE
        $request->validate([
            'name' => 'required|string',
            'ordering' => 'nullable|integer',
            'seotitle' => 'nullable|string',
            'seodescription' => 'nullable|string',
            'color' => 'required|integer|min:1|max:5',
            'parent' => 'nullable|exists:workout__categories,id',
            'headerimage' => 'nullable|string'
        ]);

        //SAVE
        $subcategory->name = $request->input('name');
        $subcategory->ordering = $request->input('ordering');
        $subcategory->seotitle = $request->input('seotitle');
        $subcategory->seodescription = $request->input('seodescription');
        $subcategory->color = $request->input('color');
        $subcategory->category_id = $request->input('parent');
        $subcategory->headerimage = $request->input('headerimage');
        $subcategory->save();
        $subcategory->updateCache();

        return redirect()->route('backend.admin.settings.functions.workout.subcategories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blog\SubCategory  $blogSubCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCategory $subcategory)
    {
        $subcategory->delete();
        $subcategory->updateCache();

        return redirect()->route('backend.admin.settings.functions.workout.subcategories.index');
    }
}
