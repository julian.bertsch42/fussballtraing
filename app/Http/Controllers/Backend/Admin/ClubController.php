<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Models\Association;
use App\Models\Club;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->input('search') !== null )
            $clubs = Club::Where('title','like','%'.$request->input('search').'%')->orderBy('title');
        else
            $clubs = Club::orderBy('title');

        $clubs = $clubs->paginate('15');

        return view('backend.admin.clubs.index',['clubs' => $clubs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $associations = Association::all();

        return view('backend.admin.clubs.create',['associations' => $associations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'street' => 'nullable|string|max:255',
            'zip' => 'nullable|string|max:6',
            'city' => 'nullable|string|max:255',
            'phone' => 'nullable|string|max:15',
            'fax' => 'nullable|string|max:15',
            'email' => 'nullable|string|max:100',
            'uri' => 'nullable|string|max:255',
            'stadium' => 'nullable|string|max:255',
            'association_id' => 'nullable|integer|exists:associations,id',
            'facebook' => 'nullable|string|max:255',
            'twitter' => 'nullable|string|max:255',
            'youtube' => 'nullable|string|max:255',
            'instagram' => 'nullable|string|max:255',
            'description' => 'nullable|string',
        ]);

        $club = new Club();

        $club->title = $request->input('title');
        $club->street = $request->input('street');
        $club->zip = $request->input('zip');
        $club->city = $request->input('city');
        $club->phone = $request->input('phone');
        $club->fax = $request->input('fax');
        $club->email = $request->input('email');
        $club->uri = $request->input('uri');
        $club->stadium = $request->input('stadium');
        $club->association_id = $request->input('association_id');
        $club->facebook = $request->input('facebook');
        $club->twitter = $request->input('twitter');
        $club->youtube = $request->input('youtube');
        $club->instagram = $request->input('instagram');
        $club->description = $request->input('description');

        $club->save();

        return redirect()->route('backend.admin.clubs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Club  $club
     * @return \Illuminate\Http\Response
     */
    public function show(Club $club)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Club  $club
     * @return \Illuminate\Http\Response
     */
    public function edit(Club $club)
    {
        $associations = Association::all();

        return view('backend.admin.clubs.edit',['club' => $club, 'associations' => $associations]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Club  $club
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Club $club)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'street' => 'nullable|string|max:255',
            'zip' => 'nullable|string|max:6',
            'city' => 'nullable|string|max:255',
            'phone' => 'nullable|string|max:15',
            'fax' => 'nullable|string|max:15',
            'email' => 'nullable|string|max:100',
            'uri' => 'nullable|string|max:255',
            'stadium' => 'nullable|string|max:255',
            'association_id' => 'nullable|integer|exists:associations,id',
            'facebook' => 'nullable|string|max:255',
            'twitter' => 'nullable|string|max:255',
            'youtube' => 'nullable|string|max:255',
            'instagram' => 'nullable|string|max:255',
            'description' => 'nullable|string',
        ]);

        $club->title = $request->input('title');
        $club->street = $request->input('street');
        $club->zip = $request->input('zip');
        $club->city = $request->input('city');
        $club->phone = $request->input('phone');
        $club->fax = $request->input('fax');
        $club->email = $request->input('email');
        $club->uri = $request->input('uri');
        $club->stadium = $request->input('stadium');
        $club->association_id = $request->input('association_id');
        $club->facebook = $request->input('facebook');
        $club->twitter = $request->input('twitter');
        $club->youtube = $request->input('youtube');
        $club->instagram = $request->input('instagram');
        $club->description = $request->input('description');

        $club->save();

        return redirect()->route('backend.admin.clubs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Club  $club
     * @return \Illuminate\Http\Response
     */
    public function destroy(Club $club)
    {
        $club->delete();
        return redirect()->route('backend.admin.clubs.index');
    }
}
