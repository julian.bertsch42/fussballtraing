<?php

namespace App\Http\Controllers\Backend;

use App\Models\Blog\Post;
use App\Models\Blog\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SubCategory $cat = null)
    {
        $posts = !empty($cat) ? Post::where('subcategory_id', $cat->id)->paginate(25) : Post::paginate(25);

        return view('backend.blog.posts.index',['posts' => $posts, 'cat' => $cat]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(SubCategory $cat = null)
    {
        return view('backend.blog.posts.create',['category' => $cat]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //VALIDATE
        $request->validate([
           'title' => 'required|string|max:77',
           'tags' => 'nullable|string|max:191',
           'seo-title' => 'nullable|string|max:77',
           'body' => 'required|string',
           'category' => 'required|integer|exists:blog__subcategories,id',
           'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg,bmp|max:2048',
        ]);

        // SAVE POST
        $post = new Post();

        $post->title = $request->input('title');
        $post->{"seo-title"} = $request->input('seo-title');
        $post->body = $request->input('body');
        $post->user_id = $request->user()->id;
        $post->subcategory_id = $request->input('category');
        $post->tags = explode(',',str_replace(' ','',$request->input('tags')));

        if(!empty($image = $post->uploadImage($request)))
            $post->image = "upload/images/".$image;

        $post->save();
        $post->updateCache();

        return redirect()->route('backend.blog.posts.index',['cat' => $post->blog_category_id]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blog\Post  $blog_Post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('backend.blog.posts.edit',['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blog\Post  $blog_Post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $request->validate([
            'title' => 'required|string|max:77',
            'tags' => 'nullable|string|max:191',
            'seo-title' => 'nullable|string|max:77',
            'body' => 'required|string',
            'category' => 'required|integer|exists:blog__subcategories,id',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg,bmp|max:2048',
        ]);

        //SAVE POST
        $post->title = $request->input('title');
        $post->{"seo-title"} = $request->input('seo-title');
        $post->body = $request->input('body');
        $post->subcategory_id = $request->input('category');
        $post->tags = !empty($request->input('tags')) ? explode(',',str_replace(' ','',$request->input('tags'))) : [];

        if(!empty($image = $post->uploadImage($request)))
            $post->image = "upload/images/".$image;

        if($request->user()->isAdmin())
            if($request->input('active') != null)
                $post->active = $request->input('active');

        $post->save();
        $post->updateCache();

        return redirect()->route('backend.blog.posts.index',['cat' => $request->input('category')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blog\Post  $blog_Post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $cat = $post->subCategory;
        @unlink($post->image);

        $post->delete();
        $post->updateCache();

        return redirect()->route('backend.blog.posts.index',['category' => $cat->id]);
    }
}
