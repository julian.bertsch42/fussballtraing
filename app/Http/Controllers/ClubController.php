<?php

namespace App\Http\Controllers;

use App\Models\Association;
use App\Models\Club;
use Html2Text\Html2Text;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClubController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($club)
    {
        $clubObj = Club::where(DB::raw('REPLACE(REPLACE(title, " ", "-"),"/","-")'),$club)->first();

        if(empty($clubObj))
            $clubObj = Club::where('id',$club)->first();


        $seodescription = $clubObj->description;

        $association = Association::Where('id',$clubObj->association_id)->first();

        return view('club.show',['club' => $clubObj, 'association' => $association, 'seodescription' => $seodescription]);
    }
}
