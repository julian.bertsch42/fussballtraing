<?php

namespace App\Http\Controllers;

use App\Models\Association;
use App\Models\Club;
use App\Models\Submission;
use Illuminate\Http\Request;

class SubmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('submissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'street' => 'nullable|string|max:255',
            'zip' => 'nullable|string|max:6',
            'city' => 'nullable|string|max:255',
            'phone' => 'nullable|string|max:15',
            'fax' => 'nullable|string|max:15',
            'email' => 'nullable|string|max:100',
            'uri' => 'nullable|string|max:255',
            'stadium' => 'nullable|string|max:255',
            'association' => 'nullable|string|max:255',
            'facebook' => 'nullable|string|max:255',
            'twitter' => 'nullable|string|max:255',
            'youtube' => 'nullable|string|max:255',
            'instagram' => 'nullable|string|max:255',
            'description' => 'nullable|string',
            'author' => 'nullable|string',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        $submission = new Submission();
        $submission->title = $request->input('title');
        $submission->street = $request->input('street');
        $submission->zip = $request->input('zip');
        $submission->city = $request->input('city');
        $submission->phone = $request->input('phone');
        $submission->fax = $request->input('fax');
        $submission->email = $request->input('email');
        $submission->uri = $request->input('uri');
        $submission->stadium = $request->input('stadium');
        $submission->association = $request->input('association');
        $submission->facebook = $request->input('facebook');
        $submission->twitter = $request->input('twitter');
        $submission->youtube = $request->input('youtube');
        $submission->instagram = $request->input('instagram');
        $submission->description = $request->input('description');
        $submission->author = $request->input('author');


        $submission->save();

        return redirect()->route('search.index')->with('message','submissionSuccess');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Submission  $submission
     * @return \Illuminate\Http\Response
     */
    public function show(Submission $submission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Submission  $submission
     * @return \Illuminate\Http\Response
     */
    public function edit(Club $club)
    {
        return view('submissions.edit',['club' => $club]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Submission  $submission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Club $club)
    {
        $request->validate([
           'title' => 'required|string|max:255',
           'street' => 'nullable|string|max:255',
           'zip' => 'nullable|string|max:6',
           'city' => 'nullable|string|max:255',
           'phone' => 'nullable|string|max:15',
           'fax' => 'nullable|string|max:15',
           'email' => 'nullable|string|max:100',
           'uri' => 'nullable|string|max:255',
           'stadium' => 'nullable|string|max:255',
           'association' => 'nullable|string|max:255',
           'facebook' => 'nullable|string|max:255',
           'twitter' => 'nullable|string|max:255',
           'youtube' => 'nullable|string|max:255',
           'instagram' => 'nullable|string|max:255',
           'description' => 'nullable|string',
           'author' => 'nullable|string',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        $submission = new Submission();
        $submission->title = $request->input('title');
        $submission->street = $request->input('street');
        $submission->zip = $request->input('zip');
        $submission->city = $request->input('city');
        $submission->phone = $request->input('phone');
        $submission->fax = $request->input('fax');
        $submission->email = $request->input('email');
        $submission->uri = $request->input('uri');
        $submission->stadium = $request->input('stadium');
        $submission->association = $request->input('association');
        $submission->facebook = $request->input('facebook');
        $submission->twitter = $request->input('twitter');
        $submission->youtube = $request->input('youtube');
        $submission->instagram = $request->input('instagram');
        $submission->description = $request->input('description');
        $submission->author = $request->input('author');

        $submission->club_id = $club->id;

        $submission->save();

        return redirect()->route('club.show',$club->id)->with('message','submissionSuccess');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Submission  $submission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Submission $submission)
    {
        //
    }
}
