<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function send(Request $request){

        $request->validate([

            'email' => 'required|email'

        ]);


        Mail::send(new Contact($request));

        if (Mail::failures())
            $status = [1,"Die Nachricht konnte nicht versendet werden, bitte versuchen Sie es später noch einmal oder schreiben Sie uns eine Email an: support@fussballtrainer-software.de"];
        else
            $status = [2,"Die Nachricht wurde erfolgreich versendet!"];

        return view('main.kontakt.index',['status' => $status]);

    }
}
