<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GamefieldController extends Controller
{
    public function index(){

        $spieler = is_dir('vendor/gamefield/texture/Spieler') ? scandir('vendor/gamefield/texture/Spieler') : [];
        unset($spieler[0], $spieler[1]);

        $baelle = is_dir('vendor/gamefield/texture/Baelle') ? scandir('vendor/gamefield/texture/Baelle') : [];
        unset($baelle[0], $baelle[1]);

        $markierungen = is_dir('vendor/gamefield/texture/Markierungen') ? scandir('vendor/gamefield/texture/Markierungen') : [];
        unset($markierungen[0], $markierungen[1]);

        $pfeile = is_dir('vendor/gamefield/texture/Pfeile') ? scandir('vendor/gamefield/texture/Pfeile') : [];
        unset($pfeile[0], $pfeile[1]);

        $material = is_dir('vendor/gamefield/texture/Material')? scandir('vendor/gamefield/texture/Material') : [];
        unset($material[0], $material[1]);

        return view('gamefield',[
            'spieler' => $spieler,
            'baelle' => $baelle,
            'markierungen' => $markierungen,
            'pfeile' => $pfeile,
            'material' => $material
        ]);
    }
}
