<?php

namespace App\Http\Controllers;

use App\Models\Association;
use App\Models\Club;
use Illuminate\Http\Request;

class AssociationController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Association $association)
    {
        $clubs = Club::Where('association_id',$association->id)->get();

        return view('association.show',['association' => $association, 'clubs' => $clubs]);
    }
}
