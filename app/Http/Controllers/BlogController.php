<?php

namespace App\Http\Controllers;

use App\Models\Blog\Category;
use App\Models\Blog\Post;
use App\Models\Blog\SubCategory;
use App\Models\Newsticker;
use Illuminate\Http\Request;

class BlogController extends Controller
{

    public function index($category = null, $subcategory = null){

        $category = Category::where('slug', $category)->first();
        $subcategory = SubCategory::where('slug', $subcategory)->first();

        //GET DATA
        if(!empty($subcategory)){
            $last5 = $subcategory->posts()->where('active', 1)->orderBy('created_at', 'desc')->take(5)->get();
            $posts = $subcategory->posts()->where('active', 1)->orderBy('created_at', 'desc')->paginate(8);
        }
        elseif(!empty($category)){
            $last5 = $category->posts()->where('active', 1)->orderBy('created_at', 'desc')->take(5)->get();
            $posts = $category->posts()->where('active', 1)->orderBy('created_at', 'desc')->paginate(8);
        }
        else {
            $last5 = Post::where('active', 1)->orderBy('created_at', 'desc')->take(5)->get();
            $posts = Post::where('active', 1)->orderBy('created_at', 'desc')->paginate(8);
        }

        $newsticker = Newsticker::all();

        return view('blog.index',[
            'last5' => $last5,
            'posts' => $posts,
            'newsTicker' => $newsticker,
            'category' => $category,
            'subCategory' => $subcategory,
        ]);
    }

    public function show($category, $subcategory, $slug, $id){

        //GET DATA
        $newsticker = Newsticker::all();
        $post = Post::where('slug',$slug)->where('active',1)->first() ?? Post::where('id',$id)->where('active',1)->firstOrFail();

        //CHECK CATEGORY SLUG AND POST SLUG AND REDIRECT IF NOT CORRECT WITH 301 ERROR
        if([$category, $subcategory, $slug, $id] != [$post->category->slug, $post->subCategory->slug, $post->slug, $post->id])
            return redirect($post->link(),301);

        return view('blog.show',[
            'post' => $post,
            'newsTicker' => $newsticker,
            'category' => $post->category,
            'subCategory' => $post->subCategory
        ]);

    }

    public function searchRedirect(Request $request){
        return redirect()->route('blog.search',$request->input('search'));
    }

    public function search(Request $request, $search = null){

        //GET DATA
        $last5 = Post::where('active',1)->where('title','LIKE','%'.$search.'%')->orderBy('created_at','desc')->take(5)->get();
        $posts = Post::where('active',1)->where('title','LIKE','%'.$search.'%')->paginate(8);
        $newsticker = Newsticker::all();

        return view('blog.index',[
            'last5' => $last5,
            'posts' => $posts,
            'search' => $search,
            'newsTicker' => $newsticker,
        ]);
    }
}
