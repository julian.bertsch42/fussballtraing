<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ExcelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('exceltopdf');
    }

    /**
     * Get Excel File
     *
     * @return \Illuminate\Http\Response
     */
    public function getXlsx()
    {
        $url = storage_path('app/public/Trainingsbeteiligung.xlsx');

        return response()->download($url, 'Trainingsbeteiligung.xlsx');
    }
}
