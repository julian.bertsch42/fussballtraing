<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Association
 *
 * @mixin \Eloquent
 */
class Association extends Model
{
    public function parent(){
        return (new Association())->Where('id',$this->parent_id)->first();
    }
}
