<?php

namespace App\Models\Blog;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Category extends Model
{
    use Sluggable;

    protected $table = "blog__categories";

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function childs(){
        return $this->hasMany('App\Models\Blog\SubCategory','category_id','id');
    }

    public function link(){
        return route('blog.index',[$this->slug]);
    }

    public function posts(){
        return $this->hasManyThrough('App\Models\Blog\Post','App\Models\Blog\SubCategory','category_id','subcategory_id');
    }

    public function activePosts(){
        return $this->posts()->where('active',1);
    }

    public function unactivePosts(){
        return $this->posts()->where('active',0);
    }

    public function updateCache(){
        Cache::forget('blog.categories');
    }

}
