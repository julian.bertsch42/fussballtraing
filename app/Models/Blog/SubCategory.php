<?php

namespace App\Models\Blog;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class SubCategory extends Model
{
    use Sluggable;

    protected $table = "blog__subcategories";

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function parent(){
        return $this->hasOne('App\Models\Blog\Category','id','category_id');
    }

    public function link(){
        return route('blog.index',[$this->parent->slug,$this->slug]);
    }

    public function posts(){
        return $this->hasMany('App\Models\Blog\Post', 'subcategory_id');
    }

    public function activePosts(){
        return $this->posts()->where('active',1);
    }

    public function unactivePosts(){
        return $this->posts()->where('active',0);
    }

    public function updateCache(){
        Cache::forget('blog.subCategories');
    }

}
