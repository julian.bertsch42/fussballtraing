<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\Cache;

class Post extends Model
{
    use Sluggable;

    protected $table = "blog__posts";

    public $with = [
       'user', 'subCategory.parent'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected $casts = [
        'tags' => 'array'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function subCategory(){
        return $this->belongsTo('App\Models\Blog\SubCategory', 'subcategory_id');
    }

    public function category(){
        return $this->subCategory->parent();
    }

    public function tagsAsString(){
        return @implode(', ',$this->tags);
    }

    public function bodyText(){
        $text = new \Html2Text\Html2Text($this->body);
        return $text = $text->getText();
    }

    public function link(){
        return route('blog.show',[$this->category->slug,$this->subCategory->slug,$this->slug,$this->id]);
    }

    public function uploadImage($request){

        if(!empty($data = $request->input('imageObj'))){
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);

            file_put_contents(public_path('/upload/vendor/blog/posts/images/'.str_slug($request->input('title'),'-','de').'.png'), $data);
            $image = str_slug($request->input('title'),'-','de').'.png';
        }
        elseif($request->hasFile('image')){
            $image = $request->file('image');
            $input['imagename'] = str_slug($request->input('title'),'-','de').'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/upload/vendor/blog/posts/images');
            $image = $image->move($destinationPath, $input['imagename']);
            $image = $image->getFilename();
        }
        return $image ?? null;

    }

    public function updateCache(){
        Cache::forget('blog.categories');
        Cache::forget('blog.subCategories');
    }
}
