<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Club
 *
 * @mixin \Eloquent
 */
class Club extends Model
{
    public function association(){
        return $this->belongsTo('App\Models\Association');
    }
}
