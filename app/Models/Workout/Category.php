<?php

namespace App\Models\Workout;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Category extends Model
{
    use Sluggable;

    protected $table = "workout__categories";

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function childs(){
        return $this->hasMany('App\Models\Workout\SubCategory','category_id','id');
    }

    public function link(){
        return route('workout.index',[$this->slug]);
    }

    public function posts(){
        return $this->hasManyThrough('App\Models\Workout\Post','App\Models\Workout\SubCategory','category_id','subcategory_id');
    }

    public function activePosts(){
        return $this->posts()->where('active',1);
    }

    public function unactivePosts(){
        return $this->posts()->where('active',0);
    }

    public function updateCache(){
        Cache::forget('workout.categories');
    }
}
