<?php

namespace App\Providers;

use App\Models\Blog\Category;
use App\Models\Blog\Post;
use App\Models\Blog\SubCategory;
use App\Models\Setting;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      
        Schema::defaultStringLength(191);

        //LOAD DATA FROM CACHE AND CREATE CACHE

        //SHARE VARIABLE TO VIEWS
        View::share('blogCategories', Cache::rememberForever('blog.categories', function() {
            return Category::orderBy('ordering')->with(['childs' => function($q){
                $q->withCount('unactivePosts');
            }])->withCount('activePosts')->withCount('unactivePosts')->get();
        }));
        View::share('blogSubCategories', Cache::rememberForever('blog.subCategories', function() {
            return SubCategory::orderBy('category_id')->orderBy('ordering')->with('parent')->withCount('activePosts')->withCount('unactivePosts')->get();
        }));
        View::share('workoutCategories', Cache::rememberForever('workout.categories', function() {
            return \App\Models\Workout\Category::orderBy('ordering')->with(['childs' => function($q){
                $q->withCount('unactivePosts');
            }])->withCount('activePosts')->withCount('unactivePosts')->get();
        }));
        View::share('workoutSubCategories', Cache::rememberForever('workout.subCategories', function() {
            return \App\Models\Workout\SubCategory::orderBy('category_id')->orderBy('ordering')->with('parent')->withCount('activePosts')->withCount('unactivePosts')->get();
        }));

        View::share('settings', Setting::all());

        View::share('last5Workouts', Post::where('active',1)->where('subcategory_id',4)->take(5)->get());
        View::share('last5Manuals', Post::where('active',1)->where('subcategory_id',35)->take(5)->get());
        View::share('last5FoodAndInjuries', Post::where('active',1)->whereIn('subcategory_id',[30,31])->take(5)->get());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once __DIR__.'../../Helpers/Navigation.php';
    }
}
